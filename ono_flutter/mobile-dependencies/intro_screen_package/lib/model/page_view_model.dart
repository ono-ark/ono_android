import 'package:flutter/material.dart';
import 'package:intro_screen_package/model/page_decoration.dart';

class PageViewModel {
  /// Title of page
  final String title;

  /// Title of page
  final Widget titleWidget;

  /// Text of page (description)
  final String content;

  /// Widget content of page (description)
  final Widget contentWidget;

  /// Image of page
  /// Tips: Wrap your image with an alignment widget like Align or Center
  final Widget image;

  /// Footer widget, you can add a button for example
  final Widget footer;

  /// set value showTitleByImage
  final bool showTitleByImage;

  /// Page decoration
  /// Contain all page customizations, like page color, text styles
  final PageDecoration decoration;

  PageViewModel({
    this.title,
    this.titleWidget,
    this.content,
    this.contentWidget,
    this.image,
    this.footer,
    this.showTitleByImage=false,
    this.decoration = const PageDecoration(),
  })  : assert(
          title != null || titleWidget != null,
          "You must provide either title (String) or titleWidget (Widget).",
        ),
        assert(
          (title == null) != (titleWidget == null),
          "You can not provide both title and titleWidget.",
        ),
        assert(
          content != null || contentWidget != null,
          "You must provide either body (String) or bodyWidget (Widget).",
        ),
        assert(
          (content == null) != (contentWidget == null),
          "You can not provide both body and bodyWidget.",
        );
}
