import 'package:flutter/material.dart';
import '../model/page_view_model.dart';
import 'intro_content.dart';

class IntroPage extends StatelessWidget {
  final PageViewModel page;

  const IntroPage({Key key, @required this.page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: page.decoration.pageColor,
      decoration: page.decoration.boxDecoration,
      child: SafeArea(
        top: false,
        child: (page.showTitleByImage)
            ? _buildTitleByImage(context)
            : _buildImageByTitle(),
      ),
    );
  }

  Widget _buildImageByTitle() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        if (page.image != null)
          Expanded(
            flex: page.decoration.imageFlex,
            child: Padding(
              padding: page.decoration.imagePadding,
              child: page.image,
            ),
          ),
        Expanded(
          flex: page.decoration.bodyFlex,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 70.0),
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: IntroContent(page: page),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildWidget(Widget widget, String text, TextStyle style) {
    return widget ?? Text(text, style: style, textAlign: TextAlign.start);
  }

  Widget _buildTitleByImage(context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Expanded(
            flex: page.decoration.bodyFlex,
            child: Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).viewPadding.top + 20,
              ),
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: IntroContent(page: page),
              ),
            ),
          ),
          if (page.image != null)
            Expanded(
              flex: page.decoration.imageFlex,
              child: Padding(
                padding: page.decoration.imagePadding,
                child: page.image,
              ),
            ),
          // Expanded(
          //   flex: page.decoration.imageFlex,
          //   child: Padding(
          //     padding: page.decoration.titlePadding,
          //     child: _buildWidget(
          //       page.titleWidget,
          //       page.title,
          //       page.decoration.titleTextStyle,
          //     ),
          //   ),
          // ),
          // Padding(
          //   padding: page.decoration.descriptionPadding,
          //   child: _buildWidget(
          //     page.contentWidget,
          //     page.content,
          //     page.decoration.bodyTextStyle,
          //   ),
          // ),
          // if (page.image != null)
          //   widget(
          //     child: Padding(
          //       padding: page.decoration.imagePadding,
          //       child: page.image,
          //     ),
          //   ),
        ],
      ),
    );
  }
}
