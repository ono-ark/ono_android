class UserStatusKeys {
  static final userRegistrationPending = 'USER_REGISTRATION_PENDING';
  static final newUser = 'NEW_USER';
  static final userSignInPending = 'USER_SIGN_IN_PENDING';
  static final registeredUser = 'REGISTERED_USER';
}
