import 'package:ono_clean_framework/service/ono_service.dart';
import 'package:ono_clean_framework/service/rest_api.dart';
import 'package:ono_flutter/core/api/fuse_api/fuse_api.dart';

import 'otp_response.dart';

class OTPService extends OnoService {
  OTPService(OnoServiceResponseHandler handler)
      : super(
          url: FuseAPI.otpValidateUrl,
          action: HttpAction.post,
          handler: handler,
          // options: FuseAPI.getFuseOptions(),
        );

  @override
  OTPResponse parseJson(Map<String, dynamic> json) {
    return OTPResponse.fromJson(json);
  }
}
