import 'package:ono_clean_framework/service/ono_service.dart';

class OTPRequestData implements RequestModel {
  final String otpReqId;
  final String otp;
  final String mobile;

  OTPRequestData({this.otpReqId, this.otp, this.mobile});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> requestData = Map<String, dynamic>();
    requestData["otpReqId"] = this.otpReqId;
    requestData["otp"] = this.otp;
    requestData['mobile'] = this.mobile;
    return requestData;
  }
}
