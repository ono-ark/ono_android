import 'package:ono_clean_framework/service/ono_service.dart';

class OTPResponse implements ResponseModel {
  final Data data;
  final Error error;

  OTPResponse({this.data, this.error});

  factory OTPResponse.fromJson(Map<String, dynamic> json) {
    return OTPResponse(
      data: json['data'] != null ? Data.fromJson(json['data']) : null,
      error: json['error'] != null ? Error.fromJson(json['error']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error.toJson();
    }
    return data;
  }
}

class Error implements ResponseModel {
  final String errorCode;
  final String errorDesc;

  Error({this.errorCode, this.errorDesc});

  factory Error.fromJson(Map<String, dynamic> json) {
    return Error(
      errorCode: json['errorCode'],
      errorDesc: json['errorDesc'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorCode'] = this.errorCode;
    data['errorDesc'] = this.errorDesc;
    return data;
  }
}

class Data implements ResponseModel {
  final int onoId;
  final Profile profile;
  final String useStatus;

  Data({this.onoId, this.profile, this.useStatus});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      onoId: json['onoId'],
      profile:
          json['profile'] != null ? Profile.fromJson(json['profile']) : null,
      useStatus: json['useStatus'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['onoId'] = this.onoId;
    data['useStatus'] = this.useStatus;
    if (this.profile != null) {
      data['profile'] = this.profile.toJson();
    }
    return data;
  }
}

class Profile implements ResponseModel {
  final DeviceInfo deviceInfo;
  final bool locAllow;
  final Location location;
  final OnoBuddyPrefs onoBuddyPrefs;
  final ProfileDetails profile;

  Profile(
      {this.deviceInfo,
      this.locAllow,
      this.location,
      this.onoBuddyPrefs,
      this.profile});

  factory Profile.fromJson(Map<String, dynamic> json) {
    return Profile(
      deviceInfo: json['deviceInfo'] != null
          ? DeviceInfo.fromJson(json['deviceInfo'])
          : null,
      locAllow: json['locAllow'],
      location:
          json['location'] != null ? Location.fromJson(json['location']) : null,
      onoBuddyPrefs: json['onoBuddyPrefs'] != null
          ? OnoBuddyPrefs.fromJson(json['onoBuddyPrefs'])
          : null,
      profile: json['profile'] != null
          ? ProfileDetails.fromJson(json['profile'])
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['locAllow'] = this.locAllow;
    if (this.deviceInfo != null) {
      data['deviceInfo'] = this.deviceInfo.toJson();
    }
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    if (this.onoBuddyPrefs != null) {
      data['onoBuddyPrefs'] = this.onoBuddyPrefs.toJson();
    }
    if (this.profile != null) {
      data['profile'] = this.profile.toJson();
    }
    return data;
  }
}

class ProfileDetails implements ResponseModel {
  final String fullName;
  final String gender;
  final String mobile;
  final int onoId;
  final String profilePicUrl;
  final String userType;

  ProfileDetails(
      {this.fullName,
      this.gender,
      this.mobile,
      this.onoId,
      this.profilePicUrl,
      this.userType});

  factory ProfileDetails.fromJson(Map<String, dynamic> json) {
    return ProfileDetails(
      fullName: json['fullName'],
      gender: json['gender'],
      mobile: json['mobile'],
      onoId: json['onoId'],
      profilePicUrl: json['profilePicUrl'],
      userType: json['userType'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fullName'] = this.fullName;
    data['gender'] = this.gender;
    data['mobile'] = this.mobile;
    data['onoId'] = this.onoId;
    data['profilePicUrl'] = this.profilePicUrl;
    data['userType'] = this.userType;
    return data;
  }
}

class OnoBuddyPrefs implements ResponseModel {
  final List<Crop> crops;
  final String marketId;
  final String marketName;

  OnoBuddyPrefs({this.crops, this.marketId, this.marketName});

  factory OnoBuddyPrefs.fromJson(Map<String, dynamic> json) {
    return OnoBuddyPrefs(
      crops: json['crops'] != null
          ? (json['crops'] as List).map((i) => Crop.fromJson(i)).toList()
          : null,
      marketId: json['marketId'],
      marketName: json['marketName'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['marketId'] = this.marketId;
    data['marketName'] = this.marketName;
    if (this.crops != null) {
      data['crops'] = this.crops.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Crop implements ResponseModel {
  final String cropId;
  final String cropName;
  final String imageUrl;

  Crop({this.cropId, this.cropName, this.imageUrl});

  factory Crop.fromJson(Map<String, dynamic> json) {
    return Crop(
      cropId: json['cropId'],
      cropName: json['cropName'],
      imageUrl: json['imageUrl'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cropId'] = this.cropId;
    data['cropName'] = this.cropName;
    data['imageUrl'] = this.imageUrl;
    return data;
  }
}

class DeviceInfo implements ResponseModel {
  final String model;
  final String os;
  final String version;

  DeviceInfo({this.model, this.os, this.version});

  factory DeviceInfo.fromJson(Map<String, dynamic> json) {
    return DeviceInfo(
      model: json['model'],
      os: json['os'],
      version: json['version'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['model'] = this.model;
    data['os'] = this.os;
    data['version'] = this.version;
    return data;
  }
}

class Location implements ResponseModel {
  final String latitude;
  final String longitude;

  Location({this.latitude, this.longitude});

  factory Location.fromJson(Map<String, dynamic> json) {
    return Location(
      latitude: json['latitude'],
      longitude: json['longitude'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }
}
