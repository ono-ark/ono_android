// import 'dart:io';
//
// import 'package:agrifi/common/ui_kit/agrifi_inputs.dart';
// import 'package:agrifi/theme/agrifi_theme.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
//
// import 'otp_timer.dart';
//
// class OTPTextField extends StatelessWidget {
//   final TextEditingController textEditingController;
//   final String errorText;
//   final int timeInSec;
//   final Function otpNumberTextChanged;
//   final VoidCallback resendOtpTap;
//
//   const OTPTextField(
//       {Key key,
//       this.textEditingController,
//       this.errorText,
//       this.timeInSec,
//       this.otpNumberTextChanged,
//       this.resendOtpTap})
//       : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return TextFormField(
//       controller: this.textEditingController,
//       autofocus: true,
//       decoration: InputDecoration(
//         suffixIcon: Padding(
//           padding: const EdgeInsetsDirectional.only(end: 12.0, top: 14.0),
//           child: TimerWidget(
//             timeInSec: 60,
//             resendTapCallBack: resendOtpTap,
//           ), // myIcon is a 48px-wide widget.
//         ),
//         fillColor: Colors.white,
//         filled: true,
//         errorText: errorText,
//         errorBorder: errorBorder(),
//         focusedErrorBorder: errorBorder(),
//         border: otpFocusedBorder(),
//         focusedBorder: otpFocusedBorder(),
//         enabledBorder: otpFocusedBorder(),
//       ),
//       onChanged: otpNumberTextChanged,
//       keyboardType:
//           (Platform.isIOS) ? TextInputType.text : TextInputType.number,
//       inputFormatters: [
//         LengthLimitingTextInputFormatter(6),
//         FilteringTextInputFormatter.digitsOnly
//       ],
//     );
//   }
// }
//
// OutlineInputBorder otpFocusedBorder() {
//   return OutlineInputBorder(
//     borderRadius: BorderRadius.all(Radius.circular(3.0)),
//     borderSide: BorderSide(width: 1.0, color: buttonEnabledColor),
//   );
// }
