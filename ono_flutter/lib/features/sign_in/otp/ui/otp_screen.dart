import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/sign_in/otp/model/otp_view_model.dart';
import 'package:ono_flutter/features/sign_in/otp/ui/otp_timer.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_text_field.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

class OTPScreen extends StatelessWidget {
  final OTPViewModel otpViewModel;
  final Function otpNumberOnChanged;
  final VoidCallback submitCallBack;
  final VoidCallback resendOtpTapEvent;
  final VoidCallback backButtonTapEvent;
  final TextEditingController otpController = TextEditingController(text: '');

  OTPScreen({
    Key key,
    this.otpViewModel,
    this.otpNumberOnChanged,
    this.submitCallBack,
    this.resendOtpTapEvent,
    this.backButtonTapEvent,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColor,
      body: Center(
        child: ListView(
          children: [
            _buildAppbar(),
            _buildAppbarBelowWidget(),
            _buildTitle(),
            _buildOTP(context: context),
            _buildButton(),
            _buildTermsAndConditions(),
          ],
        ),
      ),
    );
  }

  _buildAppbar() {
    return AppBarWidget(
      text: signInStrings.signIn,
      showBackIcon: true,
      voidCallback: backButtonTapEvent,
    );
  }

  _buildAppbarBelowWidget() {
    return PaddingLeftAndRight(
      child: Padding(
        padding: paddingSpaceFromAppbar,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                GenericTextWithStyle(
                  text: otpViewModel.phoneNumber,
                  textStyle: theme.headline2,
                ),
                GestureDetector(
                  onTap: backButtonTapEvent,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black12),
                          borderRadius: BorderRadius.circular(30)),
                      child: CircleAvatar(
                        radius: 14,
                        child: SvgPicture.asset(
                          'assets/Images/svg/edit.svg',
                          width: 12,
                          height: 12,
                        ),
                        backgroundColor: whiteColor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            TimerWidget(
              resendTapCallBack: resendOtpTapEvent,
            )
          ],
        ),
      ),
    );
  }

  _buildTitle() {
    return PaddingLeftAndRight(
      child: Padding(
        padding: paddingSpaceFromAppbar,
        child: GenericTextWithStyle(
          text: signInStrings.enterOTP,
          textStyle: theme.headline2,
        ),
      ),
    );
  }

  _buildOTP({BuildContext context}) {
    if (otpViewModel.otpNumberUpdate != null) {
      if (otpViewModel.otpNumberUpdate.isNotEmpty) {
        otpController.text = otpViewModel.otpNumberUpdate;
        otpController.selection =
            TextSelection.collapsed(offset: otpController.text.length);
        if (otpViewModel.textFieldValidationType ==
                TextFieldValidationType.success ||
            otpViewModel.textFieldValidationType ==
                TextFieldValidationType.enable) {
          FocusScope.of(context).requestFocus(FocusNode());
        }
      }
    }
    return Container(
      margin: paddingSpaceFromHeadline2,
      child: PaddingLeftAndRight(
        child: OnoInput(
          textEditingController: otpController,
          textFieldType: TextFieldType.otp,
          displayErrorMessage: otpViewModel.otpErrorText,
          functionCallBack: otpNumberOnChanged,
        ),
      ),
    );
  }

  _buildButton() {
    return Padding(
      padding: paddingSpaceFromAppbar,
      child: ButtonWithTapEvent(
        text: signInStrings.continueText,
        tapCallBack: submitCallBack,
      ),
    );
  }

  _buildTermsAndConditions() {
    return Padding(
      padding: paddingSpaceFromHeadline2,
      child: TermsAndConditions(),
    );
  }
}
