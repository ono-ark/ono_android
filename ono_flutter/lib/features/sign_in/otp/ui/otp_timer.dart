import 'dart:async';

import 'package:flutter/material.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

class TimerWidget extends StatefulWidget {
  final int timeInSec;
  final VoidCallback resendTapCallBack;

  const TimerWidget({Key key, this.timeInSec = 60, this.resendTapCallBack})
      : super(key: key);
  @override
  _TimerWidgetState createState() => _TimerWidgetState();
}

class _TimerWidgetState extends State<TimerWidget> {
  String timerText = '';
  var timer;
  bool enableTapEvent = false;
  @override
  void initState() {
    _startTimer();

    super.initState();
  }

  _startTimer() {
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      timerText =
          '${((widget.timeInSec - timer.tick) ~/ widget.timeInSec).toString().padLeft(2, '0')}:${((widget.timeInSec - timer.tick) % widget.timeInSec).toString().padLeft(2, '0')}';
      if (timer.tick >= 60) {
        timer.cancel();
        enableTapEvent = true;
        timerText = signInStrings.resendOtp;
      }

      setState(() {});
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // FocusScope.of(context).requestFocus(FocusNode());
    return InkWell(
      onTap: (enableTapEvent)
          ? () {
              setState(() {
                enableTapEvent = false;
                FocusScope.of(context).requestFocus(FocusNode());
                _startTimer();
                widget.resendTapCallBack();
              });
            }
          : null,
      child: GenericTextWithStyle(
        text: (!enableTapEvent) ? timerText : timerText,
        textStyle: theme.headline3.copyWith(
            color:
                (!enableTapEvent) ? timerTextColor : termsAndConditionsColor),
      ),
    );
  }
}
