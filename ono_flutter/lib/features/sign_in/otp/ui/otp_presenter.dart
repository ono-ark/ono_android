import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:ono_clean_framework/ui/presenter.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/profile_details/ui/feature_widget.dart';
import 'package:ono_flutter/features/select_crops/ui/feature_widget.dart';
import 'package:ono_flutter/features/sign_in/bloc/sign_in_bloc.dart';
import 'package:ono_flutter/features/sign_in/otp/model/otp_view_model.dart';
import 'package:ono_flutter/features/sign_in/otp/ui/otp_screen.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

class OtpPresenter extends Presenter<SignInBloc, OTPViewModel> {
  @override
  Widget buildLoadingScreen(BuildContext context) {
    return GenericProgressbar();
  }

  @override
  Widget buildErrorScreen(BuildContext context) {
    // TODO: implement buildErrorScreen
    return super.buildErrorScreen(context);
  }

  @override
  Widget buildScreen(BuildContext context, SignInBloc bloc, viewModel) {
    if (viewModel.apiResponseType == ApiResponseType.successful &&
        viewModel.userSessionStatus == UserSessionStatus.registrationPending) {
      // _modalBottomSheetMenu(context, viewModel);
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Future.delayed(Duration(seconds: 1), () {
          bloc.resetResponseEvents.launch();
          _navigateToProfileDetailsPresenter(context);
        });
      });
    } else if (viewModel.apiResponseType == ApiResponseType.successful &&
        viewModel.userSessionStatus == UserSessionStatus.registeredUser) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => SelectCropsFeatureWidget()));
      });
    } else if (viewModel.textFieldValidationType ==
        TextFieldValidationType.success) {
      // _modalBottomSheetMenu(context, viewModel);
      bloc.otpSubmitEvent.launch();
    } else if (viewModel.apiResponseType == ApiResponseType.failure) {
      // bloc.resetAPIErrorCasesEvent.launch();
      // dismissPopsIfAvailable(context);
    }
    return OTPScreen(
        otpViewModel: viewModel,
        submitCallBack: () => _submitTap(bloc, viewModel, context),
        otpNumberOnChanged: (text) => _phoneNumberValueUpdate(text, bloc),
        resendOtpTapEvent: () => _resendOtpTapEvent(bloc),
        backButtonTapEvent: () {
          Navigator.pop(context);
        });
  }

  @override
  Stream<OTPViewModel> getViewModelStream(
      BuildContext context, SignInBloc bloc) {
    return bloc.otpViewModelEvent.receive;
  }

  @override
  void sendViewModelRequest(BuildContext context, SignInBloc bloc) {
    bloc.otpRequestEvent.launch();
  }

  _submitTap(SignInBloc bloc, OTPViewModel viewModel, BuildContext context) {
    //bloc.updateTextFieldValueEvent.launch();
    // _modalBottomSheetMenu(context, viewModel);
    bloc.otpSubmitEvent.launch();
  }

  _phoneNumberValueUpdate(String text, SignInBloc bloc) {
    bloc.otpNumberTextChangedEvent.send(text);
  }

  _navigateToProfileDetailsPresenter(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => ProfileDetailsFeatureWidget()));
  }

  _resendOtpTapEvent(SignInBloc bloc) {
    bloc.resendOtpEvent.launch();
  }

  // void _modalBottomSheetMenu(BuildContext context, OTPViewModel otpViewModel) {
  //   showBottomCircularProgressBar(
  //       context: context,
  //       text: (otpViewModel.textFieldValidationType ==
  //                   TextFieldValidationType.success ||
  //               otpViewModel.textFieldValidationType ==
  //                   TextFieldValidationType.enable)
  //           ? getTranslated(context, registrationStrings.verifyingDevice)
  //           : getTranslated(context, registrationStrings.deviceVerified),
  //       icon: (otpViewModel.textFieldValidationType ==
  //               TextFieldValidationType.success)
  //           ? null
  //           : buildIcon());
  // }
}
