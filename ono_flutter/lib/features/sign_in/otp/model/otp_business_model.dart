import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class OTPBusinessModel implements BusinessModel {
  String code = "";
  String description = "";
  String status = "";
  String phoneNumber = '';
  String otpNumberUpdate = '';
  String otpErrorText = '';
  int otpTimeInSec = 0;
  UserSessionStatus userSessionStatus = UserSessionStatus.none;
  ApiResponseType apiResponseType = ApiResponseType.none;
  TextFieldValidationType textFieldValidationType =
      TextFieldValidationType.none;
}
