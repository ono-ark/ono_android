import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class OTPViewModel implements ViewModel {
  final String phoneNumber;
  final String otpNumberUpdate;
  final String otpErrorText;
  final ApiResponseType apiResponseType;
  final TextFieldValidationType textFieldValidationType;
  final int otpTimeInSec;
  final UserSessionStatus userSessionStatus;

  OTPViewModel(
      {this.apiResponseType,
      this.textFieldValidationType,
      this.otpErrorText,
      this.otpNumberUpdate,
      this.phoneNumber,
      this.otpTimeInSec,
      this.userSessionStatus});
}
