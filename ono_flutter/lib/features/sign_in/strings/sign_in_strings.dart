class SignInStrings {
  SignInStrings();
  get signIn => 'Sign-In';
  get enterYourMobileNumber => 'Enter your mobile number';
  get continueText => 'CONTINUE';
  get byContinuing => 'By continuing, you are agree to ONO’s ';
  get conditionsOfUse => 'Conditions of Use ';
  get and => ' and ';
  get privacyPolicy => 'Privacy Policy';
  get enterOTP => 'Enter OTP';
  get resendOtp => 'Resend OTP';
  get enterPhoneNumberError => 'Please Enter 10 Digit Mobile Number';
  get enterOtpError => 'Please enter a valid OTP.';
}
