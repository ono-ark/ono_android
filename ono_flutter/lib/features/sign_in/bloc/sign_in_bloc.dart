import 'dart:convert';
import 'dart:typed_data';

import 'package:network_image_to_byte/network_image_to_byte.dart';
import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/core/api/device_info/device_info.dart';
import 'package:ono_flutter/core/api/fuse_api/fuse_api.dart';
import 'package:ono_flutter/core/api/response_handler.dart';
import 'package:ono_flutter/core/api/user_info/user_info.dart';
import 'package:ono_flutter/features/sign_in/login/api/mobile_number_request_model.dart';
import 'package:ono_flutter/features/sign_in/login/api/mobile_number_response.dart';
import 'package:ono_flutter/features/sign_in/login/api/mobile_number_service.dart';
import 'package:ono_flutter/features/sign_in/login/model/login_business_model.dart';
import 'package:ono_flutter/features/sign_in/login/model/login_view_model.dart';
import 'package:ono_flutter/features/sign_in/otp/api/otp_request_model.dart';
import 'package:ono_flutter/features/sign_in/otp/api/otp_response.dart';
import 'package:ono_flutter/features/sign_in/otp/api/otp_service.dart';
import 'package:ono_flutter/features/sign_in/otp/api/user_status.dart';
import 'package:ono_flutter/features/sign_in/otp/model/otp_business_model.dart';
import 'package:ono_flutter/features/sign_in/otp/model/otp_view_model.dart';
import 'package:ono_flutter/ui_utils/validations.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:sms_user_consent/sms_user_consent.dart';

class SignInBloc extends ResponseHandlerBloc implements Bloc {
  //local variables
  int _onoId;
  bool _locationStatus = false;
  //BusinessModel;
  LoginBusinessModel _loginBusinessModel;
  OTPBusinessModel _otpBusinessModel;

  //Service
  MobileNumberService _registerService;
  OTPService _otpService;

  //Get PhoneNumbers
  SmsUserConsent _smsUserConsent;
  SmsAutoFill _smsAutoFill;
  DeviceInfoAPI _deviceInfoAPI;

  //Events Pipes for login
  EventPipe numberRequestEvent = EventPipe();
  BroadcastPipe<LoginViewModel> loginViewModelEvent =
      BroadcastPipe<LoginViewModel>();
  Pipe<String> phoneNumberTextChangedUpdateEvent = Pipe<String>();
  EventPipe loginSubmitEvent = EventPipe();

  //Events Pipes for OTP
  EventPipe otpRequestEvent = EventPipe();
  BroadcastPipe<OTPViewModel> otpViewModelEvent = BroadcastPipe<OTPViewModel>();
  Pipe<String> otpNumberTextChangedEvent = Pipe<String>();
  EventPipe otpSubmitEvent = EventPipe();
  EventPipe resetResponseEvents = EventPipe();
  EventPipe updateTextFieldValueEvent = EventPipe();
  EventPipe resendOtpEvent = EventPipe();

  SignInBloc(
      {LoginBusinessModel loginBusinessModel,
      OTPBusinessModel otpBusinessModel,
      SmsUserConsent smsUserConsent,
      SmsAutoFill smsAutoFill,
      MobileNumberService registerService,
      OTPService otpService,
      DeviceInfoAPI deviceInfoAPI}) {
    //Objects Creation of business models
    _loginBusinessModel = loginBusinessModel ?? LoginBusinessModel();
    _otpBusinessModel = otpBusinessModel ?? OTPBusinessModel();

    //Objects creation of Services
    _registerService = registerService ?? MobileNumberService(this);
    _otpService = otpService ?? OTPService(this);

    _smsUserConsent = smsUserConsent ?? SmsUserConsent();
    _smsAutoFill = smsAutoFill ?? SmsAutoFill();
    _deviceInfoAPI = deviceInfoAPI ?? DeviceInfoAPI();

    //Events Listening
    numberRequestEvent.listen(_passLoginBusinessModelToViewModel);
    phoneNumberTextChangedUpdateEvent.receive.listen(_validatePhoneNumber);
    loginSubmitEvent.listen(_submitValidationAndMakeServiceCall);
    _smsUserConsent.updatePhoneNumberListener(_updateSelectedPhoneNumber);

    otpRequestEvent.listen(_prePareOtpModel);
    resendOtpEvent.listen(_resendOtpRequest);
    otpNumberTextChangedEvent.receive.listen(_updateOtpNumber);
    otpSubmitEvent.listen(_makeOtpValidateAndServiceCall);
    _smsAutoFill.code.listen(_listenForOtpCode);
    updateTextFieldValueEvent.listen(_updateTextFieldValidation);
  }

  void _passLoginBusinessModelToViewModel() async {
    UserInfoAPI.setUserAppStatus(userAppStatus: UserAppStatus.signIn);
    _loginBusinessModel = LoginBusinessModel();
    _smsUserConsent.requestPhoneNumber();
    _passLoginViewModel();
  }

  _passLoginViewModel() {
    loginViewModelEvent.send(_prepareLoginBusinessToViewModel());
  }

  //validation
  void _validatePhoneNumber(String event) {
    _loginBusinessModel.phoneNumberUpdate = event;
    _loginBusinessModel.phoneNumberErrorText =
        _validatePhoneNumberField(_loginBusinessModel.phoneNumberUpdate);
    _passLoginViewModel();
  }

  _setNumberSuccessMessage() {
    _loginBusinessModel.textFieldValidationType =
        TextFieldValidationType.success;
    _loginBusinessModel.phoneNumberErrorText = 'Valid Mobile Number';
  }

  _updateSelectedPhoneNumber() {
    if (_smsUserConsent.selectedPhoneNumber != null &&
        _smsUserConsent.selectedPhoneNumber.isNotEmpty) {
      if (_smsUserConsent.selectedPhoneNumber.substring(0, 3) == '+91') {
        _loginBusinessModel.phoneNumberUpdate =
            _smsUserConsent.selectedPhoneNumber.substring(3);
      } else {
        _loginBusinessModel.phoneNumberUpdate =
            _smsUserConsent.selectedPhoneNumber;
      }
      _smsAutoFill.listenForCode;
      _setNumberSuccessMessage();
    }
    loginViewModelEvent.send(_prepareLoginBusinessToViewModel());
  }

  _loginRequestData() async {
    String langId = await UserInfoAPI.getLanguagePreferenceId();
    MobileNumberRequestModel requestData = MobileNumberRequestModel(
      mobile: _loginBusinessModel.phoneNumberUpdate,
      langId: langId,
      locationRequestModel: await _getLocationDetails(),
      locationAllow: _locationStatus,
      deviceInfoRequestModel: await _getDeviceDetails(),
    );

    return requestData;
  }

  _getLanguage() async {
    String langId = await UserInfoAPI.getLanguagePreferenceId();
    List list = langId.split('&');
    return LanguageRequestModel(
        langId: list[0].toString(),
        langName: list[1].toString(),
        type: list[2].toString());
  }

  _getLocationDetails() async {
    String location = await UserInfoAPI.getLocationDetails();
    List list = ['', ''];
    if (location != null && location.isNotEmpty) {
      _locationStatus = true;
      list = location.split('&');
    }
    return LocationRequestModel(
        longitude: list[1].toString(), latitude: list[0].toString());
  }

  _getDeviceDetails() async {
    DeviceInfoBusinessModel deviceInfoBusinessModel =
        await _deviceInfoAPI.getDeviceInfo();
    return DeviceInfoRequestModel(
        model: deviceInfoBusinessModel.model,
        os: deviceInfoBusinessModel.device,
        version: deviceInfoBusinessModel.deviceVersion);
  }

  _prepareLoginBusinessToViewModel() {
    return LoginViewModel(
        phoneNumberUpdate: _loginBusinessModel.phoneNumberUpdate,
        textFieldValidationType: _loginBusinessModel.textFieldValidationType,
        phoneNumberErrorText: _loginBusinessModel.phoneNumberErrorText,
        apiResponseType: _loginBusinessModel.apiResponseType);
  }

  _makeOtpRequest() async {
    var requestData = await _loginRequestData();
    MobileNumberResponse registerResponse =
        await _registerService.request(data: requestData);
    return registerResponse;
  }

  void _submitValidationAndMakeServiceCall() async {
    _loginBusinessModel.phoneNumberErrorText =
        _validatePhoneNumberField(_loginBusinessModel.phoneNumberUpdate);
    if (_loginBusinessModel.phoneNumberUpdate.isNotEmpty) {
      MobileNumberResponse mobileNumberResponse = await _makeOtpRequest();
      if (mobileNumberResponse != null) {
        _loginBusinessModel.loginActivityId =
            mobileNumberResponse.data.otpReqId;
        _loginBusinessModel.apiResponseType = ApiResponseType.successful;
        // await UserSessionAPI.setPhoneNumber(
        //     _registerBusinessModel.phoneNumberUpdate);
      } else {
        _loginBusinessModel.apiResponseType = ApiResponseType.failure;
      }
    } else {
      _loginBusinessModel.textFieldValidationType =
          TextFieldValidationType.error;
    }
    // _loginBusinessModel.apiResponseType = ApiResponseType.successful;
    _passLoginViewModel();
  }

  void _prePareOtpModel() {
    _otpBusinessModel.phoneNumber = _loginBusinessModel.phoneNumberUpdate;
    _otpBusinessModel.otpErrorText = null;
    _otpBusinessModel.otpNumberUpdate = '';
    _passOtpViewModel();
  }

  _passOtpViewModel() {
    otpViewModelEvent.send(_prePareOtpViewModel());
  }

  _resendOtpRequest() async {
    MobileNumberResponse registerResponse = await _makeOtpRequest();
    if (registerResponse != null) {
      _loginBusinessModel.loginActivityId = registerResponse.data.otpReqId;
      _prePareOtpViewModel();
    } else {
      _otpBusinessModel.apiResponseType = ApiResponseType.failure;
      _prePareOtpViewModel();
    }
  }

  _prePareOtpViewModel() {
    return OTPViewModel(
        otpErrorText: _otpBusinessModel.otpErrorText,
        otpNumberUpdate: _otpBusinessModel.otpNumberUpdate,
        phoneNumber: _otpBusinessModel.phoneNumber,
        apiResponseType: _otpBusinessModel.apiResponseType,
        textFieldValidationType: _otpBusinessModel.textFieldValidationType,
        userSessionStatus: _otpBusinessModel.userSessionStatus,
        otpTimeInSec: _otpBusinessModel.otpTimeInSec);
  }

  void _updateOtpNumber(String event) {
    _otpBusinessModel.otpNumberUpdate = event;
    _otpBusinessModel.otpErrorText =
        _validateOTPField(_otpBusinessModel.otpNumberUpdate);
    // if (_otpBusinessModel.otpNumberUpdate.length == 6) {
    //   _setOtpSuccessMessage();
    // } else {
    //   _otpBusinessModel.textFieldValidationType = TextFieldValidationType.error;
    // }
    _passOtpViewModel();
  }

  _setOtpSuccessMessage() {
    _otpBusinessModel.textFieldValidationType = TextFieldValidationType.enable;
  }

  _updateTextFieldValidation() {
    _otpBusinessModel.textFieldValidationType = TextFieldValidationType.success;
    _passOtpViewModel();
  }

  void _listenForOtpCode(String event) {
    if (event.isNotEmpty) {
      _smsAutoFill.unregisterListener();
      _otpBusinessModel.otpNumberUpdate = event;
      _otpBusinessModel.textFieldValidationType =
          TextFieldValidationType.success;
      _passOtpViewModel();
    }
  }

  void _reSetResponseEvents() {
    _loginBusinessModel = LoginBusinessModel();
    _otpBusinessModel = OTPBusinessModel();
  }

  void _makeOtpValidateAndServiceCall() async {
    _otpBusinessModel.otpErrorText =
        _validateOTPField(_otpBusinessModel.otpNumberUpdate);
    if (_otpBusinessModel.otpErrorText == null) {
      var otpRequestData = OTPRequestData(
          otpReqId: _loginBusinessModel.loginActivityId,
          otp: _otpBusinessModel.otpNumberUpdate,
          mobile: _otpBusinessModel.phoneNumber);

      OTPResponse otpResponse = await _otpService.request(data: otpRequestData);
      if (otpResponse != null && otpResponse.data != null) {
        _otpBusinessModel.textFieldValidationType =
            TextFieldValidationType.none;
        _otpBusinessModel.apiResponseType = ApiResponseType.successful;
        _onoId = otpResponse.data.onoId;
        UserInfoAPI.setOnoId(_onoId.toString());
        if (otpResponse.data.useStatus ==
            UserStatusKeys.userRegistrationPending) {
          _otpBusinessModel.userSessionStatus =
              UserSessionStatus.registrationPending;
        } else if (otpResponse.data.useStatus ==
            UserStatusKeys.registeredUser) {
          if (otpResponse.data.profile.profile.profilePicUrl.isNotEmpty) {
            Uint8List bytes;
            bytes = await networkImageToByte(
                otpResponse.data.profile.profile.profilePicUrl);
            String imageB64 = base64Encode(bytes);
            UserInfoAPI.setUserProfilePic(imageB64);
          }
          _otpBusinessModel.userSessionStatus =
              UserSessionStatus.registeredUser;
        } else {
          _otpBusinessModel.userSessionStatus = UserSessionStatus.freshUser;
        }
      } else {
        _otpBusinessModel.apiResponseType = ApiResponseType.failure;
      }
    }
    _passOtpViewModel();
  }

  //Validations
  String _validatePhoneNumberField(String text) {
    return Validations.validateMobileNumber(text);
  }

  String _validateOTPField(String text) {
    return Validations.validateOTP(text);
  }

  @override
  void dispose() {
    //login dispose
    loginSubmitEvent.dispose();
    numberRequestEvent.dispose();
    loginViewModelEvent.dispose();
    phoneNumberTextChangedUpdateEvent.dispose();

    //otp dispose
    otpViewModelEvent.dispose();
    otpNumberTextChangedEvent.dispose();
    otpRequestEvent.dispose();
    otpSubmitEvent.dispose();
    _smsUserConsent.dispose();
    super.dispose();
  }
}
