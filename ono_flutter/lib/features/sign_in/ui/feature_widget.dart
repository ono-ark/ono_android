import 'package:flutter/material.dart';
import 'package:ono_clean_framework/bloc/bloc_provider.dart';
import 'package:ono_flutter/common/features/navigator/navigator.dart';
import 'package:ono_flutter/features/sign_in/bloc/sign_in_bloc.dart';
import 'package:ono_flutter/features/sign_in/login/ui/login_presenter.dart';

class SignInFeatureWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: SignInBloc(),
      child: FeatureNavigator(
        sectionChild: LoginPresenter(),
        maintainState: true,
      ),
    );
  }
}
