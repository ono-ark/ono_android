import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/sign_in/login/model/login_view_model.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_text_field.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

class LoginScreen extends StatelessWidget {
  final LoginViewModel loginViewModel;
  final Function phoneNumberOnChanged;
  final VoidCallback submitCallBack;
  final VoidCallback conditionsOfUseTapEvent;
  final VoidCallback privacyPolicyTapEvent;
  final TextEditingController phoneNumberController = TextEditingController();
  final FocusNode focusNode = FocusNode();

  LoginScreen(
      {Key key,
      this.loginViewModel,
      this.phoneNumberOnChanged,
      this.submitCallBack,
      this.conditionsOfUseTapEvent,
      this.privacyPolicyTapEvent})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColor,
      body: Center(
        child: ListView(
          children: [
            _buildAppbar(),
            _buildTitle(),
            _buildPhoneNumber(context: context),
            _buildButton(),
            _buildTermsAndConditions(),
          ],
        ),
      ),
    );
  }

  _buildAppbar() {
    return AppBarWidget(
      text: signInStrings.signIn,
    );
  }

  _buildTitle() {
    return PaddingLeftAndRight(
      child: Padding(
        padding: paddingSpaceFromAppbar,
        child: GenericTextWithStyle(
          text: signInStrings.enterYourMobileNumber,
          textStyle: theme.headline2,
        ),
      ),
    );
  }

  _buildPhoneNumber({BuildContext context}) {
    if (loginViewModel.phoneNumberUpdate != null) {
      if (loginViewModel.phoneNumberUpdate.isNotEmpty) {
        phoneNumberController.text = loginViewModel.phoneNumberUpdate;
        phoneNumberController.selection =
            TextSelection.collapsed(offset: phoneNumberController.text.length);
      }
    }
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height / 8),
      child: PaddingLeftAndRight(
        child: OnoInput(
          textEditingController: phoneNumberController,
          displayErrorMessage: (loginViewModel.textFieldValidationType ==
                  TextFieldValidationType.error)
              ? loginViewModel.phoneNumberErrorText
              : null,
          textFieldType: TextFieldType.PhoneNumber,
          functionCallBack: this.phoneNumberOnChanged,
        ),
      ),
    );
  }

  _buildButton() {
    return Padding(
      padding: paddingBetweenButton,
      child: ButtonWithTapEvent(
        text: signInStrings.continueText,
        tapCallBack: submitCallBack,
      ),
    );
  }

  _buildTermsAndConditions() {
    return Padding(
      padding: paddingSpaceFromHeadline2,
      child: TermsAndConditions(),
    );
  }
}
