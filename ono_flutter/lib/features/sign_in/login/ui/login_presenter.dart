import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:ono_clean_framework/ui/presenter.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/sign_in/bloc/sign_in_bloc.dart';
import 'package:ono_flutter/features/sign_in/login/model/login_view_model.dart';
import 'package:ono_flutter/features/sign_in/login/ui/login_screen.dart';
import 'package:ono_flutter/features/sign_in/otp/ui/otp_presenter.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

class LoginPresenter extends Presenter<SignInBloc, LoginViewModel> {
  @override
  Widget buildLoadingScreen(BuildContext context) {
    return GenericProgressbar();
  }

  @override
  void sendViewModelRequest(BuildContext context, SignInBloc bloc) {
    bloc.numberRequestEvent.launch();
  }

  @override
  Stream<LoginViewModel> getViewModelStream(
      BuildContext context, SignInBloc bloc) {
    return bloc.loginViewModelEvent.receive;
  }

  @override
  Widget buildScreen(
      BuildContext context, SignInBloc bloc, LoginViewModel viewModel) {
    if (viewModel.apiResponseType == ApiResponseType.successful) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        _navigateToOTPPresenter(context);
      });
    } else if (viewModel.apiResponseType == ApiResponseType.failure) {
      // bloc.resetAPIErrorCasesEvent.launch();
      // dismissPopsIfAvailable(context);
    }

    return LoginScreen(
      loginViewModel: viewModel,
      submitCallBack: () => _submitTap(bloc, context),
      phoneNumberOnChanged: (text) => _phoneNumberValueUpdate(text, bloc),
    );
  }

  @override
  Widget buildErrorScreen(BuildContext context) {
    return super.buildErrorScreen(context);
  }

  _submitTap(SignInBloc bloc, BuildContext context) {
    // _modalBottomSheetMenu(context);
     bloc.loginSubmitEvent.launch();
    // _navigateToOTPPresenter(context);
  }

  _phoneNumberValueUpdate(String text, SignInBloc bloc) {
    bloc.phoneNumberTextChangedUpdateEvent.send(text);
  }

  _navigateToOTPPresenter(BuildContext context) {
    // dismissProgressbar(context);
    Navigator.push(context,
    MaterialPageRoute(builder: (BuildContext context) => OtpPresenter(),maintainState: false));
  }
}
