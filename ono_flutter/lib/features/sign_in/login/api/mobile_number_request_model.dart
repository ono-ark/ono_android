import 'package:ono_clean_framework/service/ono_service.dart';

class MobileNumberRequestModel implements RequestModel {
  final DeviceInfoRequestModel deviceInfoRequestModel;
  final LocationRequestModel locationRequestModel;
  final bool locationAllow;
  final String mobile;
  final String langId;

  MobileNumberRequestModel(
      {this.deviceInfoRequestModel,
      this.locationRequestModel,
      this.locationAllow,
      this.mobile,this.langId});

  factory MobileNumberRequestModel.fromJson(Map<String, dynamic> json) {
    return MobileNumberRequestModel(
      deviceInfoRequestModel: json['deviceInfo'] != null
          ? DeviceInfoRequestModel.fromJson(json['deviceInfo'])
          : null,
      locationRequestModel: json['location'] != null
          ? LocationRequestModel.fromJson(json['location'])
          : null,
      locationAllow: json['locAllow'],
      mobile: json['mobile'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['locAllow'] = this.locationAllow;
    data['langId'] = this.langId;
    data['mobile'] = this.mobile;
    if (this.deviceInfoRequestModel != null) {
      data['deviceInfo'] = this.deviceInfoRequestModel.toJson();
    }
    if (this.locationRequestModel != null) {
      data['location'] = this.locationRequestModel.toJson();
    }
    return data;
  }
}

class LanguageRequestModel implements RequestModel {
  final String langId;
  final String langName;
  final String type;

  LanguageRequestModel({this.langId, this.langName, this.type});

  factory LanguageRequestModel.fromJson(Map<String, dynamic> json) {
    return LanguageRequestModel(
      langId: json['langId'],
      langName: json['langName'],
      type: json['type'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['langId'] = this.langId;
    data['langName'] = this.langName;
    data['type'] = this.type;
    return data;
  }
}

class DeviceInfoRequestModel implements RequestModel {
  final String model;
  final String os;
  final String version;

  DeviceInfoRequestModel({this.model, this.os, this.version});

  factory DeviceInfoRequestModel.fromJson(Map<String, dynamic> json) {
    return DeviceInfoRequestModel(
      model: json['model'],
      os: json['os'],
      version: json['version'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['model'] = this.model;
    data['os'] = this.os;
    data['version'] = this.version;
    return data;
  }
}

class LocationRequestModel implements RequestModel {
  final String latitude;
  final String longitude;

  LocationRequestModel({this.latitude, this.longitude});

  factory LocationRequestModel.fromJson(Map<String, dynamic> json) {
    return LocationRequestModel(
      latitude: json['latitude'],
      longitude: json['longitude'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }
}
