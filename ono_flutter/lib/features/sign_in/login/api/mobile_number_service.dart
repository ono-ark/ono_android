import 'package:ono_clean_framework/service/ono_service.dart';
import 'package:ono_clean_framework/service/rest_api.dart';
import 'package:ono_flutter/core/api/fuse_api/fuse_api.dart';

import 'mobile_number_response.dart';

class MobileNumberService extends OnoService {
  MobileNumberService(OnoServiceResponseHandler handler)
      : super(
          // url: OnoEnvironment.serverUrl + "account/quick-signup",
          url: FuseAPI.signInUrl,
          action: HttpAction.post,
          handler: handler,
          // options: FuseAPI.getFuseOptions(),
        );

  @override
  MobileNumberResponse parseJson(Map<String, dynamic> json) {
    return MobileNumberResponse.fromJson(json);
  }
}
