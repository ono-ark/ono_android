import 'package:ono_clean_framework/service/ono_service.dart';

class MobileNumberResponse implements ResponseModel {
  final Data data;

  MobileNumberResponse({this.data});

  factory MobileNumberResponse.fromJson(Map<String, dynamic> json) {
    return MobileNumberResponse(
      data: json['data'] != null ? Data.fromJson(json['data']) : null,
    );
  }
}

class Data implements ResponseModel {
  final String otpReqId;
  final dynamic onoId;

  Data({this.otpReqId, this.onoId});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(otpReqId: json['otpReqId'], onoId: json['onoId']);
  }
}
