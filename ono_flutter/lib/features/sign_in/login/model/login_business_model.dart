import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class LoginBusinessModel implements BusinessModel {
  String phoneNumberUpdate = '';
  String phoneNumberErrorText = '';
  String loginActivityId = '';
  ApiResponseType apiResponseType = ApiResponseType.none;
  TextFieldValidationType textFieldValidationType =
      TextFieldValidationType.none;
}

class DeviceInfoBusinessModel implements BusinessModel {
  String device = '';
  String deviceVersion = '';
  String model = '';
}
