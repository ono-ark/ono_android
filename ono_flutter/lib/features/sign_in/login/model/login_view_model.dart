

import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class LoginViewModel implements ViewModel {
  final String phoneNumberErrorText;
  final ApiResponseType apiResponseType;
  final String phoneNumberUpdate;
  final TextFieldValidationType textFieldValidationType;

  LoginViewModel(
      {this.phoneNumberErrorText,
      this.apiResponseType,
      this.phoneNumberUpdate,
      this.textFieldValidationType});
}
