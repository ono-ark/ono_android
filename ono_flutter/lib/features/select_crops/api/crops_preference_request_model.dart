import 'package:ono_clean_framework/service/ono_service.dart';

class CropsPreferenceRequestModel implements RequestModel {
  final int onoId;
  final List<String> prefIds;
  final String type;

  CropsPreferenceRequestModel({this.onoId, this.prefIds, this.type});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['onoId'] = this.onoId;
    data['prefIds'] = this.prefIds;
    data['type'] = this.type;
    return data;
  }
}
