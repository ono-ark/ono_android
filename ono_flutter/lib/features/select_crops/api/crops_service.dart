import 'package:ono_clean_framework/service/ono_service.dart';
import 'package:ono_clean_framework/service/rest_api.dart';
import 'package:ono_flutter/core/api/fuse_api/fuse_api.dart';
import 'package:ono_flutter/features/select_crops/api/crops_response_model.dart';

class CropsListService extends OnoService {
  CropsListService(OnoServiceResponseHandler handler)
      : super(
          url: FuseAPI.getCropsUrl,
          action: HttpAction.get,
          handler: handler,
          // options: FuseAPI.getFuseOptions(),
        );

  @override
  CropsListResponseModel parseJson(Map<String, dynamic> json) {
    return CropsListResponseModel.fromJson(json);
  }
}
