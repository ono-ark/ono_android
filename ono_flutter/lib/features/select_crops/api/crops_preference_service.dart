import 'package:ono_clean_framework/service/ono_service.dart';
import 'package:ono_clean_framework/service/rest_api.dart';
import 'package:ono_flutter/core/api/fuse_api/fuse_api.dart';
import 'package:ono_flutter/features/select_crops/api/crops_preference_response.dart';

class CropsPreferenceService extends OnoService {
  CropsPreferenceService(OnoServiceResponseHandler handler)
      : super(
          url: FuseAPI.cropsPreferenceUrl,
          action: HttpAction.post,
          handler: handler,
          // options: FuseAPI.getFuseOptions(),
        );

  @override
  CropsPreferenceResponseModel parseJson(Map<String, dynamic> json) {
    return CropsPreferenceResponseModel.fromJson(json);
  }
}
