import 'package:ono_clean_framework/service/ono_service.dart';

class CropsListResponseModel implements ResponseModel {
  final List<Data> data;

  CropsListResponseModel({this.data});

  factory CropsListResponseModel.fromJson(Map<String, dynamic> json) {
    return CropsListResponseModel(
      data: json['data'] != null
          ? (json['data'] as List).map((i) => Data.fromJson(i)).toList()
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data implements ResponseModel {
  final String cropId;
  final String cropName;
  final String imageUrl;
  final String type;
  final String units;

  Data({this.cropId, this.cropName, this.imageUrl, this.type, this.units});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      cropId: json['cropId'] ?? 0,
      cropName: json['cropName'] ?? '',
      imageUrl: json['imageUrl'] ?? '',
      type: json['type'] ?? '',
      units: json['units'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cropId'] = this.cropId;
    data['cropName'] = this.cropName;
    data['imageUrl'] = this.imageUrl;
    data['type'] = this.type;
    data['units'] = this.units;
    return data;
  }
}
