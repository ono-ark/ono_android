import 'package:ono_clean_framework/service/ono_service.dart';

class CropsPreferenceResponseModel implements ResponseModel {
  final Data data;
  final Error error;

  CropsPreferenceResponseModel({this.data, this.error});

  factory CropsPreferenceResponseModel.fromJson(Map<String, dynamic> json) {
    return CropsPreferenceResponseModel(
      data: json['data'] != null ? Data.fromJson(json['data']) : null,
      error: json['error'] != null ? Error.fromJson(json['error']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error.toJson();
    }
    return data;
  }
}

class Error implements ResponseModel {
  final String errorCode;
  final String errorDesc;

  Error({this.errorCode, this.errorDesc});

  factory Error.fromJson(Map<String, dynamic> json) {
    return Error(
      errorCode: json['errorCode'],
      errorDesc: json['errorDesc'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorCode'] = this.errorCode;
    data['errorDesc'] = this.errorDesc;
    return data;
  }
}

class Data implements ResponseModel {
  final String message;

  Data({this.message});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      message: json['message'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    return data;
  }
}
