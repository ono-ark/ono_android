import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class SelectCropsViewModel implements ViewModel {
  final List<CropViewModel> cropsListViewModel;
  final List<CropViewModel> selectedListViewModel;
  ApiResponseType apiResponseType;
  CropSelection cropSelection;
  final String image;

  SelectCropsViewModel(
      {this.cropsListViewModel,
      this.cropSelection,
      this.image,
      this.selectedListViewModel,
      this.apiResponseType});
}

class CropViewModel implements ViewModel {
  final String cropId;
  final String cropName;
  final String imageUrl;
  final String type;
  final String units;
  final bool isSelected;
  final String convertedImage;

  CropViewModel(
      {this.cropId,
      this.cropName,
      this.imageUrl,
      this.type,
      this.units,
      this.isSelected,
      this.convertedImage});
}
