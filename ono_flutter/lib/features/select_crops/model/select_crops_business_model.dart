import 'dart:typed_data';

import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class SelectCropsBusinessModel implements BusinessModel {
  List<CropBusinessModel> cropsListBusinessModel = [];
  List<CropBusinessModel> selectedListBusinessModel = [];
  ApiResponseType apiResponseType = ApiResponseType.none;
  CropSelection cropSelection = CropSelection.none;
  String image = '';
}

class CropBusinessModel implements BusinessModel {
  String cropId = '';
  String cropName = '';
  String imageUrl = '';
  String type = '';
  String units = '';
  String convertedImage = '';
  bool isSelected = false;
}
