import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ono_clean_framework/ui/presenter.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/dash_board/ui/dash_board_feature.dart';
import 'package:ono_flutter/features/dash_board/ui/dash_board_screen.dart';
import 'package:ono_flutter/features/select_crops/bloc/select_crops_bloc.dart';
import 'package:ono_flutter/features/select_crops/model/select_crops_view_model.dart';
import 'package:ono_flutter/features/select_crops/ui/select_crops_screen.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';
import 'package:ono_flutter/ui_utils/toast_message.dart';

class SelectCropsPresenter
    extends Presenter<SelectCropsBloc, SelectCropsViewModel> {
  @override
  Widget buildScreen(BuildContext context, SelectCropsBloc bloc,
      SelectCropsViewModel viewModel) {
    if (viewModel.apiResponseType == ApiResponseType.successful) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        // ToastMessage.showToastMessage(message: "Coming Soon");
        if (viewModel.cropSelection == CropSelection.edit) {
          Navigator.of(context, rootNavigator: true).pop(true);
        } else {
          _navigateToDashBoardFeature(context);
        }
      });
    } else if (viewModel.apiResponseType == ApiResponseType.failure) {}
    return SelectCropsScreen(
      selectCropsViewModel: viewModel,
      backTapEvent: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
      updateSelectedCropIdFunction: (value) {
        _updateSelectedCrop(viewModel, bloc, value);
      },
      continueTapEvent: () {
        _continueTapEvent(viewModel, context, bloc);
      },
    );
  }

  @override
  Widget buildLoadingScreen(BuildContext context) {
    return GenericProgressbar();
  }

  @override
  Stream<SelectCropsViewModel> getViewModelStream(
      BuildContext context, SelectCropsBloc bloc) {
    return bloc.selectCropsListViewModelEvent.receive;
  }

  @override
  void sendViewModelRequest(BuildContext context, SelectCropsBloc bloc) {
    bloc.requestCropsListEvent.launch();
  }

  _updateSelectedCrop(
      SelectCropsViewModel viewModel, SelectCropsBloc bloc, var value) {
    var list = viewModel.cropsListViewModel
        .where((element) => element.isSelected)
        .toList();
    if (list.length < 5) {
      bloc.updateSelectedCropEvent.send(value);
    } else if (list == null || list.isEmpty) {
      ToastMessage.showToastMessage(
          message: selectCropsStrings.selectAnyOneCropsMessage);
    } else {
      bool isExists = false;
      list.forEach((element) {
        if (element.cropId == value) {
          isExists = true;
        }
      });
      if (isExists) {
        bloc.updateSelectedCropEvent.send(value);
      } else {
        ToastMessage.showToastMessage(
            message: selectCropsStrings.selectUpToFiveCropsMessage);
      }
    }
  }

  _continueTapEvent(SelectCropsViewModel viewModel, BuildContext context,
      SelectCropsBloc bloc) {
    var list = viewModel.cropsListViewModel
        .where((element) => element.isSelected)
        .toList();
    if (list == null || list.isEmpty) {
      ToastMessage.showToastMessage(
        message: selectCropsStrings.selectAnyOneCropsMessage,
      );
    } else {
      bloc.requestCropsPreferenceEvent.launch();
    }
  }

  void _navigateToDashBoardFeature(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => DashboardFeatureWidget(),
            maintainState: false));
  }
}
