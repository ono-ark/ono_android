import 'package:flutter/material.dart';
import 'package:ono_clean_framework/bloc/bloc_provider.dart';
import 'package:ono_flutter/common/features/navigator/navigator.dart';
import 'package:ono_flutter/features/select_crops/bloc/select_crops_bloc.dart';
import 'package:ono_flutter/features/select_crops/ui/select_crops_presenter.dart';

class SelectCropsFeatureWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: SelectCropsBloc(),
      child: FeatureNavigator(
        sectionChild: SelectCropsPresenter(),
      ),
    );
  }
}
