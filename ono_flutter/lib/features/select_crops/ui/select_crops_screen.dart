import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/select_crops/model/select_crops_view_model.dart';
import 'package:ono_flutter/features/select_crops/strings/strings.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

var selectCropsStrings = SelectCropsStrings();

class SelectCropsScreen extends StatelessWidget {
  final SelectCropsViewModel selectCropsViewModel;
  final Function updateSelectedCropIdFunction;
  final VoidCallback backTapEvent;
  final VoidCallback continueTapEvent;
  SelectCropsScreen(
      {Key key,
      this.selectCropsViewModel,
      this.updateSelectedCropIdFunction,
      this.backTapEvent,
      this.continueTapEvent})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColor,
      body: _buildBody(context),
    );
  }

  _buildButton() {
    return Column(
      children: [
        ButtonWithTapEvent(
          text: signInStrings.continueText,
          tapCallBack: continueTapEvent,
        ),
        SizedBox(
          height: 30,
        )
      ],
    );
  }

  _buildBody(context) {
    var list = selectCropsViewModel.cropsListViewModel
        .where((element) => element.isSelected)
        .toList();
    return Column(
      children: [
        _buildAppBar(list),
        _buildSelectedCrops(),
        _buildCropsList(context),
        _buildButton(),
      ],
    );
  }

  _buildCropsList(context) {
    // var mediaQueryData = MediaQuery.of(context);
    // final double widthScreen = mediaQueryData.size.width;
    // final double appBarHeight = kToolbarHeight;
    // final double paddingTop = mediaQueryData.padding.top;
    // final double paddingBottom = mediaQueryData.padding.bottom;
    // final double heightScreen = mediaQueryData.size.height/1.9;
    return Expanded(
      child: PaddingLeftAndRight(
        child: new Container(
          child: new GridView.count(
            crossAxisCount: 3,
            // childAspectRatio: (widthScreen / heightScreen),
            childAspectRatio: (120 / 140),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            //  padding: EdgeInsets.symmetric(vertical: 20),
            children: List.generate(
                selectCropsViewModel.cropsListViewModel.length, (index) {
              var item = selectCropsViewModel.cropsListViewModel[index];
              return GestureDetector(
                onTap: () {
                  updateSelectedCropIdFunction(item.cropId);
                },
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Stack(
                        children: [
                          CircleAvatar(
                            radius: 40,
                            backgroundColor: (item.isSelected)
                                ? headLine2Color
                                : backGroundColor,
                            child: FutureImageLoadingWidget(
                              imageUrl: selectCropsViewModel
                                  .cropsListViewModel[index].imageUrl,
                              cropId: selectCropsViewModel
                                  .cropsListViewModel[index].cropId,
                            ),
                          ),
                          // Material(
                          //   shape: CircleBorder(),
                          //   elevation: 3.0,
                          //   borderOnForeground: true,
                          //   child: FutureImageLoadingWidget(
                          //     imageUrl: selectCropsViewModel
                          //         .cropsListViewModel[index].imageUrl,
                          //     cropId: selectCropsViewModel
                          //         .cropsListViewModel[index].cropId,
                          //   ),
                          // ),
                          // Container(
                          //   width: 84,
                          //   height: 84,
                          //   // child: CachedNetworkImage(
                          //   //   cacheKey: selectCropsViewModel
                          //   //       .cropsListViewModel[index].cropId,
                          //   //   imageUrl: selectCropsViewModel
                          //   //       .cropsListViewModel[index].imageUrl,
                          //   //   imageBuilder: (context, imageProvider) {
                          //   //     return Container(
                          //   //       decoration: BoxDecoration(
                          //   //         image: DecorationImage(
                          //   //           image: imageProvider,
                          //   //           fit: BoxFit.fill,
                          //   //         ),
                          //   //       ),
                          //   //     );
                          //   //   },
                          //   //   placeholder: (context, url) =>
                          //   //       CircularProgressIndicator(),
                          //   //   errorWidget: (context, url, error) =>
                          //   //       Icon(Icons.error),
                          //   // ),
                          //   child: FutureImageLoadingWidget(
                          //     imageUrl: selectCropsViewModel
                          //         .cropsListViewModel[index].imageUrl,
                          //     cropId: selectCropsViewModel
                          //         .cropsListViewModel[index].cropId,
                          //   ),
                          //   decoration: (item.isSelected)
                          //       ? BoxDecoration(
                          //           color: Colors.white,
                          //           shape: BoxShape.circle,
                          //           border: Border.all(
                          //               color: headLine2Color, width: 2.5))
                          //       : BoxDecoration(),
                          // ),
                          (item.isSelected)
                              ? Positioned(
                                  right: 5,
                                  // top: 5,
                                  child: new Container(
                                    padding: EdgeInsets.all(2),
                                    decoration: BoxDecoration(
                                      color: selectedTickMarkColor,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Icon(
                                      Icons.check,
                                      color: whiteColor,
                                      size: 15,
                                    ),
                                  ),
                                )
                              : IgnoreWidget(),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Expanded(
                          flex: 1,
                          child: GenericTextWithStyle(
                            textStyle: (item.isSelected)
                                ? theme.headline2.copyWith(fontSize: 12)
                                : theme.headline4.copyWith(
                                    color: termsAndConditionsColor,
                                    fontSize: 12),
                            text: item.cropName,
                            textAlign: TextAlign.center,
                          )),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }

  _buildSelectedCrops() {
    if (selectCropsViewModel.selectedListViewModel.length > 0) {
      return Container(
        color: headLine2Color,
        // margin: EdgeInsets.only(left: 5, right: 5),
        height: 105,
        child: new ListView(
          scrollDirection: Axis.horizontal,
          children: List.generate(
              selectCropsViewModel.selectedListViewModel.length, (index) {
            String name =
                selectCropsViewModel.selectedListViewModel[index].cropName;

            if (name.contains('-')) {
              List list = selectCropsViewModel
                  .selectedListViewModel[index].cropName
                  .split('-');
              name = list[0].toString() + '\n' + '-' + list[1];
            }

            return Padding(
              padding: const EdgeInsets.only(left: 5.0, right: 5),
              child: Column(
                children: [
                  // SizedBox(
                  //   height: 15,
                  // ),
                  Container(
                    width: 55,
                    height: 55,
                    child: FutureImageLoadingWidget(
                      imageUrl: selectCropsViewModel
                          .selectedListViewModel[index].imageUrl,
                      cropId: selectCropsViewModel
                          .selectedListViewModel[index].cropId,
                    ),
                    // child: Image.memory(
                    //   base64Decode(selectCropsViewModel
                    //       .selectedListViewModel[index].convertedImage),
                    //   fit: BoxFit.fill,
                    // ),
                    // child: CachedNetworkImage(
                    //   imageUrl: selectCropsViewModel
                    //       .cropsListViewModel[index].imageUrl,
                    //   imageBuilder: (context, imageProvider) => Container(
                    //     decoration: BoxDecoration(
                    //       image: DecorationImage(
                    //         image: imageProvider,
                    //         fit: BoxFit.fill,
                    //       ),
                    //     ),
                    //   ),
                    //   placeholder: (context, url) => Container(
                    //     child: CircularProgressIndicator(),
                    //     height: 30,
                    //     width: 30,
                    //   ),
                    //   errorWidget: (context, url, error) => Icon(Icons.error),
                    // ),
                    margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
                    // child: Cache,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(left: 0, right: 0),
                      child: GenericTextWithStyle(
                        text: name,
                        textAlign: TextAlign.center,
                        textStyle: theme.headline4
                            .copyWith(color: whiteColor, fontSize: 12),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
        ),
      );
    } else {
      return IgnoreWidget();
    }
  }

  _buildAppBar(List<CropViewModel> list) {
    return AppBarWithMultipleOptions(
      title: selectCropsStrings.selectCrops,
      subtitle: selectCropsStrings.chooseCrops.toString().replaceAll(
            "0",
            list.length.toString(),
          ),
      profileImage: selectCropsViewModel.image,
      isHideTrailing: false,
      backButtonEvent: selectCropsViewModel.cropSelection == CropSelection.edit
          ? backTapEvent
          : null,
    );
  }
}
