import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/core/api/local_storage/local_storage_api.dart';
import 'package:ono_flutter/core/api/response_handler.dart';
import 'package:ono_flutter/core/api/user_info/user_info.dart';
import 'package:ono_flutter/features/select_crops/api/crops_preference_request_model.dart';
import 'package:ono_flutter/features/select_crops/api/crops_preference_response.dart';
import 'package:ono_flutter/features/select_crops/api/crops_preference_service.dart';
import 'package:ono_flutter/features/select_crops/api/crops_response_model.dart';
import 'package:ono_flutter/features/select_crops/api/crops_service.dart';
import 'package:ono_flutter/features/select_crops/model/select_crops_business_model.dart';
import 'package:ono_flutter/features/select_crops/model/select_crops_view_model.dart';

class SelectCropsBloc extends ResponseHandlerBloc implements Bloc {
  //local variables;

  bool _isEdit = false;

  //Business Model
  SelectCropsBusinessModel _selectCropsBusinessModel;

  //Service
  CropsListService _cropsListService;
  CropsPreferenceService _cropsPreferenceService;

  //Events
  EventPipe requestCropsListEvent = EventPipe();
  BroadcastPipe<SelectCropsViewModel> selectCropsListViewModelEvent =
      BroadcastPipe<SelectCropsViewModel>();
  Pipe<String> updateSelectedCropEvent = Pipe<String>();
  EventPipe requestCropsPreferenceEvent = EventPipe();

  SelectCropsBloc(
      {SelectCropsBusinessModel selectCropsBusinessModel,
      CropsListService cropsListService,
      CropsPreferenceService cropsPreferenceService}) {
    //object creation of business models
    _selectCropsBusinessModel =
        selectCropsBusinessModel ?? SelectCropsBusinessModel();

    //object creation of service
    _cropsListService = cropsListService ?? CropsListService(this);
    _cropsPreferenceService =
        cropsPreferenceService ?? CropsPreferenceService(this);

    //events
    requestCropsListEvent.listen(_requestCropsListFromApi);
    updateSelectedCropEvent.receive.listen(_updateSelectedCropId);
    requestCropsPreferenceEvent.listen(_makeCropsPreferenceServiceRequest);
  }

  _getCropSelectionStatus() {
    return UserInfoAPI.cropSelection();
  }

  _updateUserStatus() {
    if (_getCropSelectionStatus() != CropSelection.edit) {
      _isEdit = true;
      UserInfoAPI.setUserAppStatus(userAppStatus: UserAppStatus.cropsSelection);
    } else {
      _isEdit = false;
    }
  }

  _requestCropsListFromApi() async {
    _updateUserStatus();
    CropsListResponseModel cropsListResponseModel =
        await _cropsListService.request();
    if (cropsListResponseModel != null &&
        cropsListResponseModel.data.length > 0) {
      await LocalStorageAPI().clearDataFromStorage();
      cropsListResponseModel.data.forEach((element) {
        CropBusinessModel cropBusinessModel = CropBusinessModel();
        cropBusinessModel.cropId = element.cropId;
        cropBusinessModel.cropName = element.cropName;
        cropBusinessModel.imageUrl = element.imageUrl;
        cropBusinessModel.type = element.type;
        cropBusinessModel.units = element.units;
        _selectCropsBusinessModel.cropsListBusinessModel.add(cropBusinessModel);
      });
      await _updateSelectedPreferenceList();
      _selectCropsBusinessModel.image = await UserInfoAPI.getUserProfilePic();
      _passViewModel();
    } else {
      //TODO handle no response
    }
  }

  _updateSelectedPreferenceList() async {
    if (_getCropSelectionStatus() == CropSelection.edit) {
      String list = await UserInfoAPI.getUserPreferencesList();
      if (list.isNotEmpty) {
        var selectedList = json.decode(list.toString());
        if (selectedList.length > 0) {
          for (int i = 0;
              i < _selectCropsBusinessModel.cropsListBusinessModel.length;
              i++) {
            for (int j = 0; j < selectedList.length; j++) {
              if (_selectCropsBusinessModel.cropsListBusinessModel[i].cropId ==
                  selectedList[j]) {
                _selectCropsBusinessModel.cropsListBusinessModel[i].isSelected =
                    true;
                _selectCropsBusinessModel.selectedListBusinessModel
                    .add(_selectCropsBusinessModel.cropsListBusinessModel[i]);
              }
            }
          }
        }
      }
    }
  }

  _prePareCropsViewModel() {
    List<CropViewModel> list = [];
    _selectCropsBusinessModel.cropsListBusinessModel.forEach((element) {
      list.add(CropViewModel(
          cropId: element.cropId,
          cropName: element.cropName,
          imageUrl: element.imageUrl,
          type: element.type,
          units: element.units,
          isSelected: element.isSelected));
    });

    List<CropViewModel> selectedList = [];
    _selectCropsBusinessModel.selectedListBusinessModel.forEach((element) {
      selectedList.add(CropViewModel(
          cropId: element.cropId,
          cropName: element.cropName,
          imageUrl: element.imageUrl,
          type: element.type,
          units: element.units,
          isSelected: element.isSelected,
          convertedImage: element.convertedImage));
    });

    return SelectCropsViewModel(
        cropsListViewModel: list,
        image: _selectCropsBusinessModel.image,
        apiResponseType: _selectCropsBusinessModel.apiResponseType,
        cropSelection: _getCropSelectionStatus(),
        selectedListViewModel: selectedList);
  }

  _passViewModel() {
    selectCropsListViewModelEvent.send(_prePareCropsViewModel());
  }

  _updateSelectedCropId(String id) async {
    _selectCropsBusinessModel.cropsListBusinessModel.forEach((element) {
      if (element.cropId == id) {
        if (element.isSelected) {
          element.isSelected = false;
        } else {
          element.isSelected = true;
        }
      }
    });
    if (_getCropSelectionStatus() == CropSelection.edit) {
      _isEdit = true;
      _updateCropsPreferenceList();
    } else {
      _updateCropsPreferenceList();
    }
    _passViewModel();
  }

  _updateCropsPreferenceList() {
    // _selectCropsBusinessModel.selectedListBusinessModel = [];
    for (int i = 0;
        i < _selectCropsBusinessModel.cropsListBusinessModel.length;
        i++) {
      if (_selectCropsBusinessModel.cropsListBusinessModel[i].isSelected) {
        if (!_getModelIsExistsOrNot(
            _selectCropsBusinessModel.cropsListBusinessModel[i].cropId)) {
          _selectCropsBusinessModel.selectedListBusinessModel
              .add(_selectCropsBusinessModel.cropsListBusinessModel[i]);
        }
      } else {
        if (_getModelIsExistsOrNot(
            _selectCropsBusinessModel.cropsListBusinessModel[i].cropId)) {
          _selectCropsBusinessModel.selectedListBusinessModel.removeWhere(
              (element) =>
                  element.cropId ==
                  _selectCropsBusinessModel.cropsListBusinessModel[i].cropId);
        }
      }
    }
  }

  _getModelIsExistsOrNot(String cropId) {
    bool value = false;
    _selectCropsBusinessModel.selectedListBusinessModel.forEach((element) {
      if (element.cropId == cropId) {
        value = true;
      }
    });
    return value;
  }

  _updateCropsPreferenceListByEdit() {
    for (int i = 0;
        i < _selectCropsBusinessModel.cropsListBusinessModel.length;
        i++) {
      for (int j = 0;
          j < _selectCropsBusinessModel.selectedListBusinessModel.length;
          j++) {
        if (_selectCropsBusinessModel.cropsListBusinessModel[i].isSelected) {
          if (_selectCropsBusinessModel.cropsListBusinessModel[i].cropId !=
              _selectCropsBusinessModel.selectedListBusinessModel[j].cropId) {
            _selectCropsBusinessModel.selectedListBusinessModel
                .add(_selectCropsBusinessModel.cropsListBusinessModel[i]);
          } else {
            _isEdit = true;
          }
        }
      }
    }
  }

  _makeCropsPreferenceServiceRequest() async {
    List<String> selectedPreferencesIdList = [];
    _selectCropsBusinessModel.selectedListBusinessModel.forEach((element) {
      selectedPreferencesIdList.add(element.cropId);
    });
    String id = await UserInfoAPI.getOnoId();
    print('iddd${id}');
    if (_isEdit) {
      CropsPreferenceRequestModel cropsPreferenceRequestModel =
          CropsPreferenceRequestModel(
              type: 'CROP',
              prefIds: selectedPreferencesIdList,
              onoId: int.parse(id));
      CropsPreferenceResponseModel cropsPreferenceResponseModel =
          await _cropsPreferenceService.request(
              data: cropsPreferenceRequestModel);
      if (cropsPreferenceResponseModel != null &&
          cropsPreferenceResponseModel.data != null) {
        UserInfoAPI.setUserCropPreferenceList(
            jsonEncode(selectedPreferencesIdList));
        _selectCropsBusinessModel.apiResponseType = ApiResponseType.successful;
      }
    }
    _passViewModel();
    UserInfoAPI.setCropsSelection(cropSelection: CropSelection.create);
  }

  @override
  void dispose() {
    requestCropsListEvent.dispose();
    selectCropsListViewModelEvent.dispose();
    updateSelectedCropEvent.dispose();
    requestCropsPreferenceEvent.dispose();
    super.dispose();
  }
}
