class SelectCropsStrings {
  SelectCropsStrings();
  get selectCrops => 'Select Crops';
  get chooseCrops => 'Choose up to 5 crops, 0/5 selected.';
  get selectUpToFiveCropsMessage =>
      'You can only select up to five crops. Please re-tap on one of the selected crops if you prefer to swap';
  get selectAnyOneCropsMessage =>
      'Please select the at least one crop to continue';
}
