import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/core/api/response_handler.dart';
import 'package:ono_flutter/core/api/user_info/user_info.dart';
import 'package:ono_flutter/features/user_session/model/user_session_business_model.dart';
import 'package:ono_flutter/features/user_session/model/user_session_view_model.dart';

class UserSessionBloc extends ResponseHandlerBloc implements Bloc {
  //Business model
  UserSessionBusinessModel _userSessionBusinessModel;

  //Events
  EventPipe requestUserSessionStatusEventPipe = EventPipe();
  BroadcastPipe<UserSessionViewModel> userSessionViewModelStreamPipe =
      BroadcastPipe<UserSessionViewModel>();

  UserSessionBloc({UserSessionBusinessModel userSessionBusinessModel}) {
    _userSessionBusinessModel =
        userSessionBusinessModel ?? UserSessionBusinessModel();
    //events listening
    requestUserSessionStatusEventPipe.listen(_getStatusOfUserSession);
  }

  _getStatusOfUserSession() async {
    //  UserAppStatus userAppStatus;
    UserAppStatus userAppStatus = await UserInfoAPI.getUserAppStatus();
    _userSessionBusinessModel.userAppStatus =
        userAppStatus ?? UserAppStatus.language;
    _passUserSessionViewModel();
  }

  _prePareUserSessionViewModel() {
    return UserSessionViewModel(
        userAppStatus: _userSessionBusinessModel.userAppStatus);
  }

  _passUserSessionViewModel() {
    userSessionViewModelStreamPipe.send(_prePareUserSessionViewModel());
  }

  @override
  void dispose() {
    requestUserSessionStatusEventPipe.dispose();
    userSessionViewModelStreamPipe.dispose();
    super.dispose();
  }
}
