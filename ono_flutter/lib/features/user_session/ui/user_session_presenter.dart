import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:ono_clean_framework/ui/presenter.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/dash_board/ui/dash_board_feature.dart';
import 'package:ono_flutter/features/language_selection/ui/language_feature_widget.dart';
import 'package:ono_flutter/features/location/ui/feature_widget.dart';
import 'package:ono_flutter/features/profile_details/ui/feature_widget.dart';
import 'package:ono_flutter/features/select_crops/ui/feature_widget.dart';
import 'package:ono_flutter/features/sign_in/ui/feature_widget.dart';
import 'package:ono_flutter/features/user_session/bloc/user_session_bloc.dart';
import 'package:ono_flutter/features/user_session/model/user_session_view_model.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

class UserSessionPresenter
    extends Presenter<UserSessionBloc, UserSessionViewModel> {
  @override
  Widget buildLoadingScreen(BuildContext context) {
    return GenericProgressbar();
  }

  @override
  Widget buildScreen(BuildContext context, UserSessionBloc bloc,
      UserSessionViewModel viewModel) {
    _navigateToRequiredWidget(context, viewModel);
    return GenericProgressbar();
  }

  @override
  Stream<UserSessionViewModel> getViewModelStream(
      BuildContext context, UserSessionBloc bloc) {
    return bloc.userSessionViewModelStreamPipe.receive;
  }

  @override
  void sendViewModelRequest(BuildContext context, UserSessionBloc bloc) {
    bloc.requestUserSessionStatusEventPipe.launch();
  }

  _navigateToRequiredWidget(
      BuildContext context, UserSessionViewModel viewModel) {
    Widget widget;
    switch (viewModel.userAppStatus) {
      case UserAppStatus.language:
        widget = LanguageFeatureWidget();
        break;
      case UserAppStatus.location:
        widget = LocationFeatureWidget();
        break;
      case UserAppStatus.signIn:
        widget = SignInFeatureWidget();
        break;
      case UserAppStatus.profile:
        widget = ProfileDetailsFeatureWidget();
        break;
      case UserAppStatus.cropsSelection:
        widget = SelectCropsFeatureWidget();
        break;
      case UserAppStatus.dashBoard:
        widget = DashboardFeatureWidget();
        break;
      case UserAppStatus.none:
        widget = LanguageFeatureWidget();
        break;
    }
    SchedulerBinding.instance.addPostFrameCallback((_) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => widget),
          (route) => false);
    });
  }
}
