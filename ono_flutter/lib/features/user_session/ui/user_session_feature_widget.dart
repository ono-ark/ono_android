import 'package:flutter/material.dart';
import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_flutter/features/user_session/bloc/user_session_bloc.dart';
import 'package:ono_flutter/features/user_session/ui/user_session_presenter.dart';

class UserSessionFeatureWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      child: UserSessionPresenter(),
      bloc: UserSessionBloc(),
    );
  }
}
