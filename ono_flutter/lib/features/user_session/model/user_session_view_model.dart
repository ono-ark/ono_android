import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class UserSessionViewModel implements ViewModel {
  final UserAppStatus userAppStatus;

  UserSessionViewModel({this.userAppStatus});
}
