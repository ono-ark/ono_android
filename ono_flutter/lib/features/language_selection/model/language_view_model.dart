import 'package:ono_clean_framework/model.dart';

class LanguageViewModel extends ViewModel {
  final List<LanguageListTileViewModel> languageListTileViewModel;

  LanguageViewModel({this.languageListTileViewModel})
      : assert(languageListTileViewModel != null);
}

class LanguageListTileViewModel extends ViewModel {
  final String langId;
  final String langName;
  final String type;
  final bool isSelected;
  final String langCode;

  LanguageListTileViewModel(
      {this.langId, this.langName, this.type, this.isSelected, this.langCode});
}
