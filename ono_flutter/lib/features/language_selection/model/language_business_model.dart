import 'package:ono_clean_framework/model.dart';

class LanguageBusinessModel extends BusinessModel {
  List<LanguageTileBusinessModel> languageTileList = [];
}

class LanguageTileBusinessModel extends BusinessModel {
  String langId = '';
  String langName = '';
  String langNameInEnglish = '';
  String type = '';
  String langCode = '';
  bool isSelected = false;
}
