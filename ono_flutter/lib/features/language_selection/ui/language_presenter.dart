import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ono_clean_framework/ui/presenter.dart';
import 'package:ono_flutter/features/language_selection/bloc/language_bloc.dart';
import 'package:ono_flutter/features/language_selection/model/language_view_model.dart';
import 'package:ono_flutter/features/language_selection/ui/language_screen.dart';
import 'package:ono_flutter/features/location/ui/feature_widget.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

class LanguagePresenter extends Presenter<LanguageBloc, LanguageViewModel> {
  @override
  void sendViewModelRequest(BuildContext context, LanguageBloc bloc) {
    bloc.launchLanguageSelectionEvent.launch();
  }

  @override
  Stream<LanguageViewModel> getViewModelStream(
      BuildContext context, LanguageBloc bloc) {
    return bloc.languageSelectionViewModelPipe.receive;
  }

  @override
  Widget buildLoadingScreen(BuildContext context) {
    return GenericProgressbar();
  }

  @override
  Widget buildScreen(
      BuildContext context, LanguageBloc bloc, LanguageViewModel viewModel) {
    return LanguageScreen(
        languageViewModel: viewModel,
        nextTapCallBack: () => _nextTapEventNavigation(context, bloc),
        selectedLanguageTap: (id) {
          bloc.selectedLanguageIndexPipe.send(id);
        });
  }

  _nextTapEventNavigation(BuildContext context, LanguageBloc bloc) {
    bloc.setLanguagePreference.launch();
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => LocationFeatureWidget()),
        (route) => false);
  }
}
