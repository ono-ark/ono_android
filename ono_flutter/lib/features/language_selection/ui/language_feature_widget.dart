import 'package:flutter/material.dart';
import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_clean_framework/bloc/bloc_provider.dart';
import 'package:ono_flutter/common/features/navigator/navigator.dart';
import 'package:ono_flutter/features/language_selection/bloc/language_bloc.dart';
import 'package:ono_flutter/features/language_selection/ui/language_presenter.dart';

class LanguageFeatureWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: LanguageBloc(),
      child: FeatureNavigator(sectionChild: LanguagePresenter()),
    );
  }
}
