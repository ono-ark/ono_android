import 'package:flutter/material.dart';
import 'package:ono_flutter/features/language_selection/language_strings.dart';
import 'package:ono_flutter/features/language_selection/model/language_view_model.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

var languageStrings = LanguageStrings();

class LanguageScreen extends StatelessWidget {
  final LanguageViewModel languageViewModel;
  final Function selectedLanguageTap;
  final VoidCallback nextTapCallBack;
  final VoidCallback tapEventCount;

  LanguageScreen(
      {this.languageViewModel,
      this.selectedLanguageTap,
      this.nextTapCallBack,
      this.tapEventCount});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: backGroundColor,
        body: Column(
          children: [
            _buildAppbar(),
            _buildTitle(),
            _buildLanguageTiles(context),
            _buildButton(),
          ],
        ));
  }

  _buildTitle() {
    return PaddingLeftAndRight(
      child: Padding(
        padding: paddingSpaceFromAppbar,
        child: GenericTextWithStyle(
          text: languageStrings.selectLanguageTitle,
          textStyle: theme.headline2,
        ),
      ),
    );
  }

  _buildToastWidget(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      child: GestureDetector(
        onLongPress: () {
          //_buildToastMessage(context);
        },
        child: PaddingBetweenWidgets(
          child: ListTile(onTap: tapEventCount),
        ),
      ),
    );
  }

  _buildAppbar() {
    return Card(
      elevation: 3,
      margin: EdgeInsets.all(0.0),
      child: Container(
        height: 119,
        width: double.infinity,
        color: whiteColor,
        padding: EdgeInsets.only(
          top: 65,
        ),
        child: PaddingLeftAndRight(
          child: GenericTextWithStyle(
            text: languageStrings.hello,
            textStyle: theme.headline1,
          ),
        ),
      ),
    );
  }

  _buildLanguageTiles(BuildContext context) {
    return Expanded(
      child: PaddingLeftAndRight(
        child: new Container(
          child: new GridView.count(
            crossAxisCount: 2,
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            mainAxisSpacing: 20,
            children: List.generate(
                languageViewModel.languageListTileViewModel.length, (index) {
              var item = languageViewModel.languageListTileViewModel[index];
              return GestureDetector(
                onTap: () {
                  selectedLanguageTap(item.langId);
                },
                child: Column(
                  children: [
                    Container(
                      width: 110,
                      height: 110,
                      decoration: BoxDecoration(
                          color:
                              (item.isSelected) ? headLine2Color : whiteColor,
                          shape: BoxShape.circle,
                          border: Border.all(
                              color: (item.isSelected)
                                  ? headLine2Color
                                  : priceBorderColor,
                              width: 2.5)),
                      child: Center(
                        child: GenericTextWithStyle(
                          textAlign: TextAlign.center,
                          textStyle: theme.headline4.copyWith(
                              color: (item.isSelected)
                                  ? whiteColor
                                  : termsAndConditionsColor,
                              fontSize: 45),
                          text: item.langCode,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    GenericTextWithStyle(
                      textStyle: (item.isSelected)
                          ? theme.headline2.copyWith(fontSize: 18)
                          : theme.headline4.copyWith(
                              color: termsAndConditionsColor, fontSize: 18),
                      text: item.langName,
                    ),
                  ],
                ),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }

  _buildButton() {
    return Column(
      children: [
        Padding(
          padding: paddingBetweenButton,
          child: ButtonWithTapEvent(
            text: signInStrings.continueText,
            tapCallBack: nextTapCallBack,
          ),
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
