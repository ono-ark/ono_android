import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/core/api/response_handler.dart';
import 'package:ono_flutter/core/api/user_info/user_info.dart';
import 'package:ono_flutter/features/language_selection/api/language_response.dart';
import 'package:ono_flutter/features/language_selection/api/language_service.dart';
import 'package:ono_flutter/features/language_selection/model/language_business_model.dart';
import 'package:ono_flutter/features/language_selection/model/language_view_model.dart';
import 'package:ono_flutter/features/language_selection/ui/language_screen.dart';

class LanguageBloc extends ResponseHandlerBloc implements Bloc {
  int _count = 0;
  //Business model
  LanguageBusinessModel _languageBusinessModel;

  //Events
  EventPipe launchLanguageSelectionEvent = EventPipe();
  BroadcastPipe<LanguageViewModel> languageSelectionViewModelPipe =
      BroadcastPipe<LanguageViewModel>();
  Pipe<String> selectedLanguageIndexPipe = Pipe<String>();
  EventPipe setLanguagePreference = EventPipe();
  EventPipe updateTapCountEvent = EventPipe();

  //Service
  LanguageService _languageService;

  LanguageBloc({LanguageService languageService}) {
    //object of business models
    _languageBusinessModel = LanguageBusinessModel();

    //services
    _languageService = languageService ?? LanguageService(this);

    launchLanguageSelectionEvent.listen(loadLanguageOptions);
    selectedLanguageIndexPipe.receive.listen(_updateLanguageSelection);
    setLanguagePreference.listen(_updateLanguagePreferences);
  }

  _updateLanguageSelection(String id) {
    _languageBusinessModel.languageTileList.forEach((element) {
      if (id == element.langId) {
        element.isSelected = true;
      } else {
        element.isSelected = false;
      }
    });
    _passLanguageViewModel();
  }

  void loadLanguageOptions() async {
    UserInfoAPI.setUserAppStatus(userAppStatus: UserAppStatus.language);
    LanguageResponseModel languageResponseModel =
        await _languageService.request();

    if (languageResponseModel != null && languageResponseModel.data != null) {
      _languageBusinessModel.languageTileList = [];
      languageResponseModel.data.forEach((element) {
        _languageBusinessModel.languageTileList.add(LanguageTileBusinessModel()
          ..langId = element.langId
          ..type = element.type
          ..langNameInEnglish = element.langName
          ..langName = _getLanguageName(name: element.langName, value: true)
          ..langCode = _getLanguageName(name: element.langName)
          ..isSelected = false);
      });
    } else {
      //Todo handle method
    }

    _languageBusinessModel.languageTileList[0].isSelected = true;

    _passLanguageViewModel();
  }

  LanguageViewModel _prepareLanguageSelectionViewModel() {
    List<LanguageListTileViewModel> languageTileListViewModel = [];
    _languageBusinessModel.languageTileList.forEach((element) {
      languageTileListViewModel.add(LanguageListTileViewModel(
          langName: element.langName,
          langId: element.langId,
          isSelected: element.isSelected,
          langCode: element.langCode));
    });

    return LanguageViewModel(
        languageListTileViewModel: languageTileListViewModel);
  }

  _passLanguageViewModel() {
    languageSelectionViewModelPipe.send(_prepareLanguageSelectionViewModel());
  }

  void _updateLanguagePreferences() {
    LanguageTileBusinessModel languageTileBusinessModel = _languageBusinessModel
        .languageTileList
        .where((element) => element.isSelected == true)
        .single;
    switch (languageTileBusinessModel.langName.toString().toLowerCase()) {
      case "english":
        UserInfoAPI.setLanguagePreference("en");
        break;
      case "hindi":
        UserInfoAPI.setLanguagePreference("Hi");
        break;
      case "telugu":
        UserInfoAPI.setLanguagePreference("Te");
        break;
      case "tamil":
        UserInfoAPI.setLanguagePreference("Ta");
        break;
      case "kannada":
        UserInfoAPI.setLanguagePreference("Ka");
        break;
      default:
        UserInfoAPI.setLanguagePreference("en");
        break;
    }
    UserInfoAPI.setLanguagePreferenceId(languageTileBusinessModel.langId);
  }

  _getLanguageName({String name, bool value = false}) {
    switch (name.toLowerCase()) {
      case "english":
        if (value) {
          return languageStrings.english;
        } else {
          return languageStrings.english.toString().substring(0, 2);
        }
        break;
      case "hindi":
        if (value) {
          return languageStrings.hindi;
        } else {
          return languageStrings.hindi.toString().substring(0, 2);
        }
        break;
      case "telugu":
        if (value) {
          return languageStrings.telugu;
        } else {
          return languageStrings.telugu.toString().substring(0, 2);
        }
        break;
      case "tamil":
        if (value) {
          return languageStrings.tamil;
        } else {
          return languageStrings.tamil.toString().substring(0, 1);
        }

        break;
      case "kannada":
        if (value) {
          return languageStrings.kannada;
        } else {
          return languageStrings.kannada.toString().substring(0, 1);
        }
        break;
      default:
        return languageStrings.english;
        break;
    }
  }

  @override
  void dispose() {
    launchLanguageSelectionEvent.dispose();
    languageSelectionViewModelPipe.dispose();
    selectedLanguageIndexPipe.dispose();
    setLanguagePreference.dispose();
    super.dispose();
  }
}
