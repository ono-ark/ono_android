class LanguageStrings {
  LanguageStrings();

  get selectLanguageTitle => 'Select your preferred language';
  get hello => 'Hello!';

  get english => 'English';

  get hindi => 'हिंदी';

  get telugu => 'తెలుగు';

  get tamil => 'தமிழ்';
  get kannada => 'ಕನ್ನಡ';
}
