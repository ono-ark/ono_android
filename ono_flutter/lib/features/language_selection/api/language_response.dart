import 'package:ono_clean_framework/service/ono_service.dart';

class LanguageResponseModel implements ResponseModel {
  final List<Data> data;

  LanguageResponseModel({this.data});

  factory LanguageResponseModel.fromJson(Map<String, dynamic> json) {
    return LanguageResponseModel(
      data: json['data'] != null
          ? (json['data'] as List).map((i) => Data.fromJson(i)).toList()
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data implements ResponseModel {
  final String langId;
  final String langName;
  final String type;

  Data({this.langId, this.langName, this.type});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      langId: json['langId'] ?? '',
      langName: json['langName'] ?? '',
      type: json['type'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['langId'] = this.langId;
    data['langName'] = this.langName;
    data['type'] = this.type;
    return data;
  }
}
