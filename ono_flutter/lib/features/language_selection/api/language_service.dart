import 'package:ono_clean_framework/service/ono_service.dart';
import 'package:ono_clean_framework/service/rest_api.dart';
import 'package:ono_flutter/core/api/fuse_api/fuse_api.dart';

import 'language_response.dart';

class LanguageService extends OnoService {
  LanguageService(OnoServiceResponseHandler handler)
      : super(
          url: FuseAPI.languageUrl,
          action: HttpAction.get,
          handler: handler,
          // options: FuseAPI.getFuseOptions(),
        );

  @override
  LanguageResponseModel parseJson(Map<String, dynamic> json) {
    return LanguageResponseModel.fromJson(json);
  }
}

class LanguageRequestData implements RequestModel {
  final String language;

  LanguageRequestData({this.language});

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = {"prefeType": language};
    return data;
  }
}
