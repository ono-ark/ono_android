import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:ono_clean_framework/Log.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/env/environment.dart';
import 'package:ono_flutter/features/profile_details/model/profile_details_business_model.dart';

class ProfileDetailsService {
  static postProfileDetailsRequest(
      {ProfileDetailsBusinessModel profileDetailsBusinessModel,
      String userAgFinId}) async {
    // string to uri
    var uri = Uri.parse('${OnoEnvironment.serverUrl + 'account/profile'}');

    // create multipart request
    var request = new http.MultipartRequest("POST", uri);

    request.files.add(await http.MultipartFile.fromPath(
        "profilePic", profileDetailsBusinessModel.profilePhoto.path));

    request.fields['fullName'] = profileDetailsBusinessModel.fullName;
    request.fields['gender'] =
        profileDetailsBusinessModel.genderType == GenderType.male
            ? 'Male'
            : 'Female';
    request.fields['userType'] =
        profileDetailsBusinessModel.userType == UserType.farmer
            ? 'Farmer'
            : 'Buyer';

    request.fields['onoId'] = userAgFinId;

    Log.d('Profile Details Service request',
        '${request.fields.toString() + request.files.toString() + request.toString()}');

    var response = await request.send();
    Log.d('Profile Details Service statuscode', '${response.statusCode}');
    if (response.statusCode != 200) {
      response.stream.transform(utf8.decoder).listen((value) {
        Log.d('Upload Documents failure Response', '${value.toString()}');
        // handler.responseData('');
        // showAPIErrorWidget(
        //     response: FileUploadRequestModel.errorResponseModel(
        //         responseCode: response.statusCode,
        //         data: JsonParsing.parsingJsonDecode(value)));
      });
    } else {
      response.stream.transform(utf8.decoder).listen((value) {
        Log.d('Profile Details Success Response', '${value.toString()}');
      });
    }
    return response;
  }
}
