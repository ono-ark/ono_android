import 'package:flutter/material.dart';
import 'package:ono_clean_framework/bloc/bloc_provider.dart';
import 'package:ono_flutter/common/features/navigator/navigator.dart';
import 'package:ono_flutter/features/profile_details/bloc/profile_details_bloc.dart';
import 'package:ono_flutter/features/profile_details/user_type/ui/user_type_presenter.dart';

class ProfileDetailsFeatureWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: ProfileDetailsBloc(),
      child: FeatureNavigator(
        sectionChild: UserTypePresenter(),
        maintainState: true,
      ),
    );
  }
}
