class ProfileDetailsStrings {
  ProfileDetailsStrings();
  get userType => 'User Type';
  get iAma => 'I am a';
  get farmer => 'Farmer';
  get buyer => 'Buyer';
  get personalInfo => 'Personal Info';
  get capturePhoto => 'CAPTURE PHOTO';
  get takeAPhoto => 'Take a photo with front camera';
  get photoFromGallery => 'SELECT PHOTO FROM YOUR GALLERY';
  get fullName => 'Full Name*';
  get male => 'Male';
  get female => 'Female';
}
