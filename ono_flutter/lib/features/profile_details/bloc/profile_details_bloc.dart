import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/core/api/image_cropper/image_cropper.dart';
import 'package:ono_flutter/core/api/image_picker_api/image_picker_api.dart';
import 'package:ono_flutter/core/api/response_handler.dart';
import 'package:ono_flutter/core/api/user_info/user_info.dart';
import 'package:ono_flutter/features/profile_details/api/profile_details_service.dart';
import 'package:ono_flutter/features/profile_details/model/profile_details_business_model.dart';
import 'package:ono_flutter/features/profile_details/model/profile_details_view_model.dart';
import 'package:ono_flutter/ui_utils/validations.dart';

class ProfileDetailsBloc extends ResponseHandlerBloc implements Bloc {
  //Business Model
  ProfileDetailsBusinessModel _profileDetailsBusinessModel;

  //Events

  //user type
  EventPipe userTypeRequestEvent = EventPipe();
  BroadcastPipe<ProfileDetailsViewModel> userTypeViewModelEvent =
      BroadcastPipe<ProfileDetailsViewModel>();
  Pipe<UserType> updateUserType = Pipe<UserType>();

  //Personal Info
  EventPipe personalInfoRequestEvent = EventPipe();
  BroadcastPipe<ProfileDetailsViewModel> personalInfoViewModelEvent =
      BroadcastPipe<ProfileDetailsViewModel>();
  Pipe<PickImageMode> requestImageOption = Pipe<PickImageMode>();
  Pipe<String> updateProfilePicturePathEvent = Pipe<String>();
  Pipe<String> fullNameTextChangedUpdateEvent = Pipe<String>();
  Pipe<GenderType> genderTypeValueChangedUpdateEvent = Pipe<GenderType>();
  EventPipe continueTapEvent = EventPipe();
  EventPipe backTapUiUpdateEvent = EventPipe();

  //Picker and cropper api
  ImagePickerApi _imagePickerApi;
  ImageCropperApi _imageCropperApi;

  ProfileDetailsBloc(
      {ProfileDetailsBusinessModel profileDetailsBusinessModel,
      ImageCropperApi imageCropperApi,
      ImagePickerApi imagePickerApi}) {
    //Objects Creation of business models
    _profileDetailsBusinessModel =
        profileDetailsBusinessModel ?? ProfileDetailsBusinessModel();

    _imageCropperApi = imageCropperApi ?? ImageCropperApi();
    _imagePickerApi = imagePickerApi ?? ImagePickerApi();

    //user type
    userTypeRequestEvent.listen(_passUserTypeViewModel);
    updateUserType.receive.listen(_updateUserTypeIntoBusinessModel);

    //personal Info
    personalInfoRequestEvent.listen(_passPersonalInfoViewModel);
    requestImageOption.receive.listen(_requestImageOption);
    updateProfilePicturePathEvent.receive.listen(_updateProfilePhoto);
    fullNameTextChangedUpdateEvent.receive.listen(_validateFullName);
    genderTypeValueChangedUpdateEvent.receive.listen(_updateGenderType);
    continueTapEvent.listen(_continueTapEventValidation);
    backTapUiUpdateEvent.listen(_backTapUiUpdate);
  }

  //user type
  _preparePersonalInfoViewModel() {
    return ProfileDetailsViewModel(
        userType: _profileDetailsBusinessModel.userType,
        profilePhoto: _profileDetailsBusinessModel.profilePhoto,
        fullName: _profileDetailsBusinessModel.fullName,
        genderType: _profileDetailsBusinessModel.genderType,
        fullNameErrorText: _profileDetailsBusinessModel.fullNameErrorText,
        apiResponseType: _profileDetailsBusinessModel.apiResponseType,
        profileDetailsStatus:
            _profileDetailsBusinessModel.profileDetailsStatus);
  }

  _passUserTypeViewModel() {
    UserInfoAPI.setUserAppStatus(userAppStatus: UserAppStatus.profile);
    userTypeViewModelEvent.send(_preparePersonalInfoViewModel());
  }

  _updateUserTypeIntoBusinessModel(UserType userType) {
    _profileDetailsBusinessModel.userType = userType;
    _passUserTypeViewModel();
  }

  //personal info
  _passPersonalInfoViewModel() {
    personalInfoViewModelEvent.send(_preparePersonalInfoViewModel());
  }

  _requestImageOption(PickImageMode pickImageMode) async {
    File file;
    switch (pickImageMode) {
      case PickImageMode.camera:
        var pickedFile = await _imagePickerApi.pickImageFromCamera();
        if (pickedFile != null) {
          file = await _imageCropperApi.cropImageFromCameraOrGallery(
              path: pickedFile.path);
        }
        break;
      case PickImageMode.gallery:
        PickedFile pickedFile = await _imagePickerApi.pickImageFromGallery();
        if (pickedFile != null) {
          file = await _imageCropperApi.cropImageFromCameraOrGallery(
              path: pickedFile.path);
        }
        break;
      case PickImageMode.none:
        return;
        break;
    }

    if (file != null) {
      _profileDetailsBusinessModel.profilePhoto = file;
      List<int> imageBytes =
          _profileDetailsBusinessModel.profilePhoto.readAsBytesSync();
      String imageB64 = base64Encode(imageBytes);
      UserInfoAPI.setUserProfilePic(imageB64);
      _passPersonalInfoViewModel();
    }
  }

  _updateProfilePhoto(String path) {
    _profileDetailsBusinessModel.profilePhoto = File(path);
    _passPersonalInfoViewModel();
  }

  void _validateFullName(String event) {
    _profileDetailsBusinessModel.fullName = event;
    _profileDetailsBusinessModel.fullNameErrorText =
        _validateName(_profileDetailsBusinessModel.fullName);
    if (_profileDetailsBusinessModel.fullNameErrorText == null) {
      _profileDetailsBusinessModel.profileDetailsStatus =
          ProfileDetailsStatus.none;
    }
    _passPersonalInfoViewModel();
  }

  void _updateGenderType(GenderType event) {
    _profileDetailsBusinessModel.genderType = event;
    _passPersonalInfoViewModel();
  }

  _continueTapEventValidation() async {
    if (_profileDetailsBusinessModel.profilePhoto.path != null &&
        _profileDetailsBusinessModel.profilePhoto.path.isNotEmpty &&
        _profileDetailsBusinessModel.profileDetailsStatus ==
            ProfileDetailsStatus.profilePhoto) {
      _profileDetailsBusinessModel.profileDetailsStatus =
          ProfileDetailsStatus.fullName;
    } else if (_profileDetailsBusinessModel.profileDetailsStatus ==
        ProfileDetailsStatus.fullName) {
      _profileDetailsBusinessModel.fullNameErrorText =
          _validateName(_profileDetailsBusinessModel.fullName);
    } else {
      StreamedResponse streamedResponse =
          await _makeProfileDetailsServiceRequest();
      if (streamedResponse.statusCode == 200) {
        List<int> imageBytes =
            _profileDetailsBusinessModel.profilePhoto.readAsBytesSync();
        String imageB64 = base64Encode(imageBytes);
        UserInfoAPI.setUserProfilePic(imageB64);
        _profileDetailsBusinessModel.apiResponseType =
            ApiResponseType.successful;
      } else {
        _profileDetailsBusinessModel.apiResponseType = ApiResponseType.failure;
      }
    }
    _passPersonalInfoViewModel();
  }

  _makeProfileDetailsServiceRequest() async {
    StreamedResponse streamedResponse =
        await ProfileDetailsService.postProfileDetailsRequest(
            profileDetailsBusinessModel: _profileDetailsBusinessModel,
            userAgFinId: await UserInfoAPI.getOnoId());
    return streamedResponse;
  }

  _backTapUiUpdate() {
    _profileDetailsBusinessModel.fullNameErrorText = null;
    _profileDetailsBusinessModel.fullName = '';
    _profileDetailsBusinessModel.profileDetailsStatus =
        ProfileDetailsStatus.profilePhoto;
    _passPersonalInfoViewModel();
  }

  //Validations
  String _validateName(String text) {
    return Validations.validateName(text);
  }

  @override
  void dispose() {
    //userType
    userTypeRequestEvent.dispose();
    updateUserType.dispose();
    userTypeViewModelEvent.dispose();

    //Personal Info
    personalInfoRequestEvent.dispose();
    personalInfoViewModelEvent.dispose();
    requestImageOption.dispose();
    updateProfilePicturePathEvent.dispose();
    fullNameTextChangedUpdateEvent.dispose();
    genderTypeValueChangedUpdateEvent.dispose();
    continueTapEvent.dispose();
    backTapUiUpdateEvent.dispose();

    super.dispose();
  }
}
