import 'package:flutter/material.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/profile_details/model/profile_details_view_model.dart';
import 'package:ono_flutter/features/profile_details/strings/profile_strings.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

var profileStrings = ProfileDetailsStrings();

class UserTypeScreen extends StatelessWidget {
  final ProfileDetailsViewModel profileDetailsViewModel;
  final VoidCallback continueTapEvent;
  final Function userTypeSelectTapEvent;

  const UserTypeScreen(
      {Key key,
      this.profileDetailsViewModel,
      this.continueTapEvent,
      this.userTypeSelectTapEvent})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColor,
      body: _buildBody(context),
    );
  }

  _buildBody(context) {
    return Column(
      children: [
        Expanded(
          child: ListView(
            children: [
              _buildAppbar(),
              _buildTitle(),
              _buildUserTypes(context),
            ],
          ),
        ),
        _buildButton(context),
      ],
    );
  }

  _buildAppbar() {
    return AppBarWidget(
      text: profileStrings.userType,
    );
  }

  _buildTitle() {
    return Align(
      alignment: Alignment.topLeft,
      child: PaddingLeftAndRight(
        child: Padding(
          padding: paddingSpaceFromAppbar,
          child: GenericTextWithStyle(
            text: profileStrings.iAma,
            textStyle: theme.headline2,
          ),
        ),
      ),
    );
  }

  _buildUserTypes(context) {
    return PaddingLeftAndRight(
      child: Padding(
        padding: paddingSpaceFromAppbar,
        child: Container(
          alignment: Alignment.bottomCenter,
          margin: EdgeInsets.fromLTRB(
              0, MediaQuery.of(context).size.height / 8, 0, 0),
          child: Column(
            children: [
              GestureDetector(
                  onTap: () {
                    userTypeSelectTapEvent(UserType.farmer);
                  },
                  child: _selectedWidget(
                      name: profileStrings.farmer, userType: UserType.farmer)),
              GestureDetector(
                  onTap: () {
                    userTypeSelectTapEvent(UserType.buyer);
                  },
                  child: Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: _selectedWidget(
                          name: profileStrings.buyer,
                          userType: UserType.buyer))),
            ],
          ),
        ),
      ),
    );
  }

  _selectedWidget({String name, UserType userType}) {
    Widget selectedWidget;
    BoxDecoration boxDecoration;
    if (userType == profileDetailsViewModel.userType) {
      selectedWidget = _buildSelectedWidget(name: name);
      boxDecoration = BoxDecoration(
          color: headLine2Color, borderRadius: BorderRadius.circular(5));
    } else {
      selectedWidget = GenericTextWithStyle(
        text: name,
        textStyle: theme.headline2.copyWith(color: headLine1Color),
      );
      boxDecoration = BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: Colors.black12));
    }
    return Container(
      width: double.infinity,
      height: 56,
      alignment: Alignment.center,
      decoration: boxDecoration,
      child: selectedWidget,
    );
  }

  _buildSelectedWidget({String name}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          Icons.check_circle,
          color: whiteColor,
        ),
        SizedBox(
          width: 10,
        ),
        GenericTextWithStyle(
          text: name,
          textStyle: theme.headline2.copyWith(color: whiteColor),
        ),
      ],
    );
  }

  _buildButton(BuildContext context) {
    return Column(
      children: [
        ButtonWithTapEvent(
          text: signInStrings.continueText,
          tapCallBack: continueTapEvent,
        ),
        SizedBox(
          height: 30,
        )
      ],
    );
  }
}
