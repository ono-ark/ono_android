import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:ono_clean_framework/ui/presenter.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/profile_details/bloc/profile_details_bloc.dart';
import 'package:ono_flutter/features/profile_details/model/profile_details_view_model.dart';
import 'package:ono_flutter/features/profile_details/personal_info/ui/personal_info_presenter.dart';
import 'package:ono_flutter/features/profile_details/user_type/ui/user_type_screen.dart';

class UserTypePresenter
    extends Presenter<ProfileDetailsBloc, ProfileDetailsViewModel> {
  @override
  Widget buildScreen(BuildContext context, ProfileDetailsBloc bloc,
      ProfileDetailsViewModel viewModel) {
    return UserTypeScreen(
      profileDetailsViewModel: viewModel,
      userTypeSelectTapEvent: (value) {
        bloc.updateUserType.send(value);
      },
      continueTapEvent: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => PersonalInfoPresenter()));
        if (viewModel.userType != UserType.none) {
        } else {}
      },
    );
  }

  @override
  Stream<ProfileDetailsViewModel> getViewModelStream(
      BuildContext context, ProfileDetailsBloc bloc) {
    return bloc.userTypeViewModelEvent.receive;
  }

  @override
  void sendViewModelRequest(BuildContext context, ProfileDetailsBloc bloc) {
    bloc.userTypeRequestEvent.launch();
  }
}
