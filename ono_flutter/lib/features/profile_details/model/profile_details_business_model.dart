import 'dart:io';

import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class ProfileDetailsBusinessModel extends BusinessModel {
  UserType userType = UserType.farmer;
  File profilePhoto = File('');
  String fullName = '';
  GenderType genderType = GenderType.male;
  String fullNameErrorText = '';
  ProfileDetailsStatus profileDetailsStatus = ProfileDetailsStatus.profilePhoto;
  ApiResponseType apiResponseType = ApiResponseType.none;
}
