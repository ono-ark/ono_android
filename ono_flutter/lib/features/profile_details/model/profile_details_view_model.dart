import 'dart:io';

import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class ProfileDetailsViewModel extends ViewModel {
  final UserType userType;
  final File profilePhoto;
  final String fullName;
  final GenderType genderType;
  final String fullNameErrorText;
  final ProfileDetailsStatus profileDetailsStatus;
  final ApiResponseType apiResponseType;

  ProfileDetailsViewModel(
      {this.userType,
      this.profilePhoto,
      this.fullName,
      this.genderType,
      this.fullNameErrorText,
      this.profileDetailsStatus,
      this.apiResponseType});
}
