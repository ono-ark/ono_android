import 'package:flutter/material.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/profile_details/model/profile_details_view_model.dart';
import 'package:ono_flutter/features/profile_details/user_type/ui/user_type_screen.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_text_field.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

class PersonalInfoScreen extends StatelessWidget {
  final ProfileDetailsViewModel profileDetailsViewModel;
  final Function pickImageOption;
  final Function fullNameTextOnChanged;
  final Function radioValueChanged;
  final VoidCallback continueTapEvent;
  final VoidCallback backTapEvent;
  final TextEditingController fullNameController = TextEditingController();

  PersonalInfoScreen(
      {Key key,
      this.profileDetailsViewModel,
      this.pickImageOption,
      this.continueTapEvent,
      this.fullNameTextOnChanged,
      this.radioValueChanged,
      this.backTapEvent})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColor,
      body: _buildBody(context),
    );
  }

  _buildBody(context) {
    return Column(
      children: [
        Expanded(
          child: ListView(
            children: [
              _buildAppbar(),
              _buildDynamicUI(),
            ],
          ),
        ),
        _buildButton(context),
      ],
    );
  }

  _buildDynamicUI() {
    switch (profileDetailsViewModel.profileDetailsStatus) {
      case ProfileDetailsStatus.profilePhoto:
        return Column(
          children: [
            _buildProfilePhoto(),
            _buildTakePhotoText(),
          ],
        );
        break;
      case ProfileDetailsStatus.fullName:
      case ProfileDetailsStatus.none:
        return Column(
          children: [
            _buildProfilePhoto(),
            Align(
              alignment: Alignment.topLeft,
              child: PaddingLeftAndRight(
                child: PaddingSpaceFromHeadline2Widget(
                  child: GenericTextWithStyle(
                    textStyle: theme.headline4,
                    text: profileStrings.fullName,
                  ),
                ),
              ),
            ),
            _buildFullName(),
            _buildRadioButtons(),
            SizedBox(
              height: 30,
            ),
          ],
        );
        break;
      default:
        return IgnoreWidget();
    }
  }

  _buildRadioButtons() {
    int groupIdValue = 1;
    Color activeColor = termsAndConditionsColor;
    Color inActiveColor = termsAndConditionsColor;
    TextStyle maleTextStyle = theme.headline4.copyWith(fontSize: 18);
    TextStyle femaleTextStyle = theme.headline4.copyWith(fontSize: 18);
    switch (profileDetailsViewModel.genderType) {
      case GenderType.male:
        groupIdValue = 1;
        activeColor = headLine2Color;
        maleTextStyle = theme.headline2.copyWith(color: activeColor);
        break;
      case GenderType.female:
        groupIdValue = 2;
        inActiveColor = headLine2Color;
        femaleTextStyle = theme.headline2.copyWith(color: inActiveColor);
        break;
      case GenderType.none:
        return;
        break;
    }
    return PaddingSpaceFromHeadline2Widget(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            children: [
              Radio(
                value: 1,
                groupValue: groupIdValue,
                activeColor: activeColor,
                onChanged: (val) {
                  radioValueChanged(GenderType.male);
                },
              ),
              GenericTextWithStyle(
                text: profileStrings.male,
                textStyle: maleTextStyle,
              ),
            ],
          ),
          SizedBox(
            width: 30,
          ),
          Row(
            children: [
              Radio(
                value: 2,
                groupValue: groupIdValue,
                activeColor: inActiveColor,
                onChanged: (val) {
                  radioValueChanged(GenderType.female);
                },
              ),
              GenericTextWithStyle(
                text: profileStrings.female,
                textStyle: femaleTextStyle,
              ),
            ],
          ),
        ],
      ),
    );
  }

  _buildAppbar() {
    return AppBarWidget(
      text: profileStrings.personalInfo,
      showBackIcon: true,
      voidCallback: backTapEvent,
    );
  }

  _buildProfilePhoto() {
    return Center(
      child: GestureDetector(
        onTap: () {
          pickImageOption(PickImageMode.camera);
        }, // choose image on click of profile
        child: Container(
          margin: paddingSpaceFromAppbar,
          width: 200,
          height: 200,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.blue,
              image: DecorationImage(
                  image: profileDetailsViewModel ==
                          null //profilePhoto which is File object
                      ? AssetImage("assets/images/profile_placeholder.png")
                      : FileImage(
                          profileDetailsViewModel.profilePhoto), // picked file
                  fit: BoxFit.fill)),
        ),
      ),
    );
  }

  _buildTakePhotoText() {
    return Container(
      margin: paddingSpaceFromAppbar,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.camera_alt,
                color: capturePhotoTextColor,
              ),
              SizedBox(
                width: 5,
              ),
              GestureDetector(
                onTap: () {
                  pickImageOption(PickImageMode.camera);
                },
                child: GenericTextWithStyle(
                  text: profileStrings.capturePhoto,
                  textStyle: theme.headline2
                      .copyWith(color: capturePhotoTextColor, fontSize: 16),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          GenericTextWithStyle(
            text: profileStrings.takeAPhoto,
            textStyle: theme.headline4
                .copyWith(color: takeAPhotoTextColor, fontSize: 13),
          ),
          GestureDetector(
            onTap: () {
              pickImageOption(PickImageMode.gallery);
            },
            child: Column(
              children: [
                Padding(
                  padding: paddingSpaceFromAppbar,
                  child: GenericTextWithStyle(
                    text: profileStrings.photoFromGallery + " *",
                    textStyle: theme.headline2.copyWith(
                        color: photoFromGalleryTextColor, fontSize: 13),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Icon(
                    Icons.keyboard_arrow_down,
                    color: photoFromGalleryTextColor,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  _buildFullName() {
    if (profileDetailsViewModel.fullName != null) {
      if (profileDetailsViewModel.fullName.isNotEmpty) {
        fullNameController.text = profileDetailsViewModel.fullName;
        fullNameController.selection =
            TextSelection.collapsed(offset: fullNameController.text.length);
      }
    }
    return PaddingSpaceFromHeadline2Widget(
      child: PaddingLeftAndRight(
        child: OnoInput(
          textEditingController: fullNameController,
          displayErrorMessage:
              (profileDetailsViewModel.fullNameErrorText != null &&
                      profileDetailsViewModel.fullNameErrorText.isNotEmpty)
                  ? profileDetailsViewModel.fullNameErrorText
                  : null,
          textFieldType: TextFieldType.name,
          functionCallBack: this.fullNameTextOnChanged,
        ),
      ),
    );
  }

  _buildButton(BuildContext context) {
    if (profileDetailsViewModel.profilePhoto != null &&
        profileDetailsViewModel.profilePhoto.path.isNotEmpty) {
      return Column(
        children: [
          ButtonWithTapEvent(
            text: signInStrings.continueText,
            tapCallBack: continueTapEvent,
          ),
          SizedBox(
            height: 30,
          )
        ],
      );
    } else {
      return IgnoreWidget();
    }
  }
}
