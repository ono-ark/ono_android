import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ono_clean_framework/ui/presenter.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/profile_details/bloc/profile_details_bloc.dart';
import 'package:ono_flutter/features/profile_details/model/profile_details_view_model.dart';
import 'package:ono_flutter/features/profile_details/personal_info/ui/personal_info_screen.dart';
import 'package:ono_flutter/features/select_crops/ui/feature_widget.dart';

class PersonalInfoPresenter
    extends Presenter<ProfileDetailsBloc, ProfileDetailsViewModel> {
  @override
  Widget buildScreen(BuildContext context, ProfileDetailsBloc bloc,
      ProfileDetailsViewModel viewModel) {
    if (viewModel.apiResponseType == ApiResponseType.successful) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => SelectCropsFeatureWidget()));
      });
    }
    return PersonalInfoScreen(
      profileDetailsViewModel: viewModel,
      backTapEvent: () {
        if (viewModel.profileDetailsStatus ==
            ProfileDetailsStatus.profilePhoto) {
          Navigator.pop(context);
        } else {
          bloc.backTapUiUpdateEvent.launch();
        }
      },
      continueTapEvent: () {
        // Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //         builder: (BuildContext context) => SelectCropsFeatureWidget()));
         bloc.continueTapEvent.launch();
      },
      pickImageOption: (data) {
        _pickImageOption(data, context, bloc);
      },
      fullNameTextOnChanged: (text) {
        bloc.fullNameTextChangedUpdateEvent.send(text);
      },
      radioValueChanged: (value) {
        bloc.genderTypeValueChangedUpdateEvent.send(value);
      },
    );
  }

  @override
  Stream<ProfileDetailsViewModel> getViewModelStream(
      BuildContext context, ProfileDetailsBloc bloc) {
    return bloc.personalInfoViewModelEvent.receive;
  }

  @override
  void sendViewModelRequest(BuildContext context, ProfileDetailsBloc bloc) {
    bloc.personalInfoRequestEvent.launch();
  }

  _pickImageOption(data, context, ProfileDetailsBloc bloc) async {
    bloc.requestImageOption.send(data);
  }
}
