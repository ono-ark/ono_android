class DashBoardStrings {
  DashBoardStrings();
  get dailyPrice => 'Daily Price';
  get nearByMarkets => 'Nearby Markets';
  get edit => 'Edit';
  get locateMe => 'Locate Me';
  get selectYourLocation => 'Select Your Location';
  get locationIsNotEntered => 'Location is not entered';
  get onoProvidingCorrect => 'ONO Providing Correct';
  get submit => 'SUBMIT';
  get close => 'CLOSE';
  get noDataDetected =>
      'Sorry, we don’t have data for the location you selected';
  get submitARequest => 'Submit a Request';
  get nearByMarketsValue => 'Nearby Markets';
  get min => 'Min';
  get max => 'Max';
  get rs => 'Rs.';
  get preferredCrops => 'Preferred Crops';
  get otherCrops => 'Other Crops';
  get noCropsFound=>'No Crops Found';
}
