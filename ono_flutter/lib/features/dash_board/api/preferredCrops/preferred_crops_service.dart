import 'package:ono_clean_framework/service/ono_service.dart';
import 'package:ono_clean_framework/service/rest_api.dart';
import 'package:ono_flutter/core/api/fuse_api/fuse_api.dart';
import 'file:///E:/ono/ono_flutter/lib/features/dash_board/api/daily_price/daily_price_response.dart';
import 'package:ono_flutter/features/dash_board/api/preferredCrops/preferred_crops_response.dart';

class PreferredCropsService extends OnoService {
  PreferredCropsService(OnoServiceResponseHandler handler)
      : super(
          url: FuseAPI.preferredCropsUrl,
          action: HttpAction.get,
          handler: handler,
        );

  @override
  PreferredCropsResponseModel parseJson(Map<String, dynamic> json) {
    return PreferredCropsResponseModel.fromJson(json);
  }
}
