import 'package:ono_clean_framework/service/ono_service.dart';

class PreferredCropsResponseModel implements ResponseModel {
  final List<Data> data;
  final Error error;

  PreferredCropsResponseModel({this.data, this.error});

  factory PreferredCropsResponseModel.fromJson(Map<String, dynamic> json) {
    return PreferredCropsResponseModel(
      data: json['data'] != null
          ? (json['data'] as List).map((i) => Data.fromJson(i)).toList()
          : null,
      error: json['error'] != null ? Error.fromJson(json['error']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error.toJson();
    }
    return data;
  }
}

class Error implements ResponseModel {
  final String errorCode;
  final String errorDesc;

  Error({this.errorCode, this.errorDesc});

  factory Error.fromJson(Map<String, dynamic> json) {
    return Error(
      errorCode: json['errorCode'],
      errorDesc: json['errorDesc'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorCode'] = this.errorCode;
    data['errorDesc'] = this.errorDesc;
    return data;
  }
}

class Data implements ResponseModel {
  final String cropName;
  final String prefId;
  final String prefType;
  final String image;

  Data({this.cropName, this.prefId, this.prefType, this.image});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
        cropName: json['cropName'] ?? '',
        prefId: json['prefId'] ?? '',
        prefType: json['prefType'] ?? '',
        image: json['image'] ?? '');
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cropName'] = this.cropName;
    data['prefId'] = this.prefId;
    data['prefType'] = this.prefType;
    return data;
  }
}
