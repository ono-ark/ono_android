import 'package:ono_clean_framework/service/ono_service.dart';

class PreferredRequestModel implements RequestModel {
  final String onoId;
  final String type;

  PreferredRequestModel({
    this.onoId,
    this.type,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['onoId'] = this.onoId;
    data['type'] = this.type;
    return data;
  }
}
