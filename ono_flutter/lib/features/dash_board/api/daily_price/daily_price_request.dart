import 'package:ono_clean_framework/service/ono_service.dart';

class DailyPriceRequestModel implements RequestModel {
  final String onoId;
  final String lat;
  final String long;

  DailyPriceRequestModel({this.onoId, this.lat, this.long});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['onoId'] = this.onoId;
    data['lat'] = this.lat;
    // = '12.9150918107861';
    // this.lat;
    data['lon'] =
        // '77.5733848370938';
        this.long;
    return data;
  }
}
