import 'package:ono_clean_framework/service/ono_service.dart';

class DailyPriceResponseModel implements ResponseModel {
  final Data data;
  final Error error;

  DailyPriceResponseModel({this.data, this.error});

  factory DailyPriceResponseModel.fromJson(Map<String, dynamic> json) {
    return DailyPriceResponseModel(
      data: json['data'] != null ? Data.fromJson(json['data']) : null,
      error: json['error'] != null ? Error.fromJson(json['error']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error.toJson();
    }
    return data;
  }
}

class Data implements ResponseModel {
  final dynamic onoId;
  final List<PreferredCropPrice> preferedCropPrice;

  Data({this.onoId, this.preferedCropPrice});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      onoId: json['onoId'],
      preferedCropPrice: json['preferedCropPrice'] != null
          ? (json['preferedCropPrice'] as List)
              .map((i) => PreferredCropPrice.fromJson(i))
              .toList()
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['onoId'] = this.onoId;
    if (this.preferedCropPrice != null) {
      data['preferedCropPrice'] =
          this.preferedCropPrice.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PreferredCropPrice implements ResponseModel {
  final String cropId;
  final String cropName;
  final String imageUrl;
  final List<NearestMarket> nearestMarkets;

  PreferredCropPrice(
      {this.cropId, this.cropName, this.imageUrl, this.nearestMarkets});

  factory PreferredCropPrice.fromJson(Map<String, dynamic> json) {
    return PreferredCropPrice(
      cropId: json['cropId'],
      cropName: json['cropName'],
      imageUrl: json['imageUrl'],
      nearestMarkets: json['nearestMarkets'] != null
          ? (json['nearestMarkets'] as List)
              .map((i) => NearestMarket.fromJson(i))
              .toList()
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cropId'] = this.cropId;
    data['cropName'] = this.cropName;
    data['imageUrl'] = this.imageUrl;
    if (this.nearestMarkets != null) {
      data['nearestMarkets'] =
          this.nearestMarkets.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class NearestMarket implements ResponseModel {
  final Market market;
  final List<Price> prices;

  NearestMarket({this.market, this.prices});

  factory NearestMarket.fromJson(Map<String, dynamic> json) {
    return NearestMarket(
      market: json['market'] != null ? Market.fromJson(json['market']) : null,
      prices: json['prices'] != null
          ? (json['prices'] as List).map((i) => Price.fromJson(i)).toList()
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.market != null) {
      data['market'] = this.market.toJson();
    }
    if (this.prices != null) {
      data['prices'] = this.prices.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Market implements ResponseModel {
  final AddressResponseModel address;
  final double distance;
  final String marketId;
  final String marketName;

  Market({this.address, this.distance, this.marketId, this.marketName});

  factory Market.fromJson(Map<String, dynamic> json) {
    return Market(
      address:
          json['address'] != null ? AddressResponseModel.fromJson(json['address']) : null,
      distance: json['distance'],
      marketId: json['marketId'],
      marketName: json['marketName'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['distance'] = this.distance;
    data['marketId'] = this.marketId;
    data['marketName'] = this.marketName;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    return data;
  }
}

class AddressResponseModel implements ResponseModel {
  final String addressId;
  final String addressLine;
  final String city;
  final dynamic latitude;
  final dynamic longitude;
  final dynamic pincode;
  final String state;

  AddressResponseModel(
      {this.addressId,
      this.addressLine,
      this.city,
      this.latitude,
      this.longitude,
      this.pincode,
      this.state});

  factory AddressResponseModel.fromJson(Map<String, dynamic> json) {
    return AddressResponseModel(
      addressId: json['addressId'],
      addressLine: json['addressLine'],
      city: json['city'],
      latitude: json['latitude'],
      longitude: json['longitude'],
      pincode: json['pincode'],
      state: json['state'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['addressId'] = this.addressId;
    data['addressLine'] = this.addressLine;
    data['city'] = this.city;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['pincode'] = this.pincode;
    data['state'] = this.state;
    return data;
  }
}

class Price implements ResponseModel {
  final String date;
  final dynamic max;
  final dynamic min;

  Price({this.date, this.max, this.min});

  factory Price.fromJson(Map<String, dynamic> json) {
    return Price(
      date: json['date'],
      max: json['maxPrice'],
      min: json['minPrice'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['max'] = this.max;
    data['min'] = this.min;
    return data;
  }
}

class Error implements ResponseModel {
  final String errorCode;
  final String errorDesc;

  Error({this.errorCode, this.errorDesc});

  factory Error.fromJson(Map<String, dynamic> json) {
    return Error(
      errorCode: json['errorCode'],
      errorDesc: json['errorDesc'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorCode'] = this.errorCode;
    data['errorDesc'] = this.errorDesc;
    return data;
  }
}
