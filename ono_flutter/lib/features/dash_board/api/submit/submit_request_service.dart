import 'package:ono_clean_framework/service/ono_service.dart';
import 'package:ono_clean_framework/service/rest_api.dart';
import 'package:ono_flutter/core/api/fuse_api/fuse_api.dart';
import 'package:ono_flutter/features/dash_board/api/daily_price/daily_price_response.dart';

class SubmitRequestService extends OnoService {
  SubmitRequestService(OnoServiceResponseHandler handler)
      : super(
    url: FuseAPI.dailyPriceUrl,
    action: HttpAction.post,
    handler: handler,
  );

  @override
  DailyPriceResponseModel parseJson(Map<String, dynamic> json) {
    return DailyPriceResponseModel.fromJson(json);
  }
}
