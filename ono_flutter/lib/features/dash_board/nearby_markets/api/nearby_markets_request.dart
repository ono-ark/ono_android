import 'package:ono_clean_framework/service/ono_service.dart';

class NearbyMarketsRequestModel implements RequestModel {
  final String onoId;
  final String marketId;

  NearbyMarketsRequestModel({this.onoId, this.marketId});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['onoId'] = this.onoId;
    data['marketId'] = this.marketId;
    return data;
  }
}
