import 'package:ono_clean_framework/service/ono_service.dart';
import 'package:ono_clean_framework/service/rest_api.dart';
import 'package:ono_flutter/core/api/fuse_api/fuse_api.dart';
import 'package:ono_flutter/features/dash_board/nearby_markets/api/nearby_markets_response.dart';

class NearByMarketsService extends OnoService {
  NearByMarketsService(OnoServiceResponseHandler handler)
      : super(
          url: FuseAPI.nearByMarketsUrl,
          action: HttpAction.get,
          handler: handler,
        );

  @override
  NearByMarketsResponseModel parseJson(Map<String, dynamic> json) {
    return NearByMarketsResponseModel.fromJson(json);
  }
}
