import 'package:ono_clean_framework/service/ono_service.dart';

class NearByMarketsResponseModel implements ResponseModel {
  final Data data;

  NearByMarketsResponseModel({this.data});

  factory NearByMarketsResponseModel.fromJson(Map<String, dynamic> json) {
    return NearByMarketsResponseModel(
      data: json['data'] != null ? Data.fromJson(json['data']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data implements ResponseModel {
  final List<Crop> crops;
  final String marketId;

  Data({this.crops, this.marketId});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      crops: json['crops'] != null
          ? (json['crops'] as List).map((i) => Crop.fromJson(i)).toList()
          : null,
      marketId: json['marketId'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['marketId'] = this.marketId;
    if (this.crops != null) {
      data['crops'] = this.crops.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Crop implements ResponseModel {
  final String cropId;
  final String cropName;
  final String imageUrl;
  final dynamic maxPrice;
  final dynamic minPrice;
  final dynamic modalPrice;
  final bool preferedCrop;

  Crop(
      {this.cropId,
      this.cropName,
      this.imageUrl,
      this.maxPrice,
      this.minPrice,
      this.modalPrice,
      this.preferedCrop});

  factory Crop.fromJson(Map<String, dynamic> json) {
    return Crop(
      cropId: json['cropId'],
      cropName: json['cropName'],
      imageUrl: json['imageUrl'],
      maxPrice: json['maxPrice'],
      minPrice: json['minPrice'],
      modalPrice: json['modalPrice'],
      preferedCrop: json['preferedCrop'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cropId'] = this.cropId;
    data['cropName'] = this.cropName;
    data['imageUrl'] = this.imageUrl;
    data['maxPrice'] = this.maxPrice;
    data['minPrice'] = this.minPrice;
    data['modalPrice'] = this.modalPrice;
    data['preferedCrop'] = this.preferedCrop;
    return data;
  }
}
