import 'package:flutter/material.dart';
import 'package:ono_flutter/features/dash_board/nearby_markets/model/nearby_markets_view_model.dart';
import 'package:ono_flutter/features/dash_board/ui/dash_board_screen.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';
import 'package:ono_flutter/ui_utils/validations.dart';

class NearByMarketsScreen extends StatelessWidget {
  final NearByMarketsViewModel nearByMarketViewModel;
  final Function locationTapEvent;

  const NearByMarketsScreen({Key key, this.nearByMarketViewModel, this.locationTapEvent})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: graphBackgroundColor,
      body: _buildBody(context),
    );
  }

  _buildBody(BuildContext context) {
    return Column(
      children: [
        _buildAppBar(context),
        _buildNearbyMarketsList(context),
      ],
    );
  }

  _buildAppBar(BuildContext context) {
    return AppBarWithMultipleOptions(
      title: strings.nearByMarketsValue,
      subtitle: Validations.dateFormatBuilder(),
      isHideTrailing: false,
      size: 23,
      backButtonEvent: () {
        Navigator.pop(context);
      },
    );
  }

  _buildNearbyMarketsList(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 120,
              child: ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: List.generate(
                    nearByMarketViewModel.nearestMarketsViewList.length,
                    (index) {
                  var item =
                      nearByMarketViewModel.nearestMarketsViewList[index];
                  return GestureDetector(
                    onTap: (){
                      locationTapEvent(item.marketId);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8.0, left: 8),
                      child: Container(
                          padding: EdgeInsets.only(
                            top: 10,
                            bottom: 10,
                          ),
                          height: 20,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              border: Border.all(color: priceBorderColor),
                              color:
                                  item.isSelected ? headLine2Color : whiteColor,
                              borderRadius: BorderRadius.circular(5)),
                          child: ListTile(
                            // contentPadding: EdgeInsets.symmetric(vertical: 5),
                            leading: Column(
                              children: [
                                Container(
                                  height: 30,
                                  child: Image.asset(
                                      'assets/Images/png/market_q.png'),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                GenericTextWithStyle(
                                  text: item.radius + ' KM',
                                  textStyle: theme.headline4.copyWith(
                                    color: item.isSelected
                                        ? whiteColor
                                        : nearByMarketsSubTitleColor,
                                    fontSize: 14,
                                  ),
                                ),
                              ],
                            ),
                            title: Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: GenericTextWithStyle(
                                text: item.marketName,
                                textStyle: theme.headline4.copyWith(
                                    color:
                                        item.isSelected ? whiteColor : blackColor,
                                    fontSize: 16),
                              ),
                            ),
                            subtitle: Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: GenericTextWithStyle(
                                text: item.address,
                                textStyle: theme.headline4.copyWith(
                                    color: item.isSelected
                                        ? whiteColor
                                        : nearByMarketsSubTitleColor,
                                    fontSize: 14,
                                    height: 1.2),
                              ),
                            ),
                          )),
                    ),
                  );
                }),
              ),
            ),
            _buildPreferredCropsAndOtherCrops(),
          ],
        ),
      ),
    );
  }

  _buildPreferredCropsAndOtherCrops() {
    if (nearByMarketViewModel.cropsList.length > 0) {
      return Column(
        children: [
          _buildPreferredCrops(),
          _buildOtherCrops(),
        ],
      );
    } else {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: GenericTextWithStyle(
          text: strings.noCropsFound,
          textStyle: theme.headline2.copyWith(color: headLine1Color),
        ),
      );
    }
  }

  _buildPreferredCrops() {
    var list = nearByMarketViewModel.cropsList
        .where((element) => element.preferedCrop == true)
        .toList();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: GenericTextWithStyle(
            text: strings.preferredCrops,
            textStyle: theme.headline2.copyWith(color: headLine1Color),
          ),
        ),
        Column(
          children: List.generate(list.length, (index) {
            var item = list[index];
            return _buildCommonCropCard(item);
          }),
        ),
      ],
    );
  }

  _buildOtherCrops() {
    var list = nearByMarketViewModel.cropsList
        .where((element) => element.preferedCrop != true)
        .toList();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: GenericTextWithStyle(
            text: strings.otherCrops,
            textStyle: theme.headline2.copyWith(color: headLine1Color),
          ),
        ),
        Column(
          children: List.generate(list.length, (index) {
            var item = list[index];
            return _buildCommonCropCard(item);
          }),
        ),
      ],
    );
  }

  _buildCommonCropCard(var item) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
      child: Card(
        child: ListTile(
          leading: Container(
            width: 50,
            height: 50,
            child: Image.network(item.imageUrl),
          ),
          title: GenericTextWithStyle(
            text: item.cropName,
            textStyle:
                theme.headline4.copyWith(color: blackColor, fontSize: 16),
          ),
          subtitle: Column(
            children: [
              SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GenericTextWithStyle(
                        text: strings.min,
                        textStyle: theme.headline4.copyWith(
                          color: nearByMarketsSubTitleColor,
                          fontSize: 14,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      GenericTextWithStyle(
                        text: strings.rs + item.minPrice,
                        textStyle: theme.headline4
                            .copyWith(color: blackColor, fontSize: 16),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GenericTextWithStyle(
                        text: strings.max,
                        textStyle: theme.headline4.copyWith(
                          color: nearByMarketsSubTitleColor,
                          fontSize: 14,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      GenericTextWithStyle(
                        text: strings.rs + item.maxPrice,
                        textStyle: theme.headline4
                            .copyWith(color: blackColor, fontSize: 16),
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
