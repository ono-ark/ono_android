import 'package:flutter/src/widgets/framework.dart';
import 'package:ono_clean_framework/ui/presenter.dart';
import 'package:ono_flutter/features/dash_board/bloc/dash_board_bloc.dart';
import 'package:ono_flutter/features/dash_board/nearby_markets/model/nearby_markets_view_model.dart';
import 'package:ono_flutter/features/dash_board/nearby_markets/ui/nearby_markets_screen.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

class NearByMarketsPresenter
    extends Presenter<DashBoardBloc, NearByMarketsViewModel> {
  @override
  Widget buildScreen(BuildContext context, DashBoardBloc bloc,
      NearByMarketsViewModel viewModel) {
    return NearByMarketsScreen(
      nearByMarketViewModel: viewModel,
      locationTapEvent: (item) {
        bloc.updateChangedMarketEvent.send(item);
      },
    );
  }

  @override
  Stream<NearByMarketsViewModel> getViewModelStream(
      BuildContext context, DashBoardBloc bloc) {
    return bloc.nearbyMarketsViewModelEvent.receive;
  }

  @override
  void sendViewModelRequest(BuildContext context, DashBoardBloc bloc) {
    bloc.requestNearByMarketsEvent.launch();
  }

  @override
  Widget buildLoadingScreen(BuildContext context) {
    return GenericProgressbar();
  }
}
