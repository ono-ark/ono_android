import 'package:ono_clean_framework/model.dart';

class NearByMarketsBusinessModel implements BusinessModel {
  List<CropBusinessModel> cropsList = [];
  List<NearbyMarketBusinessModel> nearestMarketsBusinessList = [];
  String marketId = '';
}

class CropBusinessModel implements BusinessModel {
  String cropId = '';
  String cropName = '';
  String imageUrl = '';
  dynamic maxPrice = '';
  dynamic minPrice = '';
  dynamic modalPrice = '';
  bool preferredCrop = false;
}

class NearbyMarketBusinessModel implements BusinessModel {
  String marketId = '';
  String marketName = '';
  String address = '';
  String radius = '';
  bool isSelected = false;
}
