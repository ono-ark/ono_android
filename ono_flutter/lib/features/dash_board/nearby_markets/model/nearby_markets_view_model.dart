import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/features/dash_board/model/dashboard_view_model.dart';

class NearByMarketsViewModel implements ViewModel {
  final List<CropDetailsViewModel> cropsList;
  final String marketId;
  final List<NearbyMarketViewModel> nearestMarketsViewList;

  NearByMarketsViewModel({this.cropsList, this.marketId,this.nearestMarketsViewList});
}

class CropDetailsViewModel implements ViewModel {
  final String cropId;
  final String cropName;
  final String imageUrl;
  final dynamic maxPrice;
  final dynamic minPrice;
  final dynamic modalPrice;
  final bool preferedCrop;

  CropDetailsViewModel(
      {this.cropId,
      this.cropName,
      this.imageUrl,
      this.maxPrice,
      this.minPrice,
      this.modalPrice,
      this.preferedCrop});
}

class NearbyMarketViewModel implements ViewModel {
  final String marketId;
  final String marketName;
  final String radius;
  final String address;
  final bool isSelected;

  NearbyMarketViewModel({
    this.marketId,
    this.marketName,
    this.radius,
    this.address,
    this.isSelected

  });
}