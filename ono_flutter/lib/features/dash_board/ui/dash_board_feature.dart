import 'package:flutter/material.dart';
import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_flutter/common/features/navigator/navigator.dart';
import 'package:ono_flutter/features/dash_board/bloc/dash_board_bloc.dart';
import 'package:ono_flutter/features/dash_board/ui/dashboard_presenter.dart';

class DashboardFeatureWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      child: FeatureNavigator(
        sectionChild: DashBoardPresenter(),
        maintainState: true,
      ),
      bloc: DashBoardBloc(),
    );
  }
}
