import 'package:flutter/material.dart';
import 'package:ono_flutter/features/dash_board/model/dashboard_view_model.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';
import 'package:ono_flutter/ui_utils/validations.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'dash_board_screen.dart';

class ShowPriceListForIndividualMarket extends StatelessWidget {
  final NearestMarketViewModel item;
  final DashBoardViewModel viewModel;

  const ShowPriceListForIndividualMarket({Key key, this.item, this.viewModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: whiteColor,
      margin: EdgeInsets.all(10),
      child: ListView(
        children: [
          _buildHeaderListTile(context, item, viewModel),
          Divider(),
          Scrollbar(
            child: Column(
              children: List.generate(item.pricesViewList.length, (index) {
                return _buildPriceItem(item.pricesViewList[index], context);
              }),
            ),
          ),
          _buildRatingBarWidget(),
          _buildButton(context),
        ],
      ),
    );
  }

  _buildHeaderListTile(
    BuildContext context,
    NearestMarketViewModel item,
    DashBoardViewModel viewModel,
  ) {
    var list = viewModel.preferredCropsViewList
        .where((element) => element.isSelected)
        .first;
    return ListTile(
      title: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Row(
          children: [
            Column(
              children: [
                Image.memory(
                  list.uint8list,
                  fit: BoxFit.fill,
                  height: 45,
                ),
                SizedBox(
                  height: 10,
                ),
                GenericTextWithStyle(
                  text: list.cropName,
                  textStyle: theme.headline2.copyWith(fontSize: 12),
                ),
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: GenericTextWithStyle(
                  text: strings.dailyPrice + " at \n" + item.marketName,
                  textStyle: theme.headline2.copyWith(color: headLine1Color),
                ),
              ),
            ),
          ],
        ),
      ),
      trailing: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.close,
            color: blackColor,
          )),
    );
  }

  _buildPriceItem(PriceViewModel pricesViewList, BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 0),
      decoration: BoxDecoration(
          border: Border.all(color: priceBorderColor),
          color: Colors.white,
          borderRadius: BorderRadius.circular(5)),
      child: Column(
        children: [
          _buildGraphText(
              text: Validations.dateFormatBuilder(date: pricesViewList.date),
              size: 14),
          Row(
            children: [
              _buildGraphText(text: 'Min Rs ${pricesViewList.min}'),
              // RotatedBox(
              //     quarterTurns: 1,
              //     child: Container(
              //         decoration: const BoxDecoration(
              //       image: DecorationImage(
              //           image: AssetImage(
              //             'assets/Images/png/bar_icons_dashboard.png',
              //           ),
              //           fit: BoxFit.fill,
              //
              //           ),
              //
              //     ))),
              _buildGraphText(text: 'Max Rs ${pricesViewList.max}'),
              Column(
                children: [
                  // Container(
                  //   // height: 70,
                  //   child: Image.asset('assets/Images/png/bar_icons_dashboard.png'),
                  // ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  _buildGraphText({String text, double size}) {
    return GenericTextWithStyle(
      text: text ?? 'Friday,Mar 5 2021',
      textStyle: theme.headline4
          .copyWith(color: blackColor, fontSize: size ?? 11, height: 1.2),
      textAlign: TextAlign.center,
    );
  }

  _buildRatingBarWidget() {
    List list = ['10', '25', '50', '75', '100'];
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, top: 15, bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildGraphText(text: strings.onoProvidingCorrect, size: 16),
          SizedBox(
            height: 10,
          ),
          RatingBar.builder(
            initialRating: 3,
            minRating: 1,
            direction: Axis.horizontal,
            allowHalfRating: false,
            itemCount: 5,
            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
            itemBuilder: (context, index) {
              return Column(
                children: [
                  Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  _buildGraphText(text: list[index] + '%')
                ],
              );
            },
            onRatingUpdate: (rating) {
              print(rating);
            },
          ),
        ],
      ),
    );
  }

  _buildButton(BuildContext context) {
    return Column(
      children: [
        ButtonWithTapEvent(
          text: strings.submit,
          tapCallBack: () {},
        ),
        _spaceWidget(),
        GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: GenericTextWithStyle(
            text: strings.close,
            textStyle:
                theme.headline3.copyWith(color: nearByMarketsSubTitleColor),
          ),
        ),
        _spaceWidget(),
      ],
    );
  }

  _spaceWidget() {
    return SizedBox(
      height: 30,
    );
  }
}
