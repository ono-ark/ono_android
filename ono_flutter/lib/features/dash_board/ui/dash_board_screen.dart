import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/core/api/user_info/user_info.dart';
import 'package:ono_flutter/features/dash_board/model/dashboard_view_model.dart';
import 'package:ono_flutter/features/dash_board/strings/strings.dart';
import 'package:ono_flutter/features/dash_board/ui/custom_painter.dart';
import 'package:ono_flutter/features/select_crops/ui/feature_widget.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';
import 'package:ono_flutter/ui_utils/validations.dart';

var strings = DashBoardStrings();

class DashBoardScreen extends StatelessWidget {
  final DashBoardViewModel dashBoardViewModel;
  final VoidCallback locationTap;
  final VoidCallback editCropPreferencesTapEvent;
  final Function cropSelectionItemEvent;
  final Function graphTapEvent;
  final Function nextCropItemEvent;
  final Function dateChangeTapEvent;
  final Function nearByMarketTapEvent;
  final _controller = ScrollController();

  DashBoardScreen(
      {Key key,
      this.dashBoardViewModel,
      this.locationTap,
      this.cropSelectionItemEvent,
      this.dateChangeTapEvent,
      this.nextCropItemEvent,
      this.editCropPreferencesTapEvent,
      this.graphTapEvent,
      this.nearByMarketTapEvent})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: graphBackgroundColor,
      body: _buildBody(context),
    );
  }

  _buildBody(context) {
    return Column(
      children: [
        _buildAppBar(),
        _buildSpaceWidget(),
        _buildLocationWidget(),
        _buildSpaceWidget(),
        _buildGraphUIBasedOnDifferentConditions(context),
      ],
    );
  }

  _buildGraphUIBasedOnDifferentConditions(BuildContext context) {
    if (dashBoardViewModel.dashBoardUIConditions ==
        DashBoardUIConditions.noLocation) {
      return _buildGraphBasedNoLocation(
          context: context,
          assetImage: 'assets/Images/png/location.png',
          errorText: strings.locationIsNotEntered,
          buttonText: strings.selectYourLocation);
    } else if (dashBoardViewModel.dashBoardUIConditions ==
        DashBoardUIConditions.preferredCropsNotInLocation) {
      return _buildGraphBasedNoLocation(
          context: context,
          isShowIcon: false,
          assetImage: 'assets/Images/png/no_location_selected.png',
          errorText: strings.noDataDetected,
          buttonText: strings.submitARequest);
    } else {
      return _buildGraph(context);
    }
  }

  _buildGraphBasedNoLocation({
    context,
    String assetImage,
    String errorText,
    String buttonText,
    bool isShowIcon = true,
  }) {
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildList(context),
            SizedBox(
              height: 10,
            ),
            PaddingLeftAndRight(
              child: Container(
                width: double.infinity,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: paddingSpaceFromAppbar,
                        child: Container(
                          height: 70,
                          child: Image.asset(
                            assetImage,
                          ),
                        ),
                      ),
                      _buildSpaceWidget(),
                      _buildSpaceWidget(),
                      GenericTextWithStyle(
                        text: errorText,
                        textStyle: theme.headline2.copyWith(
                          fontSize: 18,
                          color: blackColor,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      _buildSpaceWidget(),
                      _buildSpaceWidget(),
                      GestureDetector(
                        onTap: locationTap,
                        child: Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          padding: EdgeInsets.all(7),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              border: Border.all(color: underLineColor),
                              color: Colors.white),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: GenericTextWithStyle(
                                  text: buttonText,
                                  textStyle: theme.headline4
                                      .copyWith(color: underLineColor),
                                ),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              isShowIcon
                                  ? Icon(
                                      Icons.my_location,
                                      color: underLineColor,
                                    )
                                  : IgnoreWidget(),
                            ],
                          ),
                        ),
                      ),
                      _buildSpaceWidget(),
                      _buildSpaceWidget(),
                      _buildSpaceWidget(),
                    ],
                  ),
                ),
              ),
            ),
            isShowIcon
                ? _buildNearByMarkets()
                : SizedBox(
                    height: 30,
                  ),
          ],
        ),
      ),
    );
  }

  _buildNoLocation() {}

  _buildSpaceWidget() {
    return SizedBox(
      height: 10,
    );
  }

  _buildAppBar() {
    return AppBarWithMultipleOptions(
      title: strings.dailyPrice,
      subtitle: Validations.dateFormatBuilder(),
      isHideTrailing: false,
    );
  }

  _buildGraph(context) {
    var list = dashBoardViewModel.preferredCropsViewList
        .where((element) => element.isSelected)
        .toList();

    return Expanded(
      child: SingleChildScrollView(
        child: Stack(
          children: [
            Column(
              children: [
                _buildList(context),
                _buildGraphCard(
                    preferredCropsViewModel: (list.length > 0) ? list[0] : [],
                    context: context),
                _buildNearByMarkets(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _buildGraphCard(
      {PreferredCropsViewModel preferredCropsViewModel, BuildContext context}) {
    var priceList = preferredCropsViewModel
        .nearestMarketsViewList[0].pricesViewList
        .where((element) => element.isSelected)
        .toList();
    PriceViewModel viewModel;
    if (priceList.length == 1) {
      viewModel = priceList[0];
    } else {
      viewModel =
          PriceViewModel(max: 0.0, min: 0.0, date: DateTime.now().toString());
    }
    return new Container(
      margin: EdgeInsets.only(
        left: 5,
        right: 5,
      ),
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 5),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 8.0,
                      ),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: GenericTextWithStyle(
                          text: strings.dailyPrice,
                          textStyle: theme.headline2
                              .copyWith(color: blackColor, fontSize: 16),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          // _buildSpace(),
                          _buildArrows(tapEvent: () {
                            _previousActionEvent(date: viewModel.date);
                          }),
                          _buildSpace(),
                          Expanded(
                              child: GenericTextWithStyle(
                            text: Validations.dateFormatBuilder(
                                date: viewModel.date),
                            textStyle: theme.headline4.copyWith(
                                color: blackColor, fontSize: 14, height: 1.2),
                            textAlign: TextAlign.center,
                          )),
                          _buildSpace(),
                          _buildArrows(
                              iconData: Icons.arrow_forward,
                              tapEvent: () {
                                _nextActionEvent(date: viewModel.date);
                              }),
                          _buildSpace(),

                          GestureDetector(
                            onTap: () {
                              print(
                                  'indexxx${preferredCropsViewModel.index} cropid${preferredCropsViewModel.cropId}');
                              nextCropItemEvent(preferredCropsViewModel.index);
                              if (preferredCropsViewModel.index ==
                                  dashBoardViewModel
                                          .preferredCropsViewList.length -
                                      1) {
                                _animateToIndex(0);
                              } else {
                                _animateToIndex(
                                    preferredCropsViewModel.index + 1);
                              }
                            },
                            child: Container(
                              width: 70,
                              margin: EdgeInsets.only(left: 5),
                              padding: EdgeInsets.only(left: 4, right: 4),
                              decoration: BoxDecoration(
                                  color: whiteColor,
                                  // border: Border(left: BorderSide(color: whiteColor,width: 2,)),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(70),
                                    topRight: Radius.circular(70),
                                  )),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    width: 63,
                                    height: 63,
                                    child: Image.memory(
                                      preferredCropsViewModel.uint8list,
                                      fit: BoxFit.fill,
                                    ),
                                    // child: CachedNetworkImage(
                                    //   imageUrl: selectCropsViewModel.cropsListViewModel[index].imageUrl,
                                    //   imageBuilder: (context, imageProvider) => Container(
                                    //     decoration: BoxDecoration(
                                    //       image: DecorationImage(
                                    //           image: imageProvider,
                                    //           fit: BoxFit.cover,
                                    //           colorFilter: ColorFilter.mode(
                                    //               Colors.red, BlendMode.colorBurn)),
                                    //     ),
                                    //   ),
                                    //   placeholder: (context, url) => CircularProgressIndicator(),
                                    //   errorWidget: (context, url, error) => Icon(Icons.error),
                                    // ),
                                    // margin: EdgeInsets.all(10.0),
                                    // child: SvgPicture.network(list[index].imageUrl),
                                    decoration: BoxDecoration(
                                      color: Colors.orange,
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // _buildSpace(),
                        ],
                      ),
                    ),
                    Container(
                      color: whiteColor,
                      height: MediaQuery.of(context).size.height / 3.0,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: List.generate(
                            preferredCropsViewModel
                                .nearestMarketsViewList.length, (index) {
                          var prices = preferredCropsViewModel
                              .nearestMarketsViewList[index].pricesViewList
                              .where((element) => element.isSelected)
                              .toList();
                          PriceViewModel priceViewModel;
                          if (prices.length == 1) {
                            priceViewModel = prices[0];
                          } else {
                            priceViewModel = PriceViewModel(max: 0.0, min: 0.0);
                          }

                          var name = preferredCropsViewModel
                              .nearestMarketsViewList[index].marketName
                              .split(' ');

                          return GestureDetector(
                            onTap: () {
                              graphTapEvent(preferredCropsViewModel
                                  .nearestMarketsViewList[index]);
                            },
                            child: Row(
                              children: [
                                Column(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(10),
                                      height: 140,
                                      margin:
                                          EdgeInsets.only(left: 3, right: 3),
                                      width: MediaQuery.of(context).size.width /
                                          5.0,
                                      child: Column(
                                        children: [
                                          _buildGraphText(
                                              text: 'Min Rs' +
                                                  '\n' +
                                                  '${priceViewModel.min}'),
                                          _buildBarPriceSpace(),
                                          Container(
                                            // height: 70,
                                            child: Image.asset(
                                                'assets/Images/png/bar_icons_dashboard.png'),
                                          ),
                                          _buildBarPriceSpace(),
                                          _buildGraphText(
                                              text: 'Max Rs' +
                                                  '\n' +
                                                  '${priceViewModel.max}'),
                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: priceBorderColor),
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                    _buildBarPriceSpace(),
                                    _buildGraphText(
                                        text: name[0] + '\n' + ' ' + name[1]),
                                  ],
                                ),
                              ],
                            ),
                          );
                        }),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          // Positioned(
          //   top: 0,
          //   left: 110,
          //   child: CustomPaint(
          //     size: Size(15.0, 10),
          //     painter: TrianglePainter(),
          //   ),
          // ),
        ],
      ),
    );
  }

  _previousActionEvent({String date}) {
    if (Validations.getDifferenceBetweenDates(date: date) < 5) {
      dateChangeTapEvent(TapActionArrows.prev);
    }
  }

  _nextActionEvent({String date}) {
    if (date != Validations.pickDate()) {
      dateChangeTapEvent(TapActionArrows.next);
    }
  }

  _buildGraphText({String text}) {
    return GenericTextWithStyle(
      text: text ?? 'Friday,Mar 5 2021',
      textStyle:
          theme.headline4.copyWith(color: blackColor, fontSize: 9, height: 1.2),
      textAlign: TextAlign.center,
    );
  }

  _buildArrows({IconData iconData, Function tapEvent}) {
    return GestureDetector(
      onTap: tapEvent,
      child: CircleAvatar(
        radius: 12,
        backgroundColor: backGroundColor,
        child: Icon(
          iconData ?? Icons.arrow_back,
          size: 15,
        ),
      ),
    );
  }

  _buildSpace() {
    return SizedBox(
      width: 10,
    );
  }

  _buildBarPriceSpace() {
    return SizedBox(
      height: 5,
    );
  }

  _buildPriceSpace() {
    return SizedBox(
      width: 5,
    );
  }

  _buildLocationWidget() {
    return GestureDetector(
      // onTap: locationTap,
      child: Container(
        margin: EdgeInsets.only(left: 5, right: 5),
        padding: EdgeInsets.all(5),
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border.all(color: locationBoarder),
          color: whiteColor,
        ),
        child: Row(
          children: [
            Expanded(
              child: GenericTextWithStyle(
                text: (dashBoardViewModel.location == null ||
                        dashBoardViewModel.location.isEmpty)
                    ? strings.selectYourLocation
                    : dashBoardViewModel.location,
                textStyle: theme.headline4.copyWith(
                    fontSize: 18,
                    color: (dashBoardViewModel.location == null ||
                            dashBoardViewModel.location.isEmpty)
                        ? locationBoarder
                        : blackColor),
              ),
            ),
            GestureDetector(
              onTap: locationTap,
              child: Row(
                children: [
                  GenericTextWithStyle(
                    text: strings.locateMe,
                    textStyle: theme.headline4.copyWith(color: underLineColor),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Icon(
                    Icons.my_location,
                    color: underLineColor,
                  ),
                ],
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        ),
      ),
    );
  }

  _animateToIndex(i) => _controller.animateTo(95.0 * i,
      duration: Duration(seconds: 2), curve: Curves.fastOutSlowIn);

  _buildList(context) {
    return Stack(
      children: [
        Container(
          color: headLine2Color,
          padding: EdgeInsets.only(top: 5, left: 5, right: 5),
          height: 110,
          child: new ListView(
            controller: _controller,
            scrollDirection: Axis.horizontal,
            children: List.generate(
                dashBoardViewModel.preferredCropsViewList.length, (index) {
              var item = dashBoardViewModel.preferredCropsViewList[index];
              return GestureDetector(
                onTap: () {
                  _animateToIndex(index);
                  cropSelectionItemEvent(item.cropId);
                },
                child: Container(
                  width: 70,
                  margin: EdgeInsets.only(left: 5),
                  padding: EdgeInsets.only(left: 4, right: 4),
                  decoration: BoxDecoration(
                      color: item.isSelected
                          ? graphBackgroundColor
                          : headLine2Color,
                      // border: Border(left: BorderSide(color: whiteColor,width: 2,)),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(70),
                        topRight: Radius.circular(70),
                      )),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        width: 63,
                        height: 63,
                        child: Image.memory(
                          item.uint8list,
                          fit: BoxFit.fill,
                        ),
                        // child: CachedNetworkImage(
                        //   imageUrl: selectCropsViewModel.cropsListViewModel[index].imageUrl,
                        //   imageBuilder: (context, imageProvider) => Container(
                        //     decoration: BoxDecoration(
                        //       image: DecorationImage(
                        //           image: imageProvider,
                        //           fit: BoxFit.cover,
                        //           colorFilter: ColorFilter.mode(
                        //               Colors.red, BlendMode.colorBurn)),
                        //     ),
                        //   ),
                        //   placeholder: (context, url) => CircularProgressIndicator(),
                        //   errorWidget: (context, url, error) => Icon(Icons.error),
                        // ),
                        // margin: EdgeInsets.all(10.0),
                        // child: SvgPicture.network(list[index].imageUrl),
                        decoration: BoxDecoration(
                          color: Colors.orange,
                          shape: BoxShape.circle,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Expanded(
                        child: GenericTextWithStyle(
                          text: item.cropName,
                          textAlign: TextAlign.center,
                          textStyle: theme.headline4.copyWith(
                              color:
                                  item.isSelected ? headLine2Color : whiteColor,
                              fontSize: 12,
                              height: 1.2),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
          ),
        ),
        _buildEditCropsPreferences(),
      ],
    );
  }

  _buildEditCropsPreferences() {
    return Positioned(
      child: Container(
        width: 50,
        height: 120,
        color: headLine2Color,
        child: GestureDetector(
          onTap: editCropPreferencesTapEvent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(left: 10, right: 10),
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.white),
                child: CircleAvatar(
                  radius: 14,
                  child: SvgPicture.asset(
                    'assets/Images/svg/edit.svg',
                    width: 12,
                    height: 12,
                  ),
                  backgroundColor: whiteColor,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              GenericTextWithStyle(
                text: strings.edit,
                textAlign: TextAlign.center,
                textStyle:
                    theme.headline4.copyWith(color: whiteColor, fontSize: 12),
              ),
            ],
          ),
        ),
      ),
      right: 2,
    );
  }

  _buildNearByMarkets() {
    return Container(
      width: double.infinity,
      margin: paddingSpaceFromHeadline2,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PaddingLeftAndRight(
            child: Padding(
              padding: paddingSpaceFromHeadline2,
              child: GenericTextWithStyle(
                text: strings.nearByMarkets,
                textStyle: theme.headline2.copyWith(color: headLine1Color),
              ),
            ),
          ),
          _buildNearbyMarketsList(),
          SizedBox(
            height: 20,
          ),
        ],
      ),
      color: backGroundColor,
    );
  }

  _buildNearbyMarketsList() {
    if (dashBoardViewModel.dashBoardUIConditions ==
        DashBoardUIConditions.noLocation) {
      return GestureDetector(
        onTap: locationTap,
        child: Padding(
          padding: paddingSpaceFromHeadline2,
          child: PaddingLeftAndRight(
              child: Container(
                  padding: EdgeInsets.only(top: 5, bottom: 5),
                  decoration: BoxDecoration(
                      border: Border.all(color: priceBorderColor),
                      color: whiteColor,
                      borderRadius: BorderRadius.circular(5)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTile(
                        leading: Container(
                          // height: 30,
                          child: Image.asset('assets/Images/png/market_q.png'),
                        ),
                        title: GenericTextWithStyle(
                          text: strings.locationIsNotEntered,
                          textStyle: theme.headline4
                              .copyWith(color: blackColor, fontSize: 16),
                        ),
                        subtitle: GenericTextWithStyle(
                          text: strings.selectYourLocation,
                          textStyle: theme.headline4.copyWith(
                              color: nearByMarketsSubTitleColor, fontSize: 14),
                        ),
                      ),
                      // Padding(
                      //   padding: EdgeInsets.only(left: 15, bottom: 5),
                      //   child: GenericTextWithStyle(
                      //     text: '12KM',
                      //     textStyle: theme.headline4.copyWith(
                      //       color: nearByMarketsSubTitleColor,
                      //       fontSize: 14,
                      //     ),
                      //   ),
                      // ),
                    ],
                  ))),
        ),
      );
    } else {
      return Column(
        children: List.generate(
            dashBoardViewModel.nearestMarketsViewList.length, (index) {
          var item = dashBoardViewModel.nearestMarketsViewList[index];
          return GestureDetector(
            onTap: () {
              nearByMarketTapEvent(item.marketId);
            },
            child: Padding(
              padding: paddingSpaceFromHeadline2,
              child: PaddingLeftAndRight(
                  child: Container(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                          border: Border.all(color: priceBorderColor),
                          color: whiteColor,
                          borderRadius: BorderRadius.circular(5)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ListTile(
                            // contentPadding: EdgeInsets.symmetric(vertical: 5),
                            leading: Column(
                              children: [
                                Container(
                                  height: 30,
                                  child: Image.asset(
                                      'assets/Images/png/market_q.png'),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                GenericTextWithStyle(
                                  text: item.radius + ' KM',
                                  textStyle: theme.headline4.copyWith(
                                    color: nearByMarketsSubTitleColor,
                                    fontSize: 14,
                                  ),
                                ),
                              ],
                            ),
                            title: Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: GenericTextWithStyle(
                                text: item.marketName,
                                textStyle: theme.headline4
                                    .copyWith(color: blackColor, fontSize: 16),
                              ),
                            ),
                            subtitle: Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: GenericTextWithStyle(
                                text: item.address,
                                textStyle: theme.headline4.copyWith(
                                    color: nearByMarketsSubTitleColor,
                                    fontSize: 14,
                                    height: 1.2),
                              ),
                            ),
                          ),
                        ],
                      ))),
            ),
          );
        }),
      );
    }
  }
}
