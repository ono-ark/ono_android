import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ono_clean_framework/ui/presenter.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/core/api/user_info/user_info.dart';
import 'package:ono_flutter/features/dash_board/bloc/dash_board_bloc.dart';
import 'package:ono_flutter/features/dash_board/model/dashboard_view_model.dart';
import 'package:ono_flutter/features/dash_board/nearby_markets/ui/nearby_markets_presenter.dart';
import 'package:ono_flutter/features/dash_board/ui/dash_board_screen.dart';
import 'package:ono_flutter/features/select_crops/ui/feature_widget.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';
import 'package:ono_flutter/ui_utils/toast_message.dart';
import 'package:ono_flutter/ui_utils/validations.dart';
import 'package:ono_flutter/features/dash_board/ui/show_dailog.dart';

class DashBoardPresenter extends Presenter<DashBoardBloc, DashBoardViewModel> {
  @override
  Widget buildScreen(
      BuildContext context, DashBoardBloc bloc, DashBoardViewModel viewModel) {
    return DashBoardScreen(
      dashBoardViewModel: viewModel,
      editCropPreferencesTapEvent: () {
        ToastMessage.showToastMessage(message: "Coming Soon");
        // _navigateToEditCropPreferences(context, bloc);
      },
      nearByMarketTapEvent: (item) {
        bloc.updateSelectedMarketEvent.send(item);
        _navigateToNearByMarkets(context, bloc);
      },
      graphTapEvent: (item) {
        // _barChart(context, item, viewModel);
      },
      nextCropItemEvent: (item) {
        bloc.requestNextCropEvent.send(item);
      },
      dateChangeTapEvent: (event) {
        bloc.requestPreviousDateEvent.send(event);
      },
      cropSelectionItemEvent: (item) {
        bloc.updateSelectedCropEvent.send(item);
      },
      locationTap: () {
        if (viewModel.dashBoardUIConditions ==
            DashBoardUIConditions.noLocation) {
          bloc.requestLocationEvent.launch();
        }
      },
    );
  }

  @override
  Widget buildLoadingScreen(BuildContext context) {
    return GenericProgressbar();
  }

  @override
  Stream<DashBoardViewModel> getViewModelStream(
      BuildContext context, DashBoardBloc bloc) {
    return bloc.dailyPriceViewModelEvent.receive;
  }

  @override
  void sendViewModelRequest(BuildContext context, DashBoardBloc bloc) {
    bloc.requestDailyPriceEvent.launch();
  }

  _navigateToEditCropPreferences(
      BuildContext context, DashBoardBloc bloc) async {
    UserInfoAPI.setCropsSelection(cropSelection: CropSelection.edit);
    var result = await Navigator.of(context, rootNavigator: true).push(
        MaterialPageRoute(
            builder: (BuildContext context) => SelectCropsFeatureWidget()));
    if (result != null) {
      bloc.requestDailyPriceEvent.launch();
    }
  }

  _navigateToNearByMarkets(BuildContext context, DashBoardBloc bloc) async {
    // UserInfoAPI.setCropsSelection(cropSelection: CropSelection.edit);
    var result = await Navigator.of(
      context,
    ).push(MaterialPageRoute(
        builder: (BuildContext context) => NearByMarketsPresenter()));
    if (result != null) {
      // bloc.requestDailyPriceEvent.launch();
    }
  }

  _barChart(BuildContext context, NearestMarketViewModel item,
      DashBoardViewModel viewModel) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return ShowPriceListForIndividualMarket(
            item: item,
            viewModel: viewModel,
          );
        });
  }
}
