import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/core/api/image_convert/image_convert.dart';
import 'package:ono_flutter/core/api/location/geocoder_api.dart';
import 'package:ono_flutter/core/api/location/location_api.dart';
import 'package:ono_flutter/core/api/response_handler.dart';
import 'package:ono_flutter/core/api/user_info/user_info.dart';
import 'package:ono_flutter/features/dash_board/api/daily_price/daily_price_request.dart';
import 'package:ono_flutter/features/dash_board/api/daily_price/daily_price_response.dart';
import 'package:ono_flutter/features/dash_board/api/daily_price/daily_price_service.dart';
import 'package:ono_flutter/features/dash_board/api/preferredCrops/preferred_crops_request_model.dart';
import 'package:ono_flutter/features/dash_board/api/preferredCrops/preferred_crops_response.dart';
import 'package:ono_flutter/features/dash_board/api/preferredCrops/preferred_crops_service.dart';
import 'package:ono_flutter/features/dash_board/model/dashboard_business_model.dart';
import 'package:ono_flutter/features/dash_board/model/dashboard_view_model.dart';
import 'package:ono_flutter/features/dash_board/nearby_markets/api/nearby_markets_request.dart';
import 'package:ono_flutter/features/dash_board/nearby_markets/api/nearby_markets_response.dart';
import 'package:ono_flutter/features/dash_board/nearby_markets/api/nearby_markets_service.dart';
import 'package:ono_flutter/features/dash_board/nearby_markets/model/nearby_markets_business_model.dart';
import 'package:ono_flutter/features/dash_board/nearby_markets/model/nearby_markets_view_model.dart';
import 'package:ono_flutter/features/select_crops/model/select_crops_view_model.dart';
import 'package:ono_flutter/ui_utils/validations.dart';

class DashBoardBloc extends ResponseHandlerBloc implements Bloc {
  //variables
  String _onoId = '16';
  String _location = '12&14';
  String _marketId = '';
  //Business Model
  DashBoardBusinessModel _dashBoardBusinessModel;
  NearByMarketsBusinessModel _nearByMarketBusinessModel;

  //service
  DailyPriceService _dailyPriceService;
  PreferredCropsService _preferredCropsService;
  NearByMarketsService _nearByMarketsService;

  LocationAPI _locationAPI;

  //DashBoard
  //Events
  EventPipe requestDailyPriceEvent = EventPipe();
  BroadcastPipe<DashBoardViewModel> dailyPriceViewModelEvent =
      BroadcastPipe<DashBoardViewModel>();
  Pipe<String> updateSelectedCropEvent = Pipe<String>();
  EventPipe requestCropsPreferenceEvent = EventPipe();
  EventPipe requestLocationEvent = EventPipe();
  Pipe<TapActionArrows> requestPreviousDateEvent = Pipe<TapActionArrows>();
  Pipe<int> requestNextCropEvent = Pipe<int>();

  //NearBy Markets
  EventPipe requestNearByMarketsEvent = EventPipe();
  BroadcastPipe<NearByMarketsViewModel> nearbyMarketsViewModelEvent =
      BroadcastPipe<NearByMarketsViewModel>();
  Pipe<String> updateSelectedMarketEvent = Pipe<String>();
  Pipe<String> updateChangedMarketEvent = Pipe<String>();
  DashBoardBloc(
      {DashBoardBusinessModel dashBoardBusinessModel,
      DailyPriceService dailyPriceService,
      PreferredCropsService preferredCropsService,
      LocationAPI locationAPI,
      NearByMarketsService nearByMarketsService,
      NearByMarketsBusinessModel nearByMarketBusinessModel}) {
    //Object creation
    _dashBoardBusinessModel =
        dashBoardBusinessModel ?? DashBoardBusinessModel();
    _nearByMarketBusinessModel =
        nearByMarketBusinessModel ?? NearByMarketsBusinessModel();
    _dailyPriceService = dailyPriceService ?? DailyPriceService(this);
    _preferredCropsService =
        preferredCropsService ?? PreferredCropsService(this);
    _nearByMarketsService = nearByMarketsService ?? NearByMarketsService(this);
    _locationAPI = locationAPI ?? LocationAPI();

    requestDailyPriceEvent.listen(_requestDailyPriceServiceEvent);
    requestLocationEvent.listen(_requestLocation);
    updateSelectedCropEvent.receive.listen(_updateCropTapEvent);
    requestNextCropEvent.receive.listen((_navigateToNextCrop));
    requestPreviousDateEvent.receive.listen(_updatePreviousDailyPriceViewModel);
    _requestRequiredInfo();
    requestNearByMarketsEvent.listen(_getNearbyMarketPrices);
    updateSelectedMarketEvent.receive.listen(_updateSelectedMarketValue);
    updateChangedMarketEvent.receive.listen(_nearestLocationUpdate);
  }

  //Methods

  _requestRequiredInfo() async {
    // _location = '';
    // _onoId = '';
    _location = await UserInfoAPI.getLocationDetails();
    _onoId = await UserInfoAPI.getOnoId();
  }

  _requestDailyPriceServiceEvent() async {
    UserInfoAPI.setUserAppStatus(userAppStatus: UserAppStatus.dashBoard);
    _dashBoardBusinessModel.preferredCropsBusinessList = [];
    _dashBoardBusinessModel.nearestMarketsBusinessList = [];

    if (_location == null || _location.isEmpty) {
      await _getPreferredCrops();
      _dashBoardBusinessModel.dashBoardUIConditions =
          DashBoardUIConditions.noLocation;
    } else {
      List list = _location.split('&');

      Address address = await _getAddressFromLatAndLong(
          lat: list[0].toString(), long: list[1].toString());
      _dashBoardBusinessModel.location = address.locality;
      DailyPriceResponseModel dailyPriceResponseModel =
          await _dailyPriceService.request(
              data: DailyPriceRequestModel(
                  onoId: _onoId,
                  lat: list[0].toString(),
                  long: list[1].toString()));
      if (dailyPriceResponseModel != null &&
          dailyPriceResponseModel.data != null) {
        if (dailyPriceResponseModel.data.preferedCropPrice.length > 0) {
          _dashBoardBusinessModel.dashBoardUIConditions =
              DashBoardUIConditions.preferredCropsWithInLocation;
          List<NearestMarketBusinessModel> nearByMarketsList = [];
          for (int i = 0;
              i < dailyPriceResponseModel.data.preferedCropPrice.length;
              i++) {
            var element = dailyPriceResponseModel.data.preferedCropPrice[i];
            PreferredCropsBusinessModel preferredCropsBusinessModel =
                PreferredCropsBusinessModel();
            preferredCropsBusinessModel.cropId = element.cropId;
            preferredCropsBusinessModel.cropName = element.cropName;
            preferredCropsBusinessModel.imageUrl = element.imageUrl;
            preferredCropsBusinessModel.index = i;
            if (element.nearestMarkets != null &&
                element.nearestMarkets.length > 0) {
              element.nearestMarkets.forEach((element) {
                NearestMarketBusinessModel nearestMarketBusinessModel =
                    NearestMarketBusinessModel();
                nearestMarketBusinessModel.marketId = element.market.marketId;
                nearestMarketBusinessModel.marketName =
                    element.market.marketName;
                nearestMarketBusinessModel.radius =
                    element.market.distance.roundToDouble().toString();
                nearestMarketBusinessModel.address =
                    element.market.address.addressLine +
                        ' , ' +
                        element.market.address.state +
                        ' , ' +
                        element.market.address.city +
                        ' , ' +
                        element.market.address.pincode.toString();

                if (element.prices != null && element.prices.length > 0) {
                  for (int i = 1; i < 7; i++) {
                    PriceBusinessModel priceBusinessModel =
                        PriceBusinessModel();
                    if (i == 1) {
                      priceBusinessModel.date = Validations.pickDate();
                    } else {
                      priceBusinessModel.date =
                          Validations.previousDateFromTodayDate(number: i - 1);
                    }
                    nearestMarketBusinessModel.pricesBusinessList
                        .add(priceBusinessModel);
                  }

                  element.prices.forEach((price) {
                    nearestMarketBusinessModel.pricesBusinessList
                        .forEach((element) {
                      if (element.date == price.date) {
                        element.max = price.max;
                        element.min = price.min;
                      }
                    });
                  });
                }
                nearestMarketBusinessModel.pricesBusinessList.first.isSelected =
                    true;

                preferredCropsBusinessModel.nearestMarketsBusinessList
                    .add(nearestMarketBusinessModel);

                if (_dashBoardBusinessModel.nearestMarketsBusinessList.length <=
                    0) {
                  _dashBoardBusinessModel.nearestMarketsBusinessList
                      .add(nearestMarketBusinessModel);
                }
              });
            }
            _dashBoardBusinessModel.preferredCropsBusinessList
                .add(preferredCropsBusinessModel);
          }
        } else {
          _dashBoardBusinessModel.dashBoardUIConditions =
              DashBoardUIConditions.preferredCropsNotInLocation;
          await _getPreferredCrops();
        }
      }
    }

    for (int i = 0;
        i < _dashBoardBusinessModel.preferredCropsBusinessList.length;
        i++) {
      var item = _dashBoardBusinessModel.preferredCropsBusinessList[i];
      item.uint8list = await ConvertNetWorkImageIntoMemory.convertImage(
          imageUrl: item.imageUrl, cropId: item.cropId);
      if (item.nearestMarketsBusinessList.length > 0) {
        item.nearestMarketsBusinessList[0].image = item.uint8list;
      }
    }

    if (_dashBoardBusinessModel.location != null &&
        _dashBoardBusinessModel.location.isNotEmpty) {
      if (_dashBoardBusinessModel.preferredCropsBusinessList.length > 0 &&
          _dashBoardBusinessModel.dashBoardUIConditions ==
              DashBoardUIConditions.preferredCropsWithInLocation) {
        _dashBoardBusinessModel.preferredCropsBusinessList[0].isSelected = true;
      }
    }
    _passDashBoardViewModel();
  }

  _getPreferredCrops() async {
    PreferredCropsResponseModel preferredCropsResponseModel =
        await _makePreferredCropsRequest();
    if (preferredCropsResponseModel != null &&
        preferredCropsResponseModel.data != null) {
      preferredCropsResponseModel.data.forEach((element) {
        _dashBoardBusinessModel.preferredCropsBusinessList
            .add(PreferredCropsBusinessModel()
              ..cropId = element.prefId
              ..cropName = element.cropName
              ..imageUrl = element.image);
      });
    }
  }

  _getAddressFromLatAndLong({String lat, String long}) async {
    Address address =
        await GeoCoderAPI.getAddressFromLatAndLong(lat: lat, long: long);
    return address;
  }

  _makePreferredCropsRequest() async {
    PreferredCropsResponseModel preferredCropsResponseModel =
        await _preferredCropsService.request(
            data: PreferredRequestModel(onoId: _onoId, type: 'CROP'));
    return preferredCropsResponseModel;
  }

  _prePareDashBoardViewModel() {
    List<PreferredCropsViewModel> preferredCropsViewList = [];
    List<NearestMarketViewModel> nearestMarketsViewList = [];
    List<PriceViewModel> pricesViewList = [];
    _dashBoardBusinessModel.preferredCropsBusinessList.forEach((element) {
      nearestMarketsViewList = [];
      element.nearestMarketsBusinessList.forEach((element) {
        pricesViewList = [];
        element.pricesBusinessList.forEach((element) {
          pricesViewList.add(PriceViewModel(
              date: element.date,
              max: element.max,
              min: element.min,
              isSelected: element.isSelected));
        });
        nearestMarketsViewList.add(NearestMarketViewModel(
            pricesViewList: pricesViewList,
            marketId: element.marketId,
            marketName: element.marketName,
            image: element.image,
            address: element.address,
            radius: element.radius));
      });
      preferredCropsViewList.add(PreferredCropsViewModel(
          cropName: element.cropName,
          cropId: element.cropId,
          imageUrl: element.imageUrl,
          uint8list: element.uint8list,
          isSelected: element.isSelected,
          distance: element.distance,
          index: element.index,
          nearestMarketsViewList: nearestMarketsViewList));
    });
    return DashBoardViewModel(
        preferredCropsViewList: preferredCropsViewList,
        nearestMarketsViewList: nearestMarketsViewList,
        location: _dashBoardBusinessModel.location,
        dashBoardUIConditions: _dashBoardBusinessModel.dashBoardUIConditions);
  }

  _passDashBoardViewModel() {
    dailyPriceViewModelEvent.send(_prePareDashBoardViewModel());
  }

  _requestLocation() async {
    // if (PermissionStatus.granted ==
    //     await _locationAPI.checkAndRequestPermission()) {
    //   if (await _locationAPI.checkAndRequestService()) {
    LocationData locationData = await _locationAPI.getLocation();
    if (locationData != null) {
      _location = '${locationData.latitude}&${locationData.longitude}';
      Address address = await _getAddressFromLatAndLong(
          lat: locationData.latitude.toString(),
          long: locationData.longitude.toString());
      _dashBoardBusinessModel.location = address.locality;
      _dashBoardBusinessModel.preferredCropsBusinessList[0].isSelected = true;
      _passDashBoardViewModel();
      UserInfoAPI.setLocationDetails(
          '${locationData.latitude}&${locationData.longitude}');

      //   }
      // }
    }
  }

  _updateCropTapEvent(String cropId) {
    for (int i = 0;
        i < _dashBoardBusinessModel.preferredCropsBusinessList.length;
        i++) {
      var element = _dashBoardBusinessModel.preferredCropsBusinessList[i];
      if (element.cropId == cropId) {
        element.isSelected = true;
        // print(i);
        // if (i == 0) {
        //   element.distance = 40.toDouble();
        // } else {
        //   element.distance = i + 1 * 110.toDouble();
        // }
        _updateDailyPriceGraphData(element);
      } else {
        element.isSelected = false;
      }
    }
    // _dashBoardBusinessModel.preferredCropsBusinessList.forEach((element) {
    //
    // });
    _passDashBoardViewModel();
  }

  _navigateToNextCrop(int index) {
    int incrementedValue;
    if (_dashBoardBusinessModel.preferredCropsBusinessList.length - 1 ==
        index) {
      incrementedValue = 0;
    } else {
      incrementedValue = index + 1;
    }
    for (int i = 0;
        i < _dashBoardBusinessModel.preferredCropsBusinessList.length;
        i++) {
      var element = _dashBoardBusinessModel.preferredCropsBusinessList[i];
      if (element.index == incrementedValue) {
        element.isSelected = true;
      } else {
        element.isSelected = false;
      }
    }
    _passDashBoardViewModel();
  }

  _updateDailyPriceGraphData(PreferredCropsBusinessModel element) {
    _dashBoardBusinessModel.nearestMarketsBusinessList = [];
    NearestMarketBusinessModel nearestMarketBusinessModel =
        NearestMarketBusinessModel();
    element.nearestMarketsBusinessList.forEach((element) {
      nearestMarketBusinessModel.marketName = element.marketName;
      nearestMarketBusinessModel.marketId = element.marketId;
      nearestMarketBusinessModel.radius = element.radius;
      nearestMarketBusinessModel.pricesBusinessList =
          element.pricesBusinessList;
      nearestMarketBusinessModel.pricesBusinessList.forEach((element) {
        if (element.date == Validations.pickDate()) {
          element.isSelected = true;
        } else {
          element.isSelected = false;
        }
      });
    });

    _dashBoardBusinessModel.nearestMarketsBusinessList
        .add(nearestMarketBusinessModel);
  }

  _updatePreviousDailyPriceViewModel(TapActionArrows tapActionArrows) {
    _dashBoardBusinessModel.nearestMarketsBusinessList = [];
    NearestMarketBusinessModel nearestMarketBusinessModel =
        NearestMarketBusinessModel();

    _dashBoardBusinessModel.preferredCropsBusinessList.forEach((element) {
      if (element.isSelected) {
        element.nearestMarketsBusinessList.forEach((element) {
          nearestMarketBusinessModel.marketName = element.marketName;
          nearestMarketBusinessModel.marketId = element.marketId;
          nearestMarketBusinessModel.radius = element.radius;
          nearestMarketBusinessModel.pricesBusinessList =
              element.pricesBusinessList;
          var priceSelectedModel = nearestMarketBusinessModel.pricesBusinessList
              .where((element) => element.isSelected)
              .first;
          String previousDate;
          if (tapActionArrows == TapActionArrows.prev) {
            previousDate = Validations.previousDate(
                date: priceSelectedModel.date, increment: true);
          } else if (tapActionArrows == TapActionArrows.next &&
              priceSelectedModel.date != Validations.pickDate()) {
            previousDate = Validations.previousDate(
                date: priceSelectedModel.date, increment: false);
          } else {
            previousDate = priceSelectedModel.date;
          }
          if (previousDate != priceSelectedModel.date) {
            for (int i = 0;
                i < nearestMarketBusinessModel.pricesBusinessList.length;
                i++) {
              var element = nearestMarketBusinessModel.pricesBusinessList[i];
              if (element.date == previousDate) {
                nearestMarketBusinessModel.pricesBusinessList[i].isSelected =
                    true;
                // element.isSelected = true;
              } else {
                nearestMarketBusinessModel.pricesBusinessList[i].isSelected =
                    false;
              }
            }
          }
        });
      }
    });

    _dashBoardBusinessModel.nearestMarketsBusinessList
        .add(nearestMarketBusinessModel);
    _passDashBoardViewModel();
  }

  _getNearbyMarketPrices() async {
    // var result = _dashBoardBusinessModel.preferredCropsBusinessList
    //     .where((element) => element.isSelected)
    //     .first;
    List<NearestMarketBusinessModel> list = [];
    _dashBoardBusinessModel.preferredCropsBusinessList.forEach((element) {
      if (element.isSelected) {
        element.nearestMarketsBusinessList.forEach((element) {
          if (_marketId == element.marketId) {
            _nearByMarketBusinessModel.nearestMarketsBusinessList
                .add(NearbyMarketBusinessModel()
                  ..marketId = element.marketId
                  ..marketName = element.marketName
                  ..address = element.address
                  ..radius = element.radius);
          }
        });
        element.nearestMarketsBusinessList.forEach((element) {
          if (_marketId != element.marketId) {
            _nearByMarketBusinessModel.nearestMarketsBusinessList
                .add(NearbyMarketBusinessModel()
                  ..marketId = element.marketId
                  ..marketName = element.marketName
                  ..address = element.address
                  ..radius = element.radius);
          }
        });
      }
    });
    _nearByMarketBusinessModel.nearestMarketsBusinessList.forEach((element) {
      if (element.marketId == _marketId) {
        element.isSelected = true;
      } else {
        element.isSelected = false;
      }
    });
    await _makeNearByMarketsService(marketId: _marketId);
    _passNearByMarketsViewModel();
  }

  _makeNearByMarketsService({String marketId}) async {
    NearByMarketsResponseModel nearByMarketsResponseModel =
        await _nearByMarketsService.request(
            data: NearbyMarketsRequestModel(onoId: _onoId, marketId: marketId));
    if (nearByMarketsResponseModel != null &&
        nearByMarketsResponseModel.data != null) {
      nearByMarketsResponseModel.data.crops.forEach((element) {
        _nearByMarketBusinessModel.cropsList.add(CropBusinessModel()
          ..cropName = element.cropName
          ..cropId = element.cropId
          ..imageUrl = element.imageUrl
          ..maxPrice = element.maxPrice
          ..minPrice = element.minPrice
          ..modalPrice = element.modalPrice
          ..preferredCrop = element.preferedCrop);
      });
    }
  }

  _prePareNearByMarketViewModel() {
    List<NearbyMarketViewModel> list = [];
    List<CropDetailsViewModel> cropsList = [];
    _nearByMarketBusinessModel.nearestMarketsBusinessList.forEach((element) {
      list.add(NearbyMarketViewModel(
          marketId: element.marketId,
          marketName: element.marketName,
          radius: element.radius,
          address: element.address,
          isSelected: element.isSelected));
    });

    _nearByMarketBusinessModel.cropsList.forEach((element) {
      cropsList.add(CropDetailsViewModel(
          cropName: element.cropName,
          cropId: element.cropId,
          imageUrl: element.imageUrl,
          maxPrice: element.maxPrice,
          minPrice: element.minPrice,
          modalPrice: element.modalPrice,
          preferedCrop: element.preferredCrop));
    });

    return NearByMarketsViewModel(
        cropsList: cropsList, nearestMarketsViewList: list);
  }

  _passNearByMarketsViewModel() {
    nearbyMarketsViewModelEvent.send(_prePareNearByMarketViewModel());
  }

  _updateSelectedMarketValue(String marketId) {
    _marketId = marketId;
  }

  _nearestLocationUpdate(String marketId) async {
    _nearByMarketBusinessModel.nearestMarketsBusinessList.forEach((element) {
    //  _marketId = element.marketId;
      if (element.marketId == marketId) {
        element.isSelected = true;
      } else {
        element.isSelected = false;
      }
    });
    await _makeNearByMarketsService(marketId: marketId);
    _passNearByMarketsViewModel();
  }

  @override
  void dispose() {
    requestDailyPriceEvent.dispose();
    dailyPriceViewModelEvent.dispose();
    requestLocationEvent.dispose();
    updateSelectedCropEvent.dispose();
    requestPreviousDateEvent.dispose();
    requestNextCropEvent.dispose();
    requestNearByMarketsEvent.dispose();
    nearbyMarketsViewModelEvent.dispose();
    updateSelectedMarketEvent.dispose();
    updateChangedMarketEvent.dispose();
    super.dispose();
  }
}
