import 'dart:typed_data';
import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class DashBoardViewModel implements ViewModel {
  final List<PreferredCropsViewModel> preferredCropsViewList;
  final List<NearestMarketViewModel> nearestMarketsViewList;
  final String location;
  final DashBoardUIConditions dashBoardUIConditions;

  DashBoardViewModel(
      {this.preferredCropsViewList,
      this.nearestMarketsViewList,
      this.location,
      this.dashBoardUIConditions});
}

class PreferredCropsViewModel implements ViewModel {
  final String cropName;
  final String cropId;
  final String imageUrl;
  final bool isSelected;
  final Uint8List uint8list;
  final dynamic distance;
  final int index;
  final List<NearestMarketViewModel> nearestMarketsViewList;

  PreferredCropsViewModel(
      {this.cropName,
      this.cropId,
      this.imageUrl,
      this.isSelected,
      this.uint8list,
      this.nearestMarketsViewList,
      this.distance,
      this.index});
}

class NearestMarketViewModel implements ViewModel {
  final String marketId;
  final String marketName;
  final Uint8List image;
  final List<PriceViewModel> pricesViewList;
  final String radius;
  final String address;

  NearestMarketViewModel({
    this.marketId,
    this.marketName,
    this.pricesViewList,
    this.radius,
    this.address,
    this.image,
  });
}

class PriceViewModel implements ViewModel {
  final String date;
  final dynamic max;
  final dynamic min;
  final isSelected;

  PriceViewModel({this.date, this.max, this.min, this.isSelected});
}
