import 'dart:typed_data';

import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class DashBoardBusinessModel implements BusinessModel {
  List<PreferredCropsBusinessModel> preferredCropsBusinessList = [];
  List<NearestMarketBusinessModel> nearestMarketsBusinessList = [];
  String location = '';
  DashBoardUIConditions dashBoardUIConditions = DashBoardUIConditions.none;
}

class PreferredCropsBusinessModel implements BusinessModel {
  String cropName = '';
  String cropId = '';
  String imageUrl = '';
  Uint8List uint8list = Uint8List(0);
  int index = 0;
  bool isSelected = false;
  dynamic distance = 40.0;
  List<NearestMarketBusinessModel> nearestMarketsBusinessList = [];
}

class NearestMarketBusinessModel implements BusinessModel {
  String marketId = '';
  String marketName = '';
  String address = '';
  Uint8List image = Uint8List(0);
  List<PriceBusinessModel> pricesBusinessList = [];
  String radius = '';
}

class PriceBusinessModel implements BusinessModel {
  String date = '';
  dynamic max = 0.0;
  dynamic min = 0.0;
  bool isSelected = false;
}
