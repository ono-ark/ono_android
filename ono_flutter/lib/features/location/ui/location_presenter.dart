import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:ono_clean_framework/ui/presenter.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/features/location/bloc/location_bloc.dart';
import 'package:ono_flutter/features/location/model/location_view_model.dart';
import 'package:ono_flutter/features/location/ui/location_screen.dart';
import 'package:ono_flutter/features/sign_in/ui/feature_widget.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

class LocationPresenter extends Presenter<LocationBloc, LocationViewModel> {
  @override
  Widget buildLoadingScreen(BuildContext context) {
    return GenericProgressbar();
  }

  @override
  Widget buildScreen(
      BuildContext context, LocationBloc bloc, LocationViewModel viewModel) {
    if (viewModel.locationStatus == LocationStatus.allow ||
        viewModel.locationStatus == LocationStatus.deny) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        _navigation(context);
      });
    }
    return LocationScreen(
      locationViewModel: viewModel,
      allowTapEvent: () {
        bloc.requestLocationEvent.launch();
      },
      denyTapEvent: () {
        _navigation(context);
      },
    );
  }

  @override
  Stream<LocationViewModel> getViewModelStream(
      BuildContext context, LocationBloc bloc) {
    return bloc.locationViewModelPipe.receive;
  }

  @override
  void sendViewModelRequest(BuildContext context, LocationBloc bloc) {
    bloc.locationUIRequestEvent.launch();
  }

  _navigation(BuildContext context) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => SignInFeatureWidget()),
        (route) => false);
  }
}
