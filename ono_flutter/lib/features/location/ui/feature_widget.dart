import 'package:flutter/material.dart';
import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_clean_framework/bloc/bloc_provider.dart';
import 'package:ono_flutter/common/features/navigator/navigator.dart';
import 'package:ono_flutter/features/location/bloc/location_bloc.dart';
import 'package:ono_flutter/features/location/ui/location_presenter.dart';

class LocationFeatureWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: LocationBloc(),
      child: FeatureNavigator(sectionChild: LocationPresenter()),
    );
  }
}
