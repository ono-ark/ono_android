import 'package:flutter/material.dart';
import 'package:ono_flutter/features/location/model/location_view_model.dart';
import 'package:ono_flutter/features/location/strings/strings.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/ui_utils/ono_widgets.dart';

var locationStrings = LocationStrings();

class LocationScreen extends StatelessWidget {
  final LocationViewModel locationViewModel;
  final VoidCallback allowTapEvent;
  final VoidCallback denyTapEvent;

  const LocationScreen(
      {Key key, this.locationViewModel, this.allowTapEvent, this.denyTapEvent})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: whiteColor,
        image: DecorationImage(
            image: AssetImage("assets/Images/png/background.png"),
            fit: BoxFit.contain,
            alignment: Alignment.bottomCenter),
      ),
      child: _buildBody(context),
    );
  }

  _buildBody(BuildContext context) {
    return ListView(
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height / 12,
        ),
        PaddingLeftAndRight(
          child: GenericTextWithStyle(
            text: locationStrings.accessToDeviceLocation,
            textStyle: theme.headline2.copyWith(color: headLine1Color),
          ),
        ),
        PaddingLeftAndRight(
          child: Padding(
            padding: paddingSpaceFromHeadline2,
            child: GenericTextWithStyle(
              text: locationStrings.agfinLocationContent,
              textStyle:
                  theme.headline4.copyWith(color: termsAndConditionsColor,height: 1.2 ),
            ),
          ),
        ),
        _buildImage(),
        _buildAllowButton(),
        _buildDenyButton()
      ],
    );
  }

  _buildImage() {
    return Padding(
      padding: paddingSpaceFromAppbar,
      child: Container(
        height: 100,
        child: Image.asset(
          'assets/Images/png/location.png',
        ),
      ),
    );
  }

  _buildAllowButton() {
    return Padding(
      padding: paddingSpaceFromHeadline2,
      child: ButtonWithTapEvent(
        text: locationStrings.allow,
        textStyle: theme.headline4.copyWith(color: whiteColor),
        tapCallBack: allowTapEvent,
      ),
    );
  }

  _buildDenyButton() {
    return Padding(
      padding: paddingSpaceFromHeadline2,
      child: ButtonWithTapEvent(
        text: locationStrings.deny,
        textStyle: theme.headline4.copyWith(color: nearByMarketsSubTitleColor),
        backGroundColor: priceBorderColor,
        tapCallBack: denyTapEvent,
      ),
    );
  }
}
