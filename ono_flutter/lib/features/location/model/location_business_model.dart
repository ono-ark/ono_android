import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class LocationBusinessModel implements BusinessModel {
  String latitude = '';
  String longitude = '';
  LocationStatus locationStatus = LocationStatus.none;
}
