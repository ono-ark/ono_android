import 'package:ono_clean_framework/model.dart';
import 'package:ono_flutter/common/enums.dart';

class LocationViewModel implements ViewModel {
  final String latitude;
  final String longitude;
  final LocationStatus locationStatus;

  LocationViewModel({this.latitude, this.longitude, this.locationStatus});
}
