class LocationStrings {
  LocationStrings();
  get accessToDeviceLocation => 'Access to device location';
  get agfinLocationContent =>
      'To provide you with accurate local content, Agfin require access to your device location.';
  get allow => 'ALLOW ONLY WHILE USING THE APP';
  get deny => 'DENY';
}
