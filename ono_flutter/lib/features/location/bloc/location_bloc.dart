import 'package:location/location.dart';
import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/core/api/location/location_api.dart';
import 'package:ono_flutter/core/api/response_handler.dart';
import 'package:ono_flutter/core/api/user_info/user_info.dart';
import 'package:ono_flutter/features/location/model/location_business_model.dart';
import 'package:ono_flutter/features/location/model/location_view_model.dart';

class LocationBloc extends ResponseHandlerBloc implements Bloc {
  //Business model
  LocationBusinessModel _locationBusinessModel;

  //Api
  LocationAPI _locationAPI;

  //Events
  EventPipe locationUIRequestEvent = EventPipe();
  BroadcastPipe<LocationViewModel> locationViewModelPipe =
      BroadcastPipe<LocationViewModel>();
  EventPipe requestLocationEvent = EventPipe();

  LocationBloc(
      {LocationBusinessModel locationBusinessModel, LocationAPI locationAPI}) {
    //object of business models
    _locationBusinessModel = locationBusinessModel ?? LocationBusinessModel();

    _locationAPI = locationAPI ?? LocationAPI();

    locationUIRequestEvent.listen(passLocationViewModel);
    requestLocationEvent.listen(_requestLocation);
  }

  void passLocationViewModel() async {
    UserInfoAPI.setUserAppStatus(userAppStatus: UserAppStatus.location);
    locationViewModelPipe.send(_prePareLocationViewModel());
  }

  _prePareLocationViewModel() {
    return LocationViewModel(
        latitude: _locationBusinessModel.latitude,
        locationStatus: _locationBusinessModel.locationStatus,
        longitude: _locationBusinessModel.longitude);
  }

  _requestLocation() async {
    // if (PermissionStatus.granted ==
    //     await _locationAPI.checkAndRequestPermission()) {
    //   if (await _locationAPI.checkAndRequestService()) {
    LocationData locationData = await _locationAPI.getLocation();
    if (locationData != null) {
      _locationBusinessModel.locationStatus = LocationStatus.allow;
      _locationBusinessModel.longitude = locationData.longitude.toString();
      _locationBusinessModel.latitude = locationData.latitude.toString();
      UserInfoAPI.setLocationDetails(
          '${_locationBusinessModel.latitude}&${_locationBusinessModel.longitude}');
      //   }
      // }
    } else {
      _locationBusinessModel.locationStatus = LocationStatus.deny;
    }
    passLocationViewModel();
    _locationBusinessModel.locationStatus = LocationStatus.none;
  }

  @override
  void dispose() {
    locationUIRequestEvent.dispose();
    locationViewModelPipe.dispose();
    requestLocationEvent.dispose();
    super.dispose();
  }
}
