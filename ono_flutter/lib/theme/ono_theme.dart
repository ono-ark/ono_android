import 'package:flutter/material.dart';

Color headLine1Color = Color(0xFF293039);
Color headLine2Color = Color(0xFF16A02C);
Color underLineColor = Color(0xFF1B85FB);
Color whiteColor = Color(0xFFFFFFFF);
Color textFieldBorderColor = Color(0xFF18AA58);
Color termsAndConditionsColor = Color(0xFF4D4D4D);
Color errorColor = Color(0xFFFD0D1B);
Color backGroundColor = Color(0xFFF7F7F7);
Color timerTextColor = Color(0xFF767676);
Color takeAPhotoTextColor = Color(0xFF09101D);
Color photoFromGalleryTextColor = Color(0xFF545D69);
Color capturePhotoTextColor = Color(0xFF0733B6);
Color selectedTickMarkColor = Color(0xFF2196F3);
Color blackColor = Color(0xFF000000);
Color priceBorderColor = Color(0xFFE4E4E4);
Color graphBackgroundColor = Color(0xFFC8E4CC);
Color nearByMarketsSubTitleColor = Color(0xFF777777);
Color locationBoarder = Color(0xFF707070);

TextTheme buildOnoTextTheme() {
  return TextTheme(
    headline1: TextStyle(
        fontSize: 28.0,
        fontFamily: 'Steinem',
        color: headLine1Color,
        fontWeight: FontWeight.w600),
    headline2: TextStyle(
        fontSize: 22.0,
        fontFamily: 'Steinem',
        color: headLine2Color,
        fontWeight: FontWeight.w600),
    headline3: TextStyle(
        fontSize: 18.0,
        fontFamily: 'Steinem',
        color: whiteColor,
        fontWeight: FontWeight.w600),
    headline4: TextStyle(
        fontSize: 15.0,
        fontFamily: 'Steinem',
        color: termsAndConditionsColor,
        fontWeight: FontWeight.w500),
  );
}
