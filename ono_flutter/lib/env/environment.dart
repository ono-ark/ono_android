import 'package:flutter/foundation.dart';
import 'package:ono_flutter/common/enums.dart';

class OnoEnvironment {
  static String devUrl =
      'http://ec2-54-147-124-58.compute-1.amazonaws.com:8080/api/v1/';
  static String prodUrl =
      'http://ec2-34-239-104-77.compute-1.amazonaws.com:8080/api/v1/';
  //Dev url
  static String serverUrl = prodUrl;

  setEnvironment() {
    if (kReleaseMode) {
      print('release mode');
      serverUrl = prodUrl;
    } else {
      print('debug mode');
      serverUrl = prodUrl;
    }
  }

  setEnvironmentOnTapEvent(OnoEnvironments OnoEnvironment) {
    switch (OnoEnvironment) {
      case OnoEnvironments.dev:
        serverUrl = devUrl;
        break;
      case OnoEnvironments.prod:
        serverUrl = prodUrl;
        break;
      case OnoEnvironments.none:
        serverUrl = devUrl;
        break;
    }
  }
}
