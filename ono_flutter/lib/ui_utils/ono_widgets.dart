import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ono_flutter/core/api/user_info/user_info.dart';
import 'package:ono_flutter/features/sign_in/strings/sign_in_strings.dart';
import 'package:ono_flutter/theme/ono_theme.dart';
import 'package:ono_flutter/core/api/image_convert/image_convert.dart';

var theme = buildOnoTextTheme();
var signInStrings = SignInStrings();

EdgeInsets paddingSpaceFromHeadline2 = EdgeInsets.only(top: 20);
EdgeInsets paddingSpaceFromAppbar = EdgeInsets.only(top: 35);
EdgeInsets paddingBottom = EdgeInsets.only(top: 24);
EdgeInsets paddingBetweenButton = EdgeInsets.only(top: 10);

class GenericProgressbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Theme(
        child: CircularProgressIndicator(),
        data: Theme.of(context).copyWith(accentColor: headLine2Color),
      ),
      color: Colors.white,
    );
  }
}

class PaddingBetweenWidgets extends StatelessWidget {
  final Widget child;

  const PaddingBetweenWidgets({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      child: child,
    );
  }
}

class PaddingLeftAndRight extends StatelessWidget {
  final Widget child;

  const PaddingLeftAndRight({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
      child: child,
    );
  }
}

class PaddingSpaceFromHeadline2Widget extends StatelessWidget {
  final Widget child;

  const PaddingSpaceFromHeadline2Widget({Key key, this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: paddingSpaceFromHeadline2,
      child: child,
    );
  }
}

class GenericTextWithStyle extends StatelessWidget {
  final String text;
  final TextStyle textStyle;
  final TextAlign textAlign;

  const GenericTextWithStyle(
      {Key key, this.text, this.textStyle, this.textAlign})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: textStyle,
      textAlign: textAlign ?? TextAlign.start,
    );
  }
}

class AppBarWidget extends StatelessWidget {
  final String text;
  final bool showBackIcon;
  final VoidCallback voidCallback;
  final Widget widget;
  final double height;

  const AppBarWidget(
      {Key key,
      this.text,
      this.showBackIcon = false,
      this.voidCallback,
      this.widget,
      this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget appBar;
    if (text == null) {
      appBar = widget;
    } else {
      appBar = Row(
        children: [
          showBackIcon
              ? Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: GestureDetector(
                    onTap: voidCallback,
                    child: Icon(
                      Icons.arrow_back,
                      color: headLine1Color,
                      size: 20,
                    ),
                  ),
                )
              : IgnoreWidget(),
          showBackIcon
              ? SizedBox(
                  width: 10,
                )
              : IgnoreWidget(),
          GenericTextWithStyle(
            text: text,
            textStyle: theme.headline1,
          ),
        ],
      );
    }
    return Card(
      elevation: 3,
      margin: EdgeInsets.all(0.0),
      child: Container(
        height: height ?? 119,
        color: whiteColor,
        padding: EdgeInsets.only(top: 43, bottom: 48),
        child: PaddingLeftAndRight(
          child: appBar,
        ),
      ),
    );
  }
}

class AppBarWithMultipleOptions extends StatelessWidget {
  final String title;
  final String subtitle;
  final String profileImage;
  final bool isHideTrailing;
  final VoidCallback languageTapEvent;
  final VoidCallback profileTapEvent;
  final VoidCallback backButtonEvent;
  final double size;

  const AppBarWithMultipleOptions(
      {Key key,
      this.title,
      this.subtitle,
      this.profileImage,
      this.isHideTrailing = true,
      this.languageTapEvent,
      this.profileTapEvent,
      this.backButtonEvent,this.size})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBarWidget(
      height: 130,
      widget: Row(
        children: [
          backButtonEvent == null
              ? IgnoreWidget()
              : GestureDetector(
                  onTap: backButtonEvent,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 0.0, right: 8),
                    child: Icon(Icons.arrow_back),
                  ),
                ),
          Expanded(
            child: ListTile(
              // leading: Icon(Icons.arrow_back),
              contentPadding: EdgeInsets.zero,
              title: GenericTextWithStyle(
                text: title,
                textStyle: theme.headline1.copyWith(fontSize: size??28),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: GenericTextWithStyle(
                  text: subtitle,
                  textStyle: theme.headline4,
                ),
              ),
              // contentPadding: EdgeInsets.symmetric(vertical: 2),
              trailing: isHideTrailing
                  ? IgnoreWidget()
                  : SizedBox(
                      width: 85,
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: languageTapEvent,
                            child: Container(
                              width: 40,
                              height: 40,
                              child: Icon(Icons.language),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          GestureDetector(
                            onTap: profileTapEvent,
                            child: Container(
                              height: 40.0,
                              width: 40.0,
                              decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                              ),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(75.0),
                                  child: FutureProfileWidget()),
                            ),
                          ),
                        ],
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}

class FutureProfileWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (context, snap) {
        if (snap.hasData) {
          return Image.memory(
            base64Decode(snap.data),
            fit: BoxFit.fill,
            gaplessPlayback: true,
            // width: 75,
            // height: 75,
          );
        }
        // return  Transform.scale(
        //     scale: 0.5,
        //     child: CircularProgressIndicator(),
        //   );

        return Container(
          // width: 75,
          // height: 75,
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
          ),
          child: CircularProgressIndicator(),
        );
      },
      future: UserInfoAPI.getUserProfilePic(),
    );
  }
}

class FutureImageLoadingWidget extends StatelessWidget {
  final String imageUrl;
  final String cropId;
  FutureImageLoadingWidget({this.imageUrl, this.cropId});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (context, snap) {
        if (snap.hasData) {
          return Image.memory(
            snap.data,
            fit: BoxFit.fill,
            gaplessPlayback: true,
            width: 75,
            height: 75,
          );
        }
        // return  Transform.scale(
        //     scale: 0.5,
        //     child: CircularProgressIndicator(),
        //   );

        return Container(
          width: 75,
          height: 75,
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
          ),
          child: CircularProgressIndicator(),
        );
      },
      future: ConvertNetWorkImageIntoMemory.convertImage(
          imageUrl: imageUrl, cropId: cropId),
    );
  }
}

class LabelWidget extends StatelessWidget {
  final String text;
  final bool isMandate;
  const LabelWidget({Key key, this.text, this.isMandate}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextAlignment(
      child: PaddingBetweenWidgets(
        child: GenericTextWithStyle(
          text: (isMandate ?? true) ? text + ' *' : text,
          textStyle: theme.headline3,
        ),
      ),
    );
  }
}

class HintWidget extends StatelessWidget {
  final String text;

  const HintWidget({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextAlignment(
      child: PaddingBetweenWidgets(
        child: GenericTextWithStyle(
          text: text,
          textStyle: theme.headline4,
        ),
      ),
    );
  }
}

class TextAlignment extends StatelessWidget {
  final Alignment alignment;
  final Widget child;

  const TextAlignment({Key key, this.alignment, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment ?? Alignment.bottomLeft,
      child: child,
    );
  }
}

class ButtonWithTapEvent extends StatelessWidget {
  final String text;
  final VoidCallback tapCallBack;
  final TextStyle textStyle;
  final Color backGroundColor;

  const ButtonWithTapEvent(
      {Key key,
      this.text,
      this.tapCallBack,
      this.textStyle,
      this.backGroundColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PaddingLeftAndRight(
      child: Container(
        height: 55.0,
        width: double.infinity,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(5.0),
          ),
          child: GenericTextWithStyle(
            text: text,
            textStyle: textStyle ?? theme.headline3,
          ),
          // disabledColor: buttonDisableColor,
          color: backGroundColor ?? headLine2Color,
          onPressed: this.tapCallBack,
        ),
      ),
    );
  }
}

class IgnoreWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IgnorePointer();
  }
}

class TermsAndConditions extends StatelessWidget {
  final String agree;
  final VoidCallback conditionsOfUseTapEvent;
  final VoidCallback privacyPolicyTapEvent;

  const TermsAndConditions(
      {Key key,
      this.agree,
      this.conditionsOfUseTapEvent,
      this.privacyPolicyTapEvent})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return PaddingBetweenWidgets(
      child: Text.rich(
        TextSpan(
          text: agree ?? signInStrings.byContinuing,
          style: theme.headline4,
          children: <InlineSpan>[
            TextSpan(
                recognizer: new TapGestureRecognizer()
                  ..onTap = conditionsOfUseTapEvent,
                text: signInStrings.conditionsOfUse,
                style: theme.headline4.copyWith(
                    decoration: TextDecoration.underline,
                    color: underLineColor,
                    height: 1.2)),
            TextSpan(
                text: signInStrings.and,
                style: theme.headline4.copyWith(height: 1.2)),
            TextSpan(
                recognizer: new TapGestureRecognizer()
                  ..onTap = privacyPolicyTapEvent,
                text: signInStrings.privacyPolicy,
                style: theme.headline4.copyWith(
                    decoration: TextDecoration.underline,
                    color: underLineColor,
                    height: 1.2)),
            // can add more TextSpans here...
          ],
        ),
        // textHeightBehavior: TextHeightBehavior.fromEncoded(7),
      ),
    );
  }
}
