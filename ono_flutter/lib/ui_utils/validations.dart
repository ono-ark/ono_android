import 'package:intl/intl.dart';

class Validations {
  //validate mobile number
  static String validateMobileNumber(String value) {
    if (value == null || value.isEmpty) return 'Mobile Number is required.';
    if (value.length < 10) return 'Please enter 10-digit Mobile Number';
    return null;
  }

  //validate otp
  static String validateOTP(String value) {
    if (value == null || value.isEmpty) return 'OTP is required.';
    if (value.length < 6) return 'Enter valid 6-digit OTP Code.';
    return null;
  }

  //validate name
  static String validateName(String value) {
    if (value == null || value.isEmpty) return 'Please enter full name.';
    return null;
  }

  //Date Format
  static String dateFormatBuilder({String date}) {
    DateTime sysCurrentDate;
    if (date == null) {
      sysCurrentDate = DateTime.now();
    } else {
      sysCurrentDate = DateTime.parse(date);
    }
    final DateFormat dateFormat = DateFormat("EEEE, MMMM dd'th', yyyy");
    final String currentDate = dateFormat.format(sysCurrentDate);
    return currentDate;
  }

  static String pickDate() {
    DateTime sysCurrentDate = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String formatted = formatter.format(sysCurrentDate);
    return formatted;
  }

  static String previousDate({String date, bool increment}) {
    DateTime sysCurrentDate =
        DateTime.parse(date).subtract(Duration(days: increment ? 1 : -1));
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String formatted = formatter.format(sysCurrentDate);
    return formatted;
  }

  static String previousDateFromTodayDate({int number}) {
    DateTime sysCurrentDate =
        DateTime.parse(pickDate()).subtract(Duration(days: number));
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String formatted = formatter.format(sysCurrentDate);
    return formatted;
  }

  static int getDifferenceBetweenDates({String date}) {
    final selectedDate = DateTime.parse(date);
    final currentDate = DateTime.now();
    final difference = currentDate.difference(selectedDate).inDays;
    return difference;
  }
}
