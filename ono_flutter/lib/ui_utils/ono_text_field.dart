import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ono_flutter/theme/ono_theme.dart';

import 'ono_widgets.dart';

enum TextFieldType { PhoneNumber, name, otp, Default }

OutlineInputBorder focusedBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(5.0)),
    borderSide: BorderSide(width: 1.0, color: textFieldBorderColor),
  );
}

OutlineInputBorder errorBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(5.0)),
    borderSide: BorderSide(width: 1.0, color: errorColor),
  );
}

InputDecoration textFieldInputDecoration(
    {@required String hintText,
    @required String errorText,
    bool showStarMark = true}) {
  return InputDecoration(
    fillColor: Colors.white,
    filled: true,
    isDense: true,
    contentPadding: EdgeInsets.all(16),
    errorText: errorText,
    errorBorder: errorBorder(),
    focusedErrorBorder: errorBorder(),
    border: focusedBorder(),
    focusedBorder: focusedBorder(),
    enabledBorder: focusedBorder(),
  );
}

class OnoInput extends StatelessWidget {
  final TextFieldType textFieldType;
  final TextEditingController textEditingController;
  final FocusNode focusNode;
  final String displayErrorMessage;
  final Function functionCallBack;
  final String hintText;
  final int maxLines;
  final bool showStarMark;
  final bool textFieldEnabled;

  OnoInput(
      {@required this.textFieldType,
      @required this.textEditingController,
      this.focusNode,
      this.displayErrorMessage,
      @required this.functionCallBack,
      this.hintText,
      this.maxLines,
      this.showStarMark = true,
      this.textFieldEnabled = true});

  @override
  Widget build(BuildContext context) {
    switch (textFieldType) {
      case TextFieldType.PhoneNumber:
        return buildPhoneNumberTextField();
        break;
      case TextFieldType.Default:
        return buildDefaultTextField();
      case TextFieldType.name:
        return buildNameTextField();
        break;
      case TextFieldType.otp:
        return buildOTPTextField();
        break;
    }
    return null;
  }

  Widget buildDefaultTextField() {
    return TextFormField(
      focusNode: focusNode,
      controller: textEditingController,
      decoration: textFieldInputDecoration(
          errorText: displayErrorMessage,
          showStarMark: showStarMark,
          hintText: ''),
      onChanged: functionCallBack,
      keyboardType: TextInputType.text,
    );
  }

  Widget buildPhoneNumberTextField() {
    return TextFormField(
      focusNode: focusNode,
      controller: textEditingController,
      style: theme.headline2.copyWith(color: headLine1Color),
      decoration: textFieldInputDecoration(
          hintText: hintText,
          errorText: displayErrorMessage,
          showStarMark: showStarMark),
      onChanged: functionCallBack,
      maxLength: 10,
      textAlign: TextAlign.center,
      keyboardType:
          (Platform.isIOS) ? TextInputType.text : TextInputType.number,
      inputFormatters: [
        //LengthLimitingTextInputFormatter(10),
        FilteringTextInputFormatter.digitsOnly,
      ],
    );
  }

  Widget buildOTPTextField() {
    return TextFormField(
      focusNode: focusNode,
      controller: textEditingController,
      style: theme.headline2.copyWith(color: headLine1Color),
      decoration: textFieldInputDecoration(
          hintText: hintText,
          errorText: displayErrorMessage,
          showStarMark: showStarMark),
      onChanged: functionCallBack,
      maxLength: 6,
      keyboardType:
          (Platform.isIOS) ? TextInputType.text : TextInputType.number,
      inputFormatters: [
        //LengthLimitingTextInputFormatter(10),
        FilteringTextInputFormatter.digitsOnly,
      ],
    );
  }

  Widget buildNameTextField() {
    return TextFormField(
      focusNode: focusNode,
      controller: textEditingController,
      style: theme.headline2.copyWith(color: headLine1Color),
      decoration: textFieldInputDecoration(
          hintText: hintText,
          errorText: displayErrorMessage,
          showStarMark: showStarMark),
      onChanged: functionCallBack,
      keyboardType: TextInputType.text,
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp('[ a-zA-Z0-9 ]')),
      ],
    );
  }
}
