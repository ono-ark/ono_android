import 'package:flutter_easyloading/flutter_easyloading.dart';

class ToastMessage {
  static showToastMessage({String message}) {
    EasyLoading.showToast(
      message,
      toastPosition: EasyLoadingToastPosition.bottom
    );
  }
}
