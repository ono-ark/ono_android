enum OnoEnvironments { dev, prod, none }

enum UserSessionStatus { freshUser, registeredUser, registrationPending, none }
enum ApiResponseType { successful, failure, none }
enum TextFieldValidationType { success, error, enable, disable, none }
enum UserType { farmer, buyer, none }
enum GenderType { male, female, none }
enum PickImageMode { camera, gallery, none }
enum ProfileDetailsStatus { profilePhoto, fullName, none }
enum LocationStatus { allow, deny, none }
enum UserAppStatus {
  language,
  location,
  signIn,
  profile,
  cropsSelection,
  dashBoard,
  none
}

enum CropSelection { create, edit, none }
enum TapActionArrows { prev, next, none }
enum DashBoardUIConditions {
  noLocation,
  locationIsAvailable,
  preferredCropsWithInLocation,
  preferredCropsNotInLocation,
  none
}
