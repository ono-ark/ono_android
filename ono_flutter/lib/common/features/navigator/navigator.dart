import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// Binds an internal Navigator reference to a specific section of the
/// from each navigation.
class FeatureNavigator extends StatelessWidget {
  final Widget sectionChild;
  final bool maintainState;

  FeatureNavigator({@required this.sectionChild, this.maintainState = false});

  @override
  Widget build(BuildContext context) {
    return Navigator(onGenerateRoute: (routeSettings) {
      return MaterialPageRoute(
          // maintainState has been set to false because the parent initial
          // screen shown on each section should be recreated each time
          // the section becomes active. If the flag is not provided, any
          // change to the screen will be preserved on memory, so the next
          // time the user navigates to that section again, scroll state
          // or data on fields are displayed again.
          builder: (context) => sectionChild,
          maintainState: maintainState);
    });
  }
}
