import 'package:flutter/material.dart';
import 'package:ono_flutter/features/language_selection/ui/language_feature_widget.dart';
import 'package:ono_flutter/features/user_session/ui/user_session_feature_widget.dart';

class OnoRouter {
  static String initialRoute = "/";
  static setUpRoutes(BuildContext context) {
    return <String, WidgetBuilder>{
      '/': (BuildContext context) => UserSessionFeatureWidget(),
    };
  }
}
