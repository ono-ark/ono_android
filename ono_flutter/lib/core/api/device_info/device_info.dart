import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';
import 'package:ono_flutter/features/sign_in/login/model/login_business_model.dart';

class DeviceInfoAPI {
  DeviceInfoPlugin _deviceInfoPlugin;
  DeviceInfoAPI() {
    _deviceInfoPlugin = DeviceInfoPlugin();
  }
  DeviceInfoAPI.withDependencies(this._deviceInfoPlugin);

  Future<DeviceInfoBusinessModel> getDeviceInfo() async {
    DeviceInfoBusinessModel deviceInfoBusinessModel = DeviceInfoBusinessModel();

    try {
      if (Platform.isAndroid) {
        deviceInfoBusinessModel =
            _readAndroidBuildData(await _deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        deviceInfoBusinessModel =
            _readIosDeviceInfo(await _deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceInfoBusinessModel = null;
    }
    return deviceInfoBusinessModel;
  }

  _readAndroidBuildData(AndroidDeviceInfo build) {
    DeviceInfoBusinessModel deviceInfoBusinessModel = DeviceInfoBusinessModel();
    deviceInfoBusinessModel.device = build.manufacturer;
    deviceInfoBusinessModel.deviceVersion = build.version.release;
    deviceInfoBusinessModel.model = build.model;
    return deviceInfoBusinessModel;
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  _readIosDeviceInfo(IosDeviceInfo data) {
    DeviceInfoBusinessModel deviceInfoBusinessModel = DeviceInfoBusinessModel();
    deviceInfoBusinessModel.device = data.systemName;
    deviceInfoBusinessModel.deviceVersion = data.systemVersion;
    deviceInfoBusinessModel.model = data.model;
    return deviceInfoBusinessModel;
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }
}
