import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ono_clean_framework/Log.dart';
import 'package:ono_clean_framework/external_dependency.dart';
import 'package:ono_flutter/core/api/secure_storage/tags.dart';
import 'package:package_info/package_info.dart';

class SecuredStorageAPI extends ExternalDependency {
  final FlutterSecureStorage _storage;

  SecuredStorageAPI._internal(this._storage);

  factory SecuredStorageAPI() =>
      SecuredStorageAPI._internal(FlutterSecureStorage());

  factory SecuredStorageAPI.withDependencies(FlutterSecureStorage storage) =>
      SecuredStorageAPI._internal(storage);

//Read single value from secured storage
  Future<String> readFromSecuredStorage(String secKey) async {
    String secValue;
    final IOSOptions options = await _getGroupIdOptions();

    if (options != null) {
      secValue = await _storage.read(key: secKey, iOptions: options);
    } else {
      secValue = await _storage.read(key: secKey);
    }
    Log.d(Tags.readFromSecuredStorageSecValue,
        "$secKey: $secKey secValue: $secValue");
    return secValue;
  }

  ///  This method is not used in the code, use it as required for debugging.
  Future<Map<String, String>> readAllFromSecuredStorage() async {
    Map<String, String> secValue;
    final IOSOptions options = await _getGroupIdOptions();
    if (options != null) {
      secValue = await _storage.readAll(iOptions: options);
    } else {
      Log.d(Tags.readAllFromSecuredStorage,
          "readAllFromSecuredStorage: options is null");
    }
    Log.d(Tags.readAllFromSecuredStorageSecValue, " - secValue: $secValue");
    return secValue;
  }

  Future<bool> readBoolFromSecuredStorage(String key) async {
    bool isTrue = false;
    final IOSOptions options = await _getGroupIdOptions();
    if (options != null) {
      await _storage.read(key: key, iOptions: options).then((val) {
        if (val != null) {
          isTrue = val == 'true';
        }
      });
    } else {
      await _storage.read(key: key).then((val) {
        if (val != null) {
          isTrue = val == 'true';
        }
      });
    }
    Log.d(Tags.readBoolFromSecuredStorage, " - secKey: $key secValue: $isTrue");
    return isTrue;
  }

  Future<void> writeToSecuredStorage(String secKey, String secVal) async {
    final IOSOptions options = await _getGroupIdOptions();
    Log.d(Tags.writeToSecureStorage, " - secKey: $secKey secValue: $secVal");
    if (options != null) {
      await _storage.write(key: secKey, value: secVal, iOptions: options);
    } else {
      await _storage.write(key: secKey, value: secVal);
    }
  }

  Future<void> deleteFromSecuredStorage(String secKey) async {
    final IOSOptions options = await _getGroupIdOptions();
    if (options != null) {
      await _storage.delete(key: secKey, iOptions: options);
    } else {
      await _storage.delete(key: secKey);
    }
  }

  //Delete single value from secured storage
  Future<void> deleteAllFromSecuredStorage() async {
    final IOSOptions options = await _getGroupIdOptions();
    if (options != null) {
      await _storage.deleteAll(iOptions: options);
    } else {
      await _storage.deleteAll();
    }
  }

  /// Generating APP group Id from IOS application bundle identifier.
  /// Here following the naming conversion for group ID as "group.<BundleIdentifier>
  /// This GroupID is used to share the keychain content between Main App and
  /// Extesnion app for Native Quick Balance widget

  Future<IOSOptions> _getGroupIdOptions() async {
    IOSOptions _options;
    if (defaultTargetPlatform == TargetPlatform.iOS) {
      PackageInfo packageInfo;
      try {
        packageInfo = await PackageInfo.fromPlatform();
      } catch (e) {
        packageInfo = PackageInfo(packageName: "",appName: '',buildNumber: '',version: '');
        Log.e(Tags.unableToReadPackageName,
            'Unable to read Package name from PackageInfo');
      }
      final String packageName = packageInfo.packageName;
      _options = IOSOptions(groupId: "group.$packageName.QBWidgetExt");
    } else {
      _options = null;
    }
    return _options;
  }
}
