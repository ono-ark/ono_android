class Tags {
  static final String readAllFromSecuredStorage = 'readAllFromSecuredStorage';
  static final String readAllFromSecuredStorageSecValue =
      'readAllFromSecuredStorageSecValue';
  static final String readFromSecuredStorageSecValue =
      'readFromSecuredStorageSecValue';
  static final String readBoolFromSecuredStorage = 'readBoolFromSecuredStorage';
  static final String unableToReadPackageName = 'unableToReadPackageName';
  static final String writeToSecureStorage = 'writeToSecureStorage';
}