import 'dart:io';

import 'package:ono_clean_framework/service/options.dart';
import 'package:ono_flutter/env/environment.dart';

class FuseAPI {
  static final String _agrifiApiKey = 'X-AGF-APIKEY';
  static final String _agrifiApiToken =
      'a36d856d-66f7-4ee9-a7d9-a9fc07225a70-5d896ce8-10c6-11eb-adc1-0242ac120002';
  static String serverUrl = OnoEnvironment.serverUrl;
  static String signInUrl = serverUrl + 'account/sign-in';
  static String languageUrl = serverUrl + 'common/langs';
  static String getCropsUrl = serverUrl + 'market-crop/crops';
  static String cropsPreferenceUrl =
      serverUrl + 'prefs/onoId/{onoId}/type/{type}';
  static String dailyPriceUrl =
      serverUrl + 'dashboard/crops/onoId/{onoId}/lat/{lat}/lon/{lon}';
  static String nearByMarketsUrl =
      serverUrl + 'dashboard/crops/onoId/{onoId}/marketId/{marketId}';
  static String preferredCropsUrl =
      serverUrl + 'prefs/onoId/{onoId}/type/{type}';
  static String otpValidateUrl = serverUrl + 'account/sign-in/validate';
  static final FuseAPI _singleton = FuseAPI._internal();

  factory FuseAPI() {
    return _singleton;
  }

  FuseAPI._internal() {
    getFuseOptions();
  }

  ///To set the api key
  static Options getFuseOptions() {
    Options defaultOptions = Options(
        headers: <String, String>{_agrifiApiKey: _agrifiApiToken},
        contentType: ContentType.json);

    return defaultOptions;
  }

  static Map<String, String> fileUploadFuseOptions() {
    Map<String, String> headers = {
      _agrifiApiKey: _agrifiApiToken,
      "Content-Type": "multipart/form-data",
    };
    return headers;
  }

  Map<String, String> headers = {
    "X-AGF-APIKEY":
        "a36d856d-66f7-4ee9-a7d9-a9fc07225a70-5d896ce8-10c6-11eb-adc1-0242ac120002",
    "Content-Type": "multipart/form-data",
  };
}
