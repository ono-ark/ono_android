import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:network_image_to_byte/network_image_to_byte.dart';
import 'package:ono_flutter/core/api/local_storage/local_storage_api.dart';
import 'package:ono_flutter/core/api/local_storage/model.dart';

class ConvertNetWorkImageIntoMemory {
  static Future<Uint8List> convertImage(
      {String imageUrl, String cropId}) async {
    Uint8List bytes;
    dynamic response = await LocalStorageAPI().readDataIntoStorage(key: cropId);
    if (response == null) {
      bytes = await networkImageToByte(imageUrl);
      LocalStorageAPI().writeDataIntoStorage(
          cropsLocalStorageModel: CropsLocalStorageModel(
              cropId: cropId, base64Image: base64Encode(bytes)));
    } else {
      dynamic response =
          await LocalStorageAPI().readDataIntoStorage(key: cropId);
      //print('respppp${response}');
      //print('responsefromlocallll${response}');
      bytes = base64Decode(response['base64Image']);
    }

    return bytes;
  }
}
