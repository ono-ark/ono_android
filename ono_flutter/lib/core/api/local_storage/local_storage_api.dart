import 'package:localstorage/localstorage.dart';
import 'package:ono_clean_framework/Log.dart';
import 'package:ono_clean_framework/service/ono_service.dart';
import 'package:ono_flutter/core/api/local_storage/local_storage_keys.dart';
import 'package:ono_flutter/core/api/local_storage/model.dart';

class LocalStorageAPI {
  LocalStorage _localStorage;
  LocalStorageAPI() {
    _localStorage = LocalStorage('ono');

    // clearDataFromStorage();
  }

  LocalStorageAPI.withDependencies(this._localStorage);

  Future<dynamic> writeDataIntoStorage(
      {CropsLocalStorageModel cropsLocalStorageModel, String key}) async {
    try {
      var value = await _localStorage.ready;
      if (value) {
        await _localStorage.setItem(
            cropsLocalStorageModel.cropId, cropsLocalStorageModel.toJson());
      }
      // Log.d('writeDataIntoStorage', "${cropsLocalStorageModel.toJson()}");
    } catch (e) {}
    return true;
  }

  readDataIntoStorage({String key}) async {
    dynamic response;
    var value = await _localStorage.ready;
    if (value) {
      response = await _localStorage.getItem(key);
      // Log.d('readDataIntoStorage', "${response}");
    }
    return response;
  }

  Future<dynamic> clearDataFromStorage() async {
    await _localStorage.clear();
    return true;
  }

  deleteDataFromStorage({String key}) async {
    await _localStorage.deleteItem(key ?? LocalStorageKeys.cropsList);
  }

  disposeStorage() async {
    _localStorage.dispose();
  }
}
