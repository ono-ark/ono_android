import 'package:ono_clean_framework/service/ono_service.dart';

class CropsLocalStorageModel {
  String base64Image;
  String cropId;

  CropsLocalStorageModel({this.base64Image, this.cropId});

  factory CropsLocalStorageModel.fromJson(Map<String, dynamic> json) {
    return CropsLocalStorageModel(
      base64Image: json['base64Image'],
      cropId: json['cropId'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['base64Image'] = this.base64Image;
    data['cropId'] = this.cropId;
    return data;
  }
}
