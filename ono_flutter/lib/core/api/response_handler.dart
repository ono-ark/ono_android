import 'package:ono_clean_framework/bloc/bloc.dart';
import 'package:ono_clean_framework/service/ono_service.dart';
import 'package:ono_clean_framework/service/response.dart';

class ResponseHandlerBloc extends Bloc implements OnoServiceResponseHandler {
  @override
  void onInvalidRequest(Error error) {}

  @override
  void onInvalidSession() {}

  @override
  void onServerError() {}

  @override
  void onUnexpectedContent(Exception exception) {}

  @override
  void onResponseError(Response response) {
    if (response != null) {
      //  showAPIErrorWidget(response: response);
    }
  }
}
