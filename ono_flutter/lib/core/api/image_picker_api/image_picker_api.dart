import 'package:image_picker/image_picker.dart';
import 'package:ono_clean_framework/Log.dart';

class ImagePickerApi {
  ImagePicker _imagePicker;

  ImagePickerApi() {
    _imagePicker = ImagePicker();
  }

  ImagePickerApi.withDependencies(this._imagePicker);

  Future<PickedFile> pickImageFromCamera() async {
    final pickedFile = await _imagePicker.getImage(
        source: ImageSource.camera, preferredCameraDevice: CameraDevice.front);
    Log.d('Pick fromCamera path', '${pickedFile.path}');
    return pickedFile;
  }

  Future<PickedFile> pickImageFromGallery() async {
    final pickedFile = await _imagePicker.getImage(
      source: ImageSource.gallery,
    );
    return pickedFile;
  }
}
