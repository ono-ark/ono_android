import 'package:geocoder/geocoder.dart';

class GeoCoderAPI {
  static Future getAddressFromLatAndLong({String lat, String long}) async {
    Address address;
    double latitude=double.parse(lat);
    double longitude=double.parse(long);
    final coordinates = new Coordinates(latitude, longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    address = addresses.first;
    print("${address.featureName} : ${address.addressLine}");
    return address;
  }
}
