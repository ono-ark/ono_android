import 'package:flutter/services.dart';
import 'package:location/location.dart';

class LocationAPI {
  Location _location;

  LocationAPI() {
    _location = Location();
  }

  LocationAPI.withDependencies(this._location);

  Future<PermissionStatus> checkAndRequestPermission() async {
    PermissionStatus _permissionGranted = await _location.hasPermission();
    if (_permissionGranted != PermissionStatus.granted) {
      _permissionGranted = await _location.requestPermission();
    }
    return _permissionGranted;
  }

  Future<bool> checkAndRequestService() async {
    final bool serviceEnabledResult = await _location.serviceEnabled();
    bool serviceRequestedResult;
    if (serviceEnabledResult == null || !serviceEnabledResult) {
      serviceRequestedResult = await _location.requestService();
    } else {
      serviceRequestedResult = serviceEnabledResult;
    }
    return serviceRequestedResult;
  }

  Future<LocationData> getLocation() async {
    LocationData _locationResult;
    try {
      _locationResult = await _location.getLocation();
    } on PlatformException catch (err) {
      if (err.code == 'PERMISSION_DENIED') {
        // print('codeee${err.code}');
        _locationResult = null;
      }
    }
    return _locationResult;
  }
}
