import 'package:ono_flutter/common/enums.dart';
import 'package:ono_flutter/core/api/secure_storage/secure_storage_api.dart';

class UserInfoAPI {
  static SecuredStorageAPI _securedStorageAPI = SecuredStorageAPI();
  static String _onoIdValue = '';
  static String _languagePreferenceIdValue = '';
  static final String _languagePreferenceIdKey = 'languagePreferenceId';
  static final String _languagePreferenceKey = 'languagePreference';
  static String _languagePreferenceValue = '';
  static final String _locationKey = 'location';
  static String _locationDetails = '';
  static final String _preferenceValue = 'LANGUAGE';
  static final String _userAppStatusKey = 'appStatus';
  static final String _userProfilePicKey = 'profilePic';
  static final String _userCropPreferencesListKey = 'cropPreference';
  static String _userProfilePic = '';
  static String _userCropPreferencesListValues = '';
  static UserAppStatus _userAppStatusValue = UserAppStatus.none;
  static CropSelection _cropSelection = CropSelection.create;

  static final String _onoIdKey = "OnoId";

  static final UserInfoAPI _singleton = UserInfoAPI._internal();

  factory UserInfoAPI() {
    return _singleton;
  }

  UserInfoAPI._internal() {
    _onoIdValue = '';
    _languagePreferenceIdValue = '';
    _languagePreferenceValue = '';
    _locationDetails = '';
    _userProfilePic = '';
    _userCropPreferencesListValues = '';
  }

  static String getPreference() {
    return _preferenceValue;
  }

  static CropSelection cropSelection() {
    return _cropSelection;
  }

  static void setCropsSelection({CropSelection cropSelection}) {
    _cropSelection = cropSelection;
  }

  static Future<String> getOnoId() async {
    if (_onoIdValue == null || _onoIdValue.isEmpty) {
      _onoIdValue = await _securedStorageAPI.readFromSecuredStorage(_onoIdKey);
    }
    // _onoIdValue = '13';
    return _onoIdValue;
  }

  static getOnoIdValue() {
    return _onoIdValue;
  }

  static Future<String> getLanguagePreference() async {
    if (_languagePreferenceValue.isEmpty) {
      _languagePreferenceValue = await _securedStorageAPI
          .readFromSecuredStorage(_languagePreferenceKey);
    }
    return _languagePreferenceValue;
  }

  static Future<String> getLanguagePreferenceId() async {
    if (_languagePreferenceIdValue.isEmpty) {
      _languagePreferenceIdValue = await _securedStorageAPI
          .readFromSecuredStorage(_languagePreferenceIdKey);
    }

    return _languagePreferenceIdValue;
  }

  static setOnoId(String value) async {
    _onoIdValue = value;
    await _securedStorageAPI.writeToSecuredStorage(_onoIdKey, value);
  }

  static setLocationDetails(String value) async {
    _locationDetails = value;
    await _securedStorageAPI.writeToSecuredStorage(_locationKey, value);
  }

  static setLanguagePreference(String value) async {
    _languagePreferenceValue = value;
    await _securedStorageAPI.writeToSecuredStorage(
        _languagePreferenceKey, value);
  }

  static setLanguagePreferenceId(String value) async {
    _languagePreferenceIdValue = value;
    await _securedStorageAPI.writeToSecuredStorage(
        _languagePreferenceIdKey, value);
  }

  static setUserAppStatus({UserAppStatus userAppStatus}) async {
    _userAppStatusValue = userAppStatus;
    await _securedStorageAPI.writeToSecuredStorage(_userAppStatusKey,
        _returnUserAppStatusEnumToString(userAppStatus: userAppStatus));
  }

  static setUserProfilePic(String value) async {
    _userProfilePic = value;
    await _securedStorageAPI.writeToSecuredStorage(_userProfilePicKey, value);
  }

  static setUserCropPreferenceList(String value) async {
    _userCropPreferencesListValues = value;
    await _securedStorageAPI.writeToSecuredStorage(
        _userCropPreferencesListKey, value);
  }

  static Future<String> getUserPreferencesList() async {
    if (_userCropPreferencesListValues == null &&
        _userCropPreferencesListValues.isEmpty) {
      _userCropPreferencesListValues = _returnUserAppStatusStringToEnum(
          await _securedStorageAPI
              .readFromSecuredStorage(_userCropPreferencesListKey));
    }
    return _userCropPreferencesListValues;
  }

  static Future<UserAppStatus> getUserAppStatus() async {
    if (_userAppStatusValue == UserAppStatus.none) {
      _userAppStatusValue = _returnUserAppStatusStringToEnum(
          await _securedStorageAPI.readFromSecuredStorage(_userAppStatusKey));
    }
    return _userAppStatusValue;
  }

  static Future<String> getLocationDetails() async {
    if (_locationDetails == null || _locationDetails.isEmpty) {
      _locationDetails =
          await _securedStorageAPI.readFromSecuredStorage(_locationKey);
    }
    return _locationDetails;
  }

  static Future<String> getUserProfilePic() async {
    if (_userProfilePic == null || _userProfilePic.isEmpty) {
      _userProfilePic =
          await _securedStorageAPI.readFromSecuredStorage(_userProfilePicKey);
    }
    return _userProfilePic;
  }

  static _returnUserAppStatusEnumToString({UserAppStatus userAppStatus}) {
    switch (userAppStatus) {
      case UserAppStatus.language:
        return 'language';
        break;
      case UserAppStatus.location:
        return 'location';
        break;
      case UserAppStatus.signIn:
        return 'signIn';
        break;
      case UserAppStatus.profile:
        return 'profile';
        break;
      case UserAppStatus.none:
        return 'none';
        break;
      case UserAppStatus.cropsSelection:
        return 'cropsSelection';
        break;
      case UserAppStatus.dashBoard:
        return 'dailyPrice';
        break;
      default:
        return;
    }
  }

  static _returnUserAppStatusStringToEnum(String value) {
    switch (value) {
      case 'language':
        return UserAppStatus.language;
        break;
      case 'location':
        return UserAppStatus.location;
        break;
      case 'signIn':
        return UserAppStatus.signIn;
        break;
      case 'profile':
        return UserAppStatus.profile;
        break;
      case 'cropsSelection':
        return UserAppStatus.cropsSelection;
        break;
      case 'dailyPrice':
        return UserAppStatus.dashBoard;
        break;
      case 'none':
        return UserAppStatus.none;
        break;
      default:
        return;
    }
  }
}
