# [**AgFin App Design**](../../README.md)

## Using Pipes

One very important part of the Bloc architecture followed by the AgFin App is the mechanism
to transmit data between Blocs and UI elements. The Bloc design avoids completely the calling of
methods directly between classes, in order to remove dependencies (this is high coupling), which
creates code very difficult to maintain, refactor and reuse.

The preferred way to send data are asynchronous streams, which follow the Publisher / Subscriber
pattern and are easily accessible as part of the Dart language.

To make easier the implementation of these streams, we have available a practical wrapper we
called Pipe, which is a core class that is just a direct implementation of those streams but with 
a more straightforward set of methods, since most of the streams require a bit of boilerplate code.

The most basic example of a Pipe with some common calls is the following:

```
Pipe<bool> myFlagPipe = Pipe<bool>;

myFlagPipe.send(true);

myFlagPipe.receive.listen((newValue) => // do something with the value );
```

Pipes are meant to be used as public attributes of any Bloc, allowing any widget that wishes to 
listen to changes through them easily attach listeners, as in the above example.

The data object sent through a Pipe can be of any type, thus making easy for the Bloc to send a
whole Model to the UI, like this:

```
class MyModel {
  DateTime date;
  String name;
  List<double> coordinates;
  
  // assume constructors and JSON parsers are contained in the class
}

// Bloc code:

Pipe<MyModel> myModelPipe = Pipe<MyModel>;
myModelPipe.send(MyModel());

// UI Code:

StreamBuilder<MyModel>(
  stream: _bloc.myModelPipe.receive,
  builder: (context, snapshot){
      // Extract the values from the model and show them here.
  }
);

```

There are available additional types of Pipes:

### Event Pipe
### Broadcast Pipe
### Validator Pipe
