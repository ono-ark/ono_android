# **Developer Notes**

### General guidelines
   - Class, Function, Fields - Naming conventions
   - Class, Function, Fields - Access Levels
   - Constants
   - Globals
   - Handling Literals & Keys.
   - Handling exceptions.
   - ADA Compliance
   - Security Compliance
### **__API Layer__**
#### Design guidelines for Datamodel
>Design Considerations.
  >- Should the data model design be specific to
    >- UI
    >- Usecase/Feature/Business/Domain
    >- Rest Endpoint.
  >- Responsibilities/Scope of a data model.
  >- What is common across data models.
  >- Testability
### __**BLOC Layer**__
#### Design guidelines for BLOC
>Design Considerations.
   >- How the bloc needs to be accessed.
   >- Handling Business logic
   >- Manage lifetime of Streams
   >- Responsibilities/Scope of a Bloc.
   >- What is common across Bloc.
   >- Testability
### __**UI Layer**__
#### Design guidelines for UI
>Design Considerations.
   >- Should life time of the Bloc be within the scope of a Widget or share between Widgets
   >- Releasing the resources at its disposal
   >- Maintaining State if required.
   >- Dynamic UI
   >- Reusability
   >- Testability
### __**Testing**__
#### Guidelines to write unit test
   - Mocking the dependent objects
   - Cover happy path & alternate path
   - Handling exceptions
   - Organize files, Scope of the file, naming convention
