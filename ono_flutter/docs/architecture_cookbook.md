# Architecture CookBook

The purpose of this document is to give guidelines and examples on how to code all the
components in a feature folder.

## Folder Structure

This is the typical feature folder, with it's corresponding test folder:

```
/features
    /{FEATURE NAME}
        /api
            example_service.dart
            example_request_model.dart
            example_response_model.dart
        /bloc
            example_bloc.dart
            example_dependency.dart
        /model
            example_view_model.dart
            example_business_model.dart
        /ui
            example_feature_widget.dart
            example_presenter.dart
            example_screen.dart
            example_strings.dart
            example_widget_keys.dart

/test
    /mocked
        /features
            /{FEATURE NAME}
                /api
                    example_service_test.dart
                /bloc
                    mock_example_service.dart
                    example_bloc_test.dart
                    example_dependency_test.dart
                /ui
                    example_screen_widget_test.dart

```

Note how all files follow Dart naming conventions and that they include a suffix according to its purpose.

---
##### GUIDELINE 1.1: Files should contain only one class. Additional classes are allowed only if they are private.
---
##### GUIDELINE 1.2: Classes should have exactly the same name as their file name.
---


## API and Services

### Service Implementation Example
```
class ExampleService extends AgFinService {
  ExampleService(AgFinServiceResponseHandler handler, {FuseAPI fuseApi})
      : super(
            url: "service/{productId}/catalog",
            action: HttpAction.get,
            handler: handler,
            restApi: restApi);

  @override
  TestResponse parseJson(Map<String, dynamic> json) {
    return TestResponse.fromJson(json);
  }
}
```
##### GUIDELINE 2.1: Services should extend from AgFinService and provide the necessary parameters to the super constructor.
---
##### GUIDELINE 2.2: Any Service that expects a JSON response should have a corresponding unique ResponseModel, and that class should be used in the parseJson method override.
---
##### GUIDELINE 2.3: Services should not include GET parameters on the URL string. To pass parameters, use a RequestModel.
---
##### GUIDELINE 2.4: If the URL contains a path parameter, provide the value in a RequestModel that contains an attribute that matches that parameter.
---

### Response and Request Models
```
class ExampleResponse implements ResponseModel {
  final String status;
  final Date creationDate;
  final double amount;

  ExampleResponse.fromJson(Map<String, dynamic> json) :
    status = json['status'] ?? '',
    creationDate = json['creationDate'] ?? DateTime.now(),
    amount = double.parse(json['amount']) ?? 0.00;
}
```
##### GUIDELINE 3.1: All Request and Response Models are immutable, so all attributes must be final, and only initialized with a constructor.
---
##### GUIDELINE 3.2: All classes that implement ResponseModel should have only a **fromJson** constructor where each field is extracted from the JSON Map. It has no other constructors.
---
##### GUIDELINE 3.3: All attributes should have a default value when the JSON field doesn't exist or is null.
---
##### GUIDELINE 3.4: When parsing the json, parse any data to a proper type, only use String when it makes sense. Failures when parsing will be handled by the Service.
---
##### GUIDELINE 3.5: If a critical field is missing in the json response, throw an exception to cause the service to fail.
---


A Request Model can be used to add a map of key/values to the body of the request. If the key exists in then query url as a path parameter or a GET parameter, the value is used there instead.

```
class ExampleRequestModel implements RequestModel {
  final String param;
  final String extra;

  ExampleRequestModel({@required this.param, this.extra = ''}) :
    assert(param != null && param.isNotEmpty);

  @override
  Map<String, dynamic> toJson() {
    var json = <String, dynamic>{};
    json['param'] = param;
    if (extra.isNotEmpty) json['extra'] = extra;
    return json;
  }
}
```
##### GUIDELINE 4.1: A RequestModel should assert all required parameters and provide default values for optional ones.
---
##### GUIDELINE 4.2: No RequestModel should create a JSON with a null field. Try to avoid empty fields when possible too.
---

### Service Test
```
  test('ExampleService Success default data', () async {
    var handler = ExampleResponseHandler();
    var getService =
        ExampleService(handler, restApi: MockFuseAPI.successful());
    ExampleResponse response = await getService.request();
    expect(response, isNotNull);
    expect(response.status, "success");
    expect(handler.result, isNull);
  });

  test('ExampleService server error', () async {
      var handler = CustomResponseHandler();
      var response = await ExampleService(handler,
              restApi: MockFuseAPI.withServerError())
          .request();
      expect(response, isNull);
      expect(handler.result, ResponseHandlerResult.serverError);
  });
```
##### GUIDELINE 5.1: All Services have a corresponding unit test that works with a Mockey success escenario.
---
##### GUIDELINE 5.2: When Service calls have custom Handlers, unit tests for every error response should be included. These test can use Mocked FUSE responses.
---

## Models
### ViewModel

The classes that extend the ViewModel are meant to hold all the data that will be passed
to the UI components. The common approach is to have one per screen, and these models will
be transmitted to the UI through to a Pipe, and then listened by a StreamBuilder.

```
@immutable
class ExampleViewModel extends ViewModel{
  final int id;
  final String name;

  ExampleViewModel({@required this.id, this.name = ''}) :
    assert(id != null && id > 0);

}

```
##### GUIDELINE 6.1: All View Models are immutable, so all attributes must be final, and only initialized with a constructor.
---
##### GUIDELINE 6.2: Fields of a View Model are either required or optional with a default value. No fields will be null. Each required field will have an assert that validates it, and the assert has to make sure no nulls are received in the class.
---
##### GUIDELINE 6.3: Blocs can create multiple ViewModels, but only one ViewModel will be used per UI file.
---
##### GUIDELINE 6.4: UI will receive it's ViewModel on creation. This initial model will hold default or empty values.
---
##### GUIDELINE 6.5: No View Models will be persisted anywhere. They are created and immediately used by the UI.
---
##### GUIDELINE 6.6: ViewModels cannot be reused on multiple screens, since each screen has its own set of business rules. It is better to repeat code in two different ViewModels than introduce inter-dependencies.
---


### BusinessModel

These classes exist only inside a Bloc. They will preserve any data that is fetched by APIs and
dependencies. When a ViewModel has to be created and sent to the UI, the data will come from
the instance of a BusinessModel. Once the Bloc is disposed, the models are destroyed as well.

```
class ExampleBusinessModel extends BusinessModel{
  DateTime lastUpdated = DateTime.now();
  List<AccountBusinessModel> accounts = [];
}

class AccountBusinessModel extends BusinessModel{
  String accountId = '';
  String nickName = '';
  Double balance = 0.00;
}

```
##### GUIDELINE 7.1: Business Models fields are mutable, and no constructor is needed.
---
##### GUIDELINE 7.2: Fields of Business Models will have initial values. A View Model
---
##### GUIDELINE 7.3: Business Models fields are only used within a Bloc, and only belong to one feature (no shared Business Models).
---
##### GUIDELINE 7.4: Business Models might share field names and types with View Models, but also one Business Model can hold the data that build multiple View Models.
---
##### GUIDELINE 7.5: Any data that needs to be preserved by the UI will be sent to the bloc and stored in the Business Model.
---
##### GUIDELINE 7.6: Any data that needs to be preserved by the UI will be sent to the bloc and stored in the Business Model.
---
##### GUIDELINE 7.7: Keep the code that creates ViewModel inside Business Model methods and return it.
---

## Blocs

### ResponseHandlerBloc

These classes will handle internally any error thrown by Services.

```
class ExampleBloc extends ResponseHandlerBloc {
  EventPipe userInteractionEvent = EventPipe();
  Pipe<ExampleViewModel> exampleViewModelPipe =
      Pipe<ExampleViewModel>(initialData: ExampleViewModel());

  ExampleBusinessModel _businessModel = ExampleBusinessModel();

  ExampleService _service;

  ExampleBloc({ExampleService service}) {
    _service = service ??= ExampleService(this);
    userInteractionEvent.listen(_userInteractionListener);
  }

  void _userInteractionListener() async{
    var responseModel = await service.request();
    if (!responseModel) return;

    _updateBusinessModel(responseModel);
    _sendViewModel();
  }

  void _updateBusinessModel(ExampleResponseModel response){
    _businessModel.lastUpdated = response.creationTime;
  }

  // This is a concept example, it won't work if you copy and execute it
  void _sendViewModel(){

    // A view model is created with the current data from the business model
    var viewModel = ViewModel(
          id: _businessModel.accounts.first.accountId;
          name: _businessModel.accounts.first.name;
    );
    exampleViewModelPipe.send(viewModel);
  }

  @override
  void dispose() {
    super.dispose();
    userInteractionEvent.dispose();
    exampleViewModelPipe.dispose();
  }
}
```
##### GUIDELINE 8.1: ResponseHandlerBlocs will handle the errors without extra code, but understand that they have to be caught in the UI by a ResponseHandlerWidget.
---
##### GUIDELINE 8.2: Blocs cannot be shared between features, each feature has its own blocs.
---
##### GUIDELINE 8.3: No objects or instances are created inside Blocs (except for Business and View Models) without allowing the reference be received through the constructor. This is important to allow Mocks be inserted during tests.
---
##### GUIDELINE 8.4: Pipes are the only the interface for layers above (UI) to communicate with Bloc. Therefore keep the methods internal to Bloc.
---
##### GUIDELINE 8.5: Blocs will only have one way to create View Models, centralize the process of copying the data from a BusinessModel in a single private method.
---
##### GUIDELINE 8.6: Blocs will only have one way to send a View Model to the UI, which is commonly a pipe or broadcast pipe.
---
##### GUIDELINE 8.7: Blocs will only use Business Models to preserve data.
---
##### GUIDELINE 8.8: Blocs will call the request on services on event or pipes listeners. No service should be requested as part of the constructor.
---

### Bloc Test
```
void main() {
  test('ExampleBloc gets model from service', () {
    final bloc = ExampleBloc();

    bloc.exampleViewModelPipe.receive.listen(expectAsync1((model) {
      expect(model, isA<ExampleViewModel>());
      expect(model.loginCount, 3);
      expect(model.lastLogin.toString(), '2020-05-01 00:00:00.000');
    }));

    bloc.fetchLoginDetails.launch();
  });
}
```
##### GUIDELINE 8.1.1: Bloc tests should include all possible usages of the pipes and events.
---
##### GUIDELINE 8.1.2: On Bloc tests, listeners should always use "expectAsyncX" to ensure the listener is executed at least once.
---
##### GUIDELINE 8.1.3: Launches and pipes sends should happen after a listener is defined, to ensure the listened pipe never sends data before the test is ready to process it.
---
##### GUIDELINE 8.1.4: Bloc tests should include all possible usages of the pipes and events.
---
##### GUIDELINE 8.1.5: When bloc tests have one or more expectAsyncX calls, always add a TimeOut of 3 seconds to the test.
---



## UI

### General Guidelines
##### GUIDELINE 9.1: For every screen, create a set of 3 classes: FeatureWidget, Presenter and Screen. Minor UI components can be present, but no more than one of those types per screen will exist.
---
##### GUIDELINE 9.2: UI components will not hardcode any string visible to the user. Those strings will be added to a strings file.
---

### Feature Widget

These are the entry points to any screen. Their responsibility is to react to the state assigned to the feature,
and control what needs to be displayed. It will also inject the bloc reference in the context with a BlocProvider.


```
class ExampleFeatureWidget extends FeatureWidget {
    ExampleFeatureWidget({Key key}) :
        super(key: key, feature: AgFinFeature.example);

  @override
  Widget buildWhenAvailable(BuildContext context) {
    return BlocProvider<ExampleBloc>(
      bloc: BlocInheritedProvider.of<ExampleBloc>(context) ??
          ExampleBloc(),
      child: ExamplePresenter(),
    );
  }
}
```
##### GUIDELINE 10.1: Feature Widgets will create the bloc reference and add it to the context, and add the Presenter as a child.
---
##### GUIDELINE 10.2: Feature Widgets will be responsible of setting up the screen name that is reported to the Analytics system.
---
##### GUIDELINE 10.3: Navigators from other features and screens will only reference to these classes.
---
##### GUIDELINE 10.4: Feature Widgets will check if the bloc can be retrieved from a Inherited Provider before trying to create one.
---
##### GUIDELINE 10.5: Feature Widgets will include Response Handler Widgets as necessary. Errors that are shown in the UI will not be handled directly by the Presenter to remove code clutter.
---

### Presenter

These classes are the only UI components that interact with Bloc instances. They will retrieve the bloc reference from the context and set up a
Stream Builder, which extracts the View Model and propagates it to the rest of the UI components. This class will also contain any callbacks with
code that requires the use of context, such as navigation and dialog creation.

#### Presenter with no initial data
```
class ExamplePresenter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var bloc = BlocProvider.of<ExampleBloc>(context);

    SchedulerBinding.instance.addPostFrameCallback((_) {
      connectivityBloc.initEvent.launch();
    });

    return StreamBuilder<ExampleViewModel>(
        stream: bloc.viewModelPipe.receive,
        builder: (context, snapshot) {
          if (snapshot.hasData){
            var _model = snapshot.data;
            return _ExampleScreen(
              model: snapshot.data,
              onClose: _onClose,
              onNameChange: _onNameChange,
              onSubmitTap: () => _onSubmitTap(context, bloc);
            );
          }
          else if (snapshot.hasErrors) {
            _ErrorScreen(context);
          }
          return Center(
            key: CommonWidgetKeys.waitingForStream, child: Container());

        });
  }

  void _onClose(){
     // Navigation code
  }

  void _onNameChange(String newName){
    bloc.namePipe.send(newName)
  }

  void _onSubmitTap(BuildContext context, ExampleBloc bloc){
    bloc.submitEvent.launch();
    Navigator.of(context).push(...);
  }

}
```
#### Presenter with initial data
```
class ExamplePresenter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var bloc = BlocProvider.of<ExampleBloc>(context);

    SchedulerBinding.instance.addPostFrameCallback((_) {
      connectivityBloc.initEvent.launch();
    });

    return StreamBuilder<ExampleViewModel>(
        stream: bloc.viewModelPipe.receive,
        initialData: bloc.viewModelPipe.initialData
        builder: (context, snapshot) {
          var _model = snapshot.data;
          if (_model.status == ERROR) {
            _ErrorScreen(context);
          }

          return _ExampleScreen(
            model: snapshot.data,
            onClose: _onClose
          );
        });
  }

  void _onClose(){
    // Navigation code
  }
}
```
##### GUIDELINE 11.1: Presenters are always StatelessWidgets, no state will be saved on UI components.
---
##### GUIDELINE 11.2: Events that are launched when the UI is created should be wrapped by a Post-Frame scheduled callback.
---
##### GUIDELINE 11.3: When not using initial data from the View Model, Presenters provide 3 flows: success, error, and loading. (For consistency, try to follow that order).
---
##### GUIDELINE 11.4: When using initial data, Presenters have no loading flow, only handle success and error.
---
##### GUIDELINE 11.5: Presenters will not have any business logic or state management, or include any widget besides the Screen, the only code that it has should be related to passing around the model and reacting to the callbacks of the Screens.
---
##### GUIDELINE 11.6: Presenters can contain scheduled callbacks to launch events on the bloc. These callbacks are meant to initialize data and/or make service calls. They should not have any business logic inside.
---
##### GUIDELINE 11.7: Presenters should not add any stream subscriptions or listeners to pipes besides the one used for the View Model on the Stream Builder.
---
##### GUIDELINE 11.8: Presenters will never pass the Bloc reference to the rest of the UI components. Only the ViewModel and callbacks are sent down to the Screen.
---
##### GUIDELINE 11.9: If the screen needs a callback, the code body of the callback exists in a method of the Presenter, to reduce the amount of code in the build method. See examples.
---

### Screen

```
class ExampleScreen extends StatelessWidget{
  final ExampleViewModel model;
  final VoidCallback onClose;

  const ExampleScreen({Key key, this.model, this.onClose}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // This is where most of the UI components that are in the screen are built
    return FlatButton(
      child: Text(model.name);
      onPressed: onClose,
    );
  }

}

```
##### GUIDELINE 12.1: Screens will receive the model and build the UI components with the data it contains
---
##### GUIDELINE 12.2: Screens should receive callback functions to allow the Presenter inject any operation after a UI component has interactions with the user (taps, drags, etc).
---
##### GUIDELINE 12.3: Fields of a Screen are required and should have a @required annotation and checked for null on asserts. No fields will be null.
---
##### GUIDELINE 12.4: No Screen or children UI component has access to the Bloc.
---
##### GUIDELINE 12.5: Screens will not have code that is not related to pure UI building logic. Any data it needs comes from the model, and any interaction is transfered to the constructor callbacks.
---
##### GUIDELINE 12.6: Whenever a UI design can be implemented by using one component from our core components catalog, the developer will use that.
---
##### GUIDELINE 12.7: Developers will make sure they are using the Theme appropriately (need examples)
---
##### GUIDELINE 12.8: Whenever a size in virtual pixels needs to be coded, the developer will always use the Material Design guidelines, which is use numbers multiple of 8.
---

### UI Tests
```
void main() {
  testWidgets('ExampleFeature screen with data from view model',
      (tester) async {
    final bloc = ExampleBloc(exampleService: ExampleServiceMock.success());

    final testWidget = TestableFeatureWidgetWrapper(
      child: BlocInheritedProvider(
      bloc: bloc,
      child: ExampleFeatureWidget(),
    ));

    await tester.pumpWidget(testWidget);
    await tester.pump(Duration(milliseconds: 500));

    expect(find.byType(ExampleScreen), findsOneWidget);
    expect(find.text('2020-01-01'), findsOneWidget);
    expect(find.text('5'), findsOneWidget);
  });
}
```
##### GUIDELINE 13.1: UI tests should test all three UI components at the same time (FeatureWidget, Presenter, Screen). They are meant to test what the user expects to see.
---
##### GUIDELINE 13.2: Presenters and Screens should not be tested directly, the test widget should create a FeatureWidget, to make sure everything works together.
---
##### GUIDELINE 13.3: UI tests should include a BlocInheritedProvider to insert a bloc on the test. This is done to be able to add mocked services, since no service will work during the test.
---
##### GUIDELINE 13.4: PumpAndSettle() is used to let the test complete asynchronous background processes. When animations are shown (like progress indicators or shimmers), a normal Pump with Duration should be used instead.
---
##### GUIDELINE 13.5: When validating a screen that contains ViewModel data, always add expects for every field that should be visible. For visible strings, add the hard-coded string value in the expect that should be seen.
---
##### GUIDELINE 13.6: find.byType() should only be used to check for custom classes, not default flutter widgets.
---
##### GUIDELINE 13.7: find.byPredicate() should be avoided. For RichText components, use findRichText (from test_utils.dart).
---
##### GUIDELINE 13.8: findsNothing should be avoided. Use an empty Container with a key. That way you ensure you actually meant to replace content with it.
---